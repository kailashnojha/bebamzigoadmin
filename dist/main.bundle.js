webpackJsonp(["main"],{

/***/ "../../../../../src lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	return new Promise(function(resolve, reject) { reject(new Error("Cannot find module '" + req + "'.")); });
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".navbar-default {\r\n    background-color: #272c33; \r\n    border-color: #272c33; \r\n}\r\n\r\n.right-panel{\r\n\twidth:100vw;\r\n}\r\n\r\n@media only screen \r\nand (max-width : 575px) {\r\n    .right-panel{\r\n\t\twidth:100%;\r\n\t}\r\n}\r\n\r\n.sub-menu-icon{\r\n\ttop:28px!important;\r\n}\r\n\r\n.navbar .navbar-nav li.menu-item-has-children a:before{\r\n\ttop: 20px!important;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<div>\r\n  <aside id=\"left-panel\" class=\"left-panel\" *ngIf=\"!isLoginPage\">\r\n     <nav class=\"navbar navbar-expand-sm navbar-default\">\r\n        <div class=\"navbar-header\">\r\n           <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#main-menu\" aria-controls=\"main-menu\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n           <i class=\"fa fa-bars\"></i>\r\n           </button>\r\n           <br />\r\n           <a class=\"navbar-brand\" routerLink=\"/\"><img src=\"assets/images/logo.png\" alt=\"Logo\"></a>\r\n           <a class=\"navbar-brand hidden\" routerLink=\"/\"><img src=\"assets/images/logo.png\" alt=\"Logo\"></a>\r\n           \r\n        </div>\r\n        <div id=\"main-menu\" class=\"main-menu collapse navbar-collapse\">\r\n           <ul class=\"nav navbar-nav\">\r\n              \r\n              <li>\r\n                 <a routerLink=\"/\"> <img src=\"./../assets/images/sidebar_icons/dashboard.png\" height=\"20px\"> &nbsp;&nbsp;&nbsp;&nbsp; Dashboard </a>\r\n              </li>\r\n              \r\n              <!-- <li>\r\n                 <a routerLink=\"/vehicle-list\"> <i class=\"menu-icon fa fa-dashboard\"></i>Vehicles</a>\r\n              </li> -->\r\n\r\n              <!-- <h3 class=\"menu-title\">Lists</h3> -->\r\n\r\n              <li class=\"menu-item-has-children dropdown\">\r\n                  <a href=\"javascript:;\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> \r\n                     <img src=\"./../assets/images/sidebar_icons/vehicles.png\" height=\"20px\"> &nbsp;&nbsp;&nbsp;&nbsp; \r\n                     Vehicles\r\n                  </a>\r\n                 <ul class=\"sub-menu children dropdown-menu\">\r\n                    <li><a routerLink=\"/vehicle-list/approved\"><i class=\"fa fa-puzzle-piece sub-menu-icon\"></i>Approved</a></li>\r\n                    <li><a routerLink=\"/vehicle-list/pending\"><i class=\"fa fa-id-badge sub-menu-icon\"></i>Pending</a></li>\r\n                    <li><a routerLink=\"/vehicle-list/rejected\"><i class=\"fa fa-bars sub-menu-icon\"></i>Rejected</a></li>\r\n                 </ul>\r\n              </li>\r\n\r\n              <li class=\"menu-item-has-children dropdown\">\r\n                  <a href=\"javascript:;\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n                     <img src=\"./../assets/images/sidebar_icons/user.png\" height=\"20px\"> &nbsp;&nbsp;&nbsp;&nbsp; \r\n                     Users\r\n                  </a>\r\n                 <ul class=\"sub-menu children dropdown-menu\">\r\n                    <li><a routerLink=\"/user-list/drivers\"><i class=\"fa fa-puzzle-piece sub-menu-icon\"></i>Drivers</a></li>\r\n                    <li><a routerLink=\"/user-list/customers\"><i class=\"fa fa-id-badge sub-menu-icon\"></i>Customers</a></li>\r\n                    <li><a routerLink=\"/user-list/owners\"><i class=\"fa fa-bars sub-menu-icon\"></i>Owners</a></li>\r\n                    <li><a routerLink=\"/user-list/admins\"><i class=\"fa fa-bars sub-menu-icon\"></i>Admins</a></li>\r\n                 </ul>\r\n              </li>\r\n\r\n              <li class=\"menu-item-has-children dropdown\">\r\n                 <a href=\"javascript:;\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n                     <img src=\"./../assets/images/sidebar_icons/jobs.png\" height=\"20px\"> &nbsp;&nbsp;&nbsp;&nbsp; \r\n                     Jobs\r\n                  </a>\r\n                 <ul class=\"sub-menu children dropdown-menu\">\r\n                    <li><a routerLink=\"/job-list/completed\"><i class=\"fa fa-puzzle-piece sub-menu-icon\"></i>Completed</a></li>\r\n                    <li><a routerLink=\"/job-list/pending\"><i class=\"fa fa-id-badge sub-menu-icon\"></i>Pending</a></li>\r\n                    <li><a routerLink=\"/user-list/rejected\"><i class=\"fa fa-bars sub-menu-icon\"></i>Rejected</a></li>\r\n                 </ul>\r\n              </li>\r\n\r\n              <!-- <h3 class=\"menu-title\">Add</h3> -->\r\n\r\n              <!-- <li class=\"menu-item-has-children dropdown\">\r\n                 <a href=\"javascript:;\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> <i class=\"menu-icon fa fa-laptop\"></i>Add Job</a>\r\n                 <ul class=\"sub-menu children dropdown-menu\">\r\n                    <li><a routerLink=\"/add-job/on-demand\"><i class=\"fa fa-puzzle-piece sub-menu-icon\"></i>On Demand</a></li>\r\n                    <li><a routerLink=\"/add-job/standard-rate\"><i class=\"fa fa-id-badge sub-menu-icon\"></i>Standard Rate</a></li>\r\n                 </ul>\r\n              </li> -->\r\n\r\n              <!-- <li class=\"menu-item-has-children dropdown\">\r\n                 <a href=\"javascript:;\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> <i class=\"menu-icon fa fa-laptop\"></i>Add User</a>\r\n                 <ul class=\"sub-menu children dropdown-menu\">\r\n                    <li><a routerLink=\"/add-user/driver\"><i class=\"fa fa-puzzle-piece sub-menu-icon\"></i>Driver</a></li>\r\n                    <li><a routerLink=\"/add-user/customer\"><i class=\"fa fa-id-badge sub-menu-icon\"></i>Customer</a></li>\r\n                    <li><a routerLink=\"/add-user/owner\"><i class=\"fa fa-bars sub-menu-icon\"></i>Owner</a></li>\r\n                    <li><a routerLink=\"/add-user/admin\"><i class=\"fa fa-bars sub-menu-icon\"></i>Admin</a></li>\r\n                 </ul>\r\n              </li> -->\r\n\r\n           </ul>\r\n        </div>\r\n      </nav>\r\n  </aside>\r\n\r\n  <div id=\"right-panel\" class=\"right-panel\">\r\n\r\n    <header id=\"header\" class=\"header\" *ngIf=\"!isLoginPage\">\r\n       <div class=\"header-menu\">\r\n          <div class=\"col-sm-7\">\r\n             <!-- <a id=\"menuToggle\" class=\"menutoggle pull-left\"><i class=\"fa fa fa-tasks\"></i></a> -->\r\n          </div>\r\n          <div class=\"col-sm-5\">\r\n             <div class=\"user-area dropdown float-right\">\r\n                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n                <img class=\"user-avatar rounded-circle\" src=\"assets/images/admin.jpg\" alt=\"User Avatar\">\r\n                </a>\r\n                <div class=\"user-menu dropdown-menu\">\r\n                   <!-- <a class=\"nav-link\" href=\"#\"><i class=\"fa fa- user sub-menu-icon\"></i>My Profile</a>\r\n                   <a class=\"nav-link\" href=\"#\"><i class=\"fa fa- user sub-menu-icon\"></i>Notifications <span class=\"count\">13</span></a>\r\n                   <a class=\"nav-link\" href=\"#\"><i class=\"fa fa -cog sub-menu-icon\"></i>Settings</a> -->\r\n                   <a class=\"nav-link\" href=\"javascript:;\" (click)=\"logout()\"><i class=\"fa fa-power -off sub-menu-icon\"></i>Logout</a>\r\n                </div>\r\n             </div>\r\n             <div class=\"language-select dropdown\" id=\"language-select\">\r\n                <a class=\"dropdown-toggle\" href=\"#\" data-toggle=\"dropdown\" id=\"language\" aria-haspopup=\"true\" aria-expanded=\"true\">\r\n                  <i class=\"flag-icon flag-icon-us\"></i>\r\n                </a>\r\n                <div class=\"dropdown-menu\" aria-labelledby=\"language\">\r\n                   <div class=\"dropdown-item\">\r\n                      <span class=\"flag-icon flag-icon-fr\"></span>\r\n                   </div>\r\n                   <div class=\"dropdown-item\">\r\n                      <i class=\"flag-icon flag-icon-es\"></i>\r\n                   </div>\r\n                   <div class=\"dropdown-item\">\r\n                      <i class=\"flag-icon flag-icon-us\"></i>\r\n                   </div>\r\n                   <div class=\"dropdown-item\">\r\n                      <i class=\"flag-icon flag-icon-it\"></i>\r\n                   </div>\r\n                </div>\r\n             </div>\r\n          </div>\r\n       </div>\r\n    </header>\r\n<!-- \r\n    <div class=\"breadcrumbs\" *ngIf=\"!isLoginPage\">\r\n        <div class=\"col-sm-4\">\r\n            <div class=\"page-header float-left\">\r\n                <div class=\"page-title\">\r\n                    <h1>Dashboard</h1>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-8\">\r\n            <div class=\"page-header float-right\">\r\n                <div class=\"page-title\">\r\n                    <ol class=\"breadcrumb text-right\">\r\n                        <li class=\"active\">Dashboard</li>\r\n                    </ol>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n -->\r\n    <div [ngClass]=\"{'content mt-3': !isLoginPage}\">\r\n      \r\n      <router-outlet></router-outlet>\r\n    </div>\r\n  </div>  \r\n</div>"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sdk_index__ = __webpack_require__("../../../../../src/app/sdk/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppComponent = (function () {
    function AppComponent(router, auth, peopleApi) {
        this.router = router;
        this.auth = auth;
        this.peopleApi = peopleApi;
        this.isLoginPage = true;
        __WEBPACK_IMPORTED_MODULE_2__sdk_index__["c" /* LoopBackConfig */].setBaseURL('https://api.mybebamzigo.com');
        // LoopBackConfig.setBaseURL('http://localhost:3000');
        __WEBPACK_IMPORTED_MODULE_2__sdk_index__["c" /* LoopBackConfig */].setApiVersion('api');
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.router.events.subscribe(function (evt) {
            if (!(evt instanceof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* NavigationEnd */])) {
                return;
            }
            _this.userAcessToken = _this.auth.getAccessTokenId();
            if (evt.url == "/login") {
                _this.isLoginPage = true;
                if (!!_this.userAcessToken) {
                    // console.log("come in this part")
                    _this.isLoginPage = false;
                    _this.router.navigate(['/']);
                }
            }
            else {
                // console.log(evt.url)
                _this.isLoginPage = false;
                if (!_this.userAcessToken)
                    _this.router.navigate(['/login']);
            }
            // console.log(this.isLoginPage);
        });
    };
    AppComponent.prototype.logout = function () {
        var _this = this;
        this.peopleApi.logout().subscribe(function (success) {
            _this.router.navigate(['/login']);
        }, function (error) {
            if (error.statusCode == 401) {
                _this.router.navigate(['/login']);
            }
        });
    };
    return AppComponent;
}());
AppComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-root',
        template: __webpack_require__("../../../../../src/app/app.component.html"),
        styles: [__webpack_require__("../../../../../src/app/app.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__sdk_index__["b" /* LoopBackAuth */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__sdk_index__["b" /* LoopBackAuth */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__sdk_index__["d" /* PeopleApi */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__sdk_index__["d" /* PeopleApi */]) === "function" && _c || Object])
], AppComponent);

var _a, _b, _c;
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__routing_routing_module__ = __webpack_require__("../../../../../src/app/routing/routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__sdk_index__ = __webpack_require__("../../../../../src/app/sdk/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__agm_core__ = __webpack_require__("../../../../@agm/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__component_dashboard_dashboard_component__ = __webpack_require__("../../../../../src/app/component/dashboard/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__component_login_login_component__ = __webpack_require__("../../../../../src/app/component/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__component_vehicle_list_vehicle_list_component__ = __webpack_require__("../../../../../src/app/component/vehicle-list/vehicle-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__component_vehicle_details_vehicle_details_component__ = __webpack_require__("../../../../../src/app/component/vehicle-details/vehicle-details.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__component_user_list_user_list_component__ = __webpack_require__("../../../../../src/app/component/user-list/user-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__component_user_details_user_details_component__ = __webpack_require__("../../../../../src/app/component/user-details/user-details.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__component_job_list_job_list_component__ = __webpack_require__("../../../../../src/app/component/job-list/job-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__component_add_job_add_job_component__ = __webpack_require__("../../../../../src/app/component/add-job/add-job.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__component_add_user_add_user_component__ = __webpack_require__("../../../../../src/app/component/add-user/add-user.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__services_default_params_default_params_service__ = __webpack_require__("../../../../../src/app/services/default-params/default-params.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__services_multipart_multipart_service__ = __webpack_require__("../../../../../src/app/services/multipart/multipart.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__services_direction_direction_service__ = __webpack_require__("../../../../../src/app/services/direction/direction.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
// --------------- Modules ------------------------------






// --------------- Components ------------------------------










// --------------- Provider ------------------------------



var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["M" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_7__component_dashboard_dashboard_component__["a" /* DashboardComponent */],
            __WEBPACK_IMPORTED_MODULE_8__component_login_login_component__["a" /* LoginComponent */],
            __WEBPACK_IMPORTED_MODULE_9__component_vehicle_list_vehicle_list_component__["a" /* VehicleListComponent */],
            __WEBPACK_IMPORTED_MODULE_10__component_vehicle_details_vehicle_details_component__["a" /* VehicleDetailsComponent */],
            __WEBPACK_IMPORTED_MODULE_11__component_user_list_user_list_component__["a" /* UserListComponent */],
            __WEBPACK_IMPORTED_MODULE_12__component_user_details_user_details_component__["a" /* UserDetailsComponent */],
            __WEBPACK_IMPORTED_MODULE_13__component_job_list_job_list_component__["a" /* JobListComponent */],
            __WEBPACK_IMPORTED_MODULE_14__component_add_job_add_job_component__["a" /* AddJobComponent */],
            __WEBPACK_IMPORTED_MODULE_15__component_add_user_add_user_component__["a" /* AddUserComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2__routing_routing_module__["a" /* RoutingModule */],
            __WEBPACK_IMPORTED_MODULE_3__sdk_index__["e" /* SDKBrowserModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_5__agm_core__["a" /* AgmCoreModule */].forRoot({
                apiKey: 'AIzaSyDll9PQUPlUyzG5oknwZ1YnZv83sGY51FQ',
                libraries: ["places"]
            })
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_16__services_default_params_default_params_service__["a" /* DefaultParamsService */],
            __WEBPACK_IMPORTED_MODULE_17__services_multipart_multipart_service__["a" /* MultipartService */],
            __WEBPACK_IMPORTED_MODULE_5__agm_core__["b" /* GoogleMapsAPIWrapper */],
            __WEBPACK_IMPORTED_MODULE_18__services_direction_direction_service__["a" /* DirectionService */]
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/app/component/add-job/add-job.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".selected-vehicle{\r\n\tborder:solid!important;\r\n\tcolor:#fd7e14!important;\r\n}\r\n\r\n.routes-margin{\r\n    margin-left: 0px;\r\n    margin-right: 0px;\r\n}\r\n\r\n.route-img{\r\n\twidth:120px!important;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/component/add-job/add-job.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\r\n    <div class=\"row\">\r\n       <div class=\"col-lg-12\">\r\n          <div class=\"card\">\r\n             <div class=\"card-header\">\r\n                <strong class=\"card-title\">Job Posting</strong>\r\n             </div>\r\n             <div class=\"card-body\">\r\n               <div class=\"tab-pane fade show active\" id=\"home\" role=\"tabpanel\" aria-labelledby=\"home-tab\">\r\n                  <div class=\"col-lg-6\">\r\n                     <div id=\"firstForm\" [hidden]=\"showFormValue != 'firstForm'\">\r\n                        <form action=\"\" method=\"post\" novalidate=\"novalidate\">\r\n                            <div class=\"radio-button\">\r\n                              <label class=\"radio-btn\" id=\"Vehicle-box\" >Vehicle\r\n                                <input type=\"radio\" (change)=\"changeRequireType()\" name=\"radio\" id=\"customer\" value=\"Vehicle\" [(ngModel)]=\"job.requiredType\" />\r\n                                <span class=\"checkmark radio-btn1\"></span>\r\n                              </label>\r\n                              <label class=\"radio-btn\" [hidden]=\"job.typeOfJob=='on demand'\" id=\"Machinery-box\">Machinery\r\n                                <input type=\"radio\"  (change)=\"changeRequireType()\" name=\"radio\" id=\"driver\" value=\"Machinery\" [(ngModel)]=\"job.requiredType\" />\r\n                                <span class=\"checkmark radio-btn1\"></span>\r\n                              </label>\r\n                            </div>\r\n                            <div style=\"clear: both;\"></div>\r\n                        \r\n                             <br />\r\n                             <div class=\"form-group\">\r\n                                <label>Pickup Location</label>\r\n                                <input class=\"form-control\"  required type=\"text\" autocorrect=\"off\" autocapitalize=\"off\" spellcheck=\"off\" id=\"pickupAutocomplete\" name=\"pickupAutocomplete\" placeholder=\"Pickup Location\">\r\n\r\n                                <div class=\"error-div\" *ngIf=\"firstFormError.pickLocation && isFormValidate.firstFormSubmit\" style=\"margin-top:10px;\" >\r\n                                  Please select valid location\r\n                                </div>\r\n                             </div>\r\n                             <div class=\"form-group\" [hidden]=\"job.requiredType == 'Machinery'\"> \r\n                                <label>Drop Location</label>\r\n                                <input class=\"form-control\"  required type=\"text\" autocorrect=\"off\" autocapitalize=\"off\" spellcheck=\"off\" id=\"dropAutocomplete\" name=\"dropAutocomplete\" placeholder=\"Drop Location\">\r\n\r\n                                <div class=\"error-div\" *ngIf=\"firstFormError.dropLocation && isFormValidate.firstFormSubmit\" style=\"margin-top:10px;\" >\r\n                                  Please select valid location\r\n                                </div>\r\n                             </div>\r\n                             <div class=\"form-group\">\r\n                                <label>Date</label>\r\n                                <input class=\"form-control\" id=\"date\" name=\"date\" placeholder=\"Date\" type=\"text\" readonly/>\r\n                                <div class=\"error-div\"  *ngIf=\"firstFormError.date && isFormValidate.firstFormSubmit\" style=\"margin-top:10px;\" >\r\n                                  Date is required \r\n                                </div>\r\n                             </div>\r\n                             <div class=\"form-group\">\r\n                                <label>Time</label>\r\n                                <input class=\"form-control\" id=\"time\" name=\"time\" placeholder=\"Time\" type=\"text\" readonly  />\r\n                                <div class=\"error-div\" *ngIf=\"firstFormError.startTime && isFormValidate.firstFormSubmit\" style=\"margin-top:10px;\" >\r\n                                  Time is required \r\n                                </div>\r\n                             </div>\r\n                             <div class=\"form-group\" [hidden]=\"job.requiredType=='Vehicle'\">\r\n                                <label>End Date</label>\r\n                                <input class=\"form-control\" id=\"endDate\" name=\"endDate\" placeholder=\"End Date\" type=\"text\" readonly/>\r\n                                <div class=\"error-div\" *ngIf=\"firstFormError.endDate && isFormValidate.firstFormSubmit\" style=\"margin-top:10px;\" >\r\n                                  Date is required \r\n                                </div>\r\n                             </div>\r\n                             <div class=\"form-group\" [hidden]=\"job.requiredType=='Vehicle'\">\r\n                                <label>End Time</label>\r\n                                <input class=\"form-control\" id=\"endTime\" name=\"endTime\" placeholder=\"End Time\" type=\"text\" readonly  />\r\n                                <div class=\"error-div\" *ngIf=\"firstFormError.endTime && isFormValidate.firstFormSubmit\" style=\"margin-top:10px;\" >\r\n                                  Time is required \r\n                                </div>\r\n                             </div>\r\n\r\n                              <div style=\"clear: both;\"></div>\r\n                              <div class=\"vehicle-selection\">\r\n                                <div class=\"vehicle-list\" id=\"Vehicle-icon\">\r\n                                    <p>Select Vehicle</p>\r\n                                    <div  class=\"hvrbox col-lg-4 col-md-4 col-sm-6\" *ngFor=\"let vehicleType of selectedVehicleTypes\">\r\n                                       <!-- <div  class=\"vehicle-img hvrbox-layer_bottom\"> -->\r\n                                        <div [ngClass]=\"{'selected-vehicle':job.typeOfVehicle === vehicleType.name,'vehicle-img':true, 'hvrbox-layer_bottom':true}\" > \r\n                                          <img class=\"img-responsive\" [src]=\"baseURL+vehicleType.imgUrl\">\r\n                                          <p>{{vehicleType.typeOfVehicle}}</p>\r\n                                          <div class=\"hvrbox-layer_top hvrbox-layer_slideup\">\r\n                                             <div class=\"hvrbox-text\">\r\n                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.\r\n                                                <a href=\"javascript:;\" (click)=\"selectVehicle(vehicleType.name)\" class=\"next-form\">select</a>\r\n                                             </div>\r\n                                          </div>\r\n                                       </div>\r\n                                    </div>\r\n\r\n                                </div>\r\n                                <br />\r\n                                <div class=\"error-div\" *ngIf=\"firstFormError.typeOfVehicle && isFormValidate.firstFormSubmit\" style=\"margin-top:10px;\" >\r\n                                      Time is required \r\n                                </div>\r\n                                <div style=\"clear: both;\"></div>\r\n                                <button id=\"payment-button\" type=\"button\" class=\"btn btn-lg btn-info btn-block next-form\" (click)=\"secondForm()\">\r\n                                 Next\r\n                                </button>\r\n                              </div>\r\n                        </form>\r\n                     </div>\r\n                     <div class=\"truck-booking\" id=\"truck2\" [hidden]=\"showFormValue != 'secondForm'\">\r\n                        <form class=\"contact100-form validate-form\">\r\n                           <div class=\"form-group\">\r\n                              <label>Job Title</label>\r\n                              <input name=\"job title\" type=\"text\" class=\"form-control\" aria-required=\"true\">\r\n                           </div>\r\n                           <div class=\"form-group\">\r\n                              <label>Description</label>\r\n                              <input name=\"job title\" type=\"text\" class=\"form-control\" aria-required=\"true\">\r\n                           </div>\r\n                            <p>List Of Possible Routs</p>\r\n                            <div class=\"row Possible-routs routes-margin\" *ngFor=\"let route of routes\">\r\n                               <a href=\"javascript:;\" class=\"Routs\">\r\n                                  <div class=\"col-md-4\">\r\n                                     <div class=\"map-img\">\r\n                                        <img class=\"img-responsive route-img\" src=\"assets/images/map.jpg\">\r\n                                     </div>\r\n                                  </div>\r\n                                  <div class=\"col-md-8\">\r\n                                     <div class=\"map-text\">\r\n                                        <h6>{{route.legs[0].distance.text}}</h6>\r\n                                        <p>{{route.legs[0].duration.text}}</p>\r\n                                        <p>via {{route.summary}}</p>\r\n                                     </div>\r\n                                  </div>\r\n                               </a>\r\n                            </div>\r\n                            <button id=\"payment-button\" type=\"button\" class=\"btn btn-lg btn-info btn-block next-form col-lg-6\" (click)=\"back()\">\r\n                                 Back\r\n                           </button>\r\n                            <button id=\"payment-button\" type=\"button\" class=\"btn btn-lg btn-info btn-block next-form col-lg-6\" (click)=\"thirdForm()\">Next</button>\r\n                        </form>\r\n                     </div>\r\n                     <div class=\"truck-booking\" id=\"kuch\" [hidden]=\"showFormValue != 'thirdForm'\">\r\n                        <form class=\"contact100-form validate-form\">\r\n                           <div class=\"form-group\">\r\n                              <label>Value Of Goods</label>\r\n                              <input name=\"job title\" type=\"text\" class=\"form-control\" aria-required=\"true\">\r\n                           </div>\r\n                           <div class=\"form-group\">\r\n                              <label>Nature Of Good</label>\r\n                              <input name=\"job title\" type=\"text\" class=\"form-control\" aria-required=\"true\">\r\n                           </div>\r\n                           <div class=\"form-group\">\r\n                              <label>Net Price</label>\r\n                              <input name=\"job title\" type=\"text\" class=\"form-control\" aria-required=\"true\">\r\n                           </div>\r\n                           <button id=\"payment-button\" type=\"button\" class=\"btn btn-lg btn-info btn-block next-form col-lg-6\" (click)=\"back()\">\r\n                                 Back\r\n                           </button>\r\n                           <button id=\"payment-button\" type=\"button\" class=\"btn btn-lg btn-info btn-block next-form col-lg-6\" (click)=\"secondForm()\">\r\n                                 Post\r\n                           </button>\r\n                        </form>\r\n                     </div>\r\n                  </div>\r\n                  <div class=\"col-lg-6\">\r\n                     <div class=\"demand-map\">\r\n                        <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3561.0912372245443!2d75.81046201555608!3d26.805223383174045!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x396db4f8c2aa0f2f%3A0x9b1fab8b662f3f36!2sAdvocosoft+IT+services+Pvt+Ltd!5e0!3m2!1sen!2sin!4v1518693227695\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>\r\n                     </div>\r\n                  </div>\r\n               </div>\r\n             </div>\r\n          </div>\r\n       </div>\r\n    </div>\r\n</div>\r\n\r\n\r\n<!-- <div class=\"animated fadeIn\">\r\n    <div class=\"row\">\r\n       <div class=\"col-lg-12\">\r\n          <div class=\"card\">\r\n             <div class=\"card-header\">\r\n                <h4>Job Posting</h4>\r\n             </div>\r\n             <div class=\"card-body\">\r\n                   <div class=\"tab-pane fade show active\" id=\"home\" role=\"tabpanel\" aria-labelledby=\"home-tab\">\r\n                      <div class=\"col-lg-6\">\r\n                         <div id=\"truck1\">\r\n                            <div class=\"radio-button\">\r\n                               <label class=\"radio-btn\" id=\"vehicle-box\">Vehicle\r\n                               <input type=\"radio\" name=\"radio\">\r\n                               <span class=\"checkmark\"></span>\r\n                               </label>\r\n                               <label class=\"radio-btn\" id=\"machinery-box\">Machinery\r\n                               <input type=\"radio\" name=\"radio\">\r\n                               <span class=\"checkmark\"></span>\r\n                               </label>\r\n                            </div>\r\n                            <div style=\"clear: both;\"></div>\r\n                            <form action=\"\" method=\"post\" novalidate=\"novalidate\">\r\n                               <div class=\"form-group\">\r\n                                  <label>Name</label>\r\n                                  <input name=\"name\" type=\"text\" class=\"form-control\" aria-required=\"true\">\r\n                               </div>\r\n                               <div class=\"form-group\">\r\n                                  <label>UserName</label>\r\n                                  <input name=\"user-name\" type=\"text\" class=\"form-control\">\r\n                               </div>\r\n                            </form>\r\n                            <div style=\"clear: both;\"></div>\r\n                            <div class=\"vehicle-selection\">\r\n                               <div class=\"vehicle-list\" id=\"Vehicle-icon\" style=\"display: none;\">\r\n                                  <p>Select Vehicle</p>\r\n                                  <div class=\"hvrbox col-lg-4 col-md-4 col-sm-6\">\r\n                                     <div class=\"vehicle-img hvrbox-layer_bottom\">\r\n                                        <img class=\"img-responsive\" src=\"assets/images/icon/1.png\">\r\n                                        <p>Ute</p>\r\n                                        <div class=\"hvrbox-layer_top hvrbox-layer_slideup\">\r\n                                           <div class=\"hvrbox-text\">\r\n                                              Lorem ipsum dolor sit amet, consectetur adipiscing elit.\r\n                                              <a href=\"#\" class=\"next-form\">select</a>\r\n                                           </div>\r\n                                        </div>\r\n                                     </div>\r\n                                  </div>\r\n                                  <div class=\"hvrbox col-lg-4 col-md-4 col-sm-6\">\r\n                                     <div class=\"vehicle-img hvrbox-layer_bottom\">\r\n                                        <img class=\"img-responsive\" src=\"assets/images/icon/2.png\">\r\n                                        <p>Ute</p>\r\n                                        <div class=\"hvrbox-layer_top hvrbox-layer_slideup\">\r\n                                           <div class=\"hvrbox-text\">\r\n                                              Lorem ipsum dolor sit amet, consectetur adipiscing elit.\r\n                                              <a href=\"#\" class=\"next-form\">select</a>\r\n                                           </div>\r\n                                        </div>\r\n                                     </div>\r\n                                  </div>\r\n                                  <div class=\"hvrbox col-lg-4 col-md-4 col-sm-6\">\r\n                                     <div class=\"vehicle-img hvrbox-layer_bottom\">\r\n                                        <img class=\"img-responsive\" src=\"assets/images/icon/3.png\">\r\n                                        <p>Ute</p>\r\n                                        <div class=\"hvrbox-layer_top hvrbox-layer_slideup\">\r\n                                           <div class=\"hvrbox-text\">\r\n                                              Lorem ipsum dolor sit amet, consectetur adipiscing elit.\r\n                                              <a href=\"#\" class=\"next-form\">select</a>\r\n                                           </div>\r\n                                        </div>\r\n                                     </div>\r\n                                  </div>\r\n                                  <div class=\"hvrbox col-lg-4 col-md-4 col-sm-6\">\r\n                                     <div class=\"vehicle-img hvrbox-layer_bottom\">\r\n                                        <img class=\"img-responsive\" src=\"assets/images/icon/4.png\">\r\n                                        <p>Ute</p>\r\n                                        <div class=\"hvrbox-layer_top hvrbox-layer_slideup\">\r\n                                           <div class=\"hvrbox-text\">\r\n                                              Lorem ipsum dolor sit amet, consectetur adipiscing elit.\r\n                                              <a href=\"#\" class=\"next-form\">select</a>\r\n                                           </div>\r\n                                        </div>\r\n                                     </div>\r\n                                  </div>\r\n                                  <div class=\"hvrbox col-lg-4 col-md-4 col-sm-6\">\r\n                                     <div class=\"vehicle-img hvrbox-layer_bottom\">\r\n                                        <img class=\"img-responsive\" src=\"assets/images/icon/5.png\">\r\n                                        <p>Ute</p>\r\n                                        <div class=\"hvrbox-layer_top hvrbox-layer_slideup\">\r\n                                           <div class=\"hvrbox-text\">\r\n                                              Lorem ipsum dolor sit amet, consectetur adipiscing elit.\r\n                                              <a href=\"#\" class=\"next-form\">select</a>\r\n                                           </div>\r\n                                        </div>\r\n                                     </div>\r\n                                  </div>\r\n                                  <div class=\"hvrbox col-lg-4 col-md-4 col-sm-6\">\r\n                                     <div class=\"vehicle-img hvrbox-layer_bottom\">\r\n                                        <img class=\"img-responsive\" src=\"assets/images/icon/6.png\">\r\n                                        <p>Ute</p>\r\n                                        <div class=\"hvrbox-layer_top hvrbox-layer_slideup\">\r\n                                           <div class=\"hvrbox-text\">\r\n                                              Lorem ipsum dolor sit amet, consectetur adipiscing elit.\r\n                                              <a href=\"#\" class=\"next-form\">select</a>\r\n                                           </div>\r\n                                        </div>\r\n                                     </div>\r\n                                  </div>\r\n                                  <div class=\"hvrbox col-lg-4 col-md-4 col-sm-6\">\r\n                                     <div class=\"vehicle-img hvrbox-layer_bottom\">\r\n                                        <img class=\"img-responsive\" src=\"assets/images/icon/7.png\">\r\n                                        <p>Ute</p>\r\n                                        <div class=\"hvrbox-layer_top hvrbox-layer_slideup\">\r\n                                           <div class=\"hvrbox-text\">\r\n                                              Lorem ipsum dolor sit amet, consectetur adipiscing elit.\r\n                                              <a href=\"\" class=\"next-form\">select</a>\r\n                                           </div>\r\n                                        </div>\r\n                                     </div>\r\n                                  </div>\r\n                               </div>\r\n                               <div class=\"vehicle-list\" id=\"Machinery-icon\" style=\"display: none;\">\r\n                                  <p>Select Machinery</p>\r\n                                  <div class=\"hvrbox col-lg-4 col-md-4 col-sm-6\">\r\n                                     <div class=\"vehicle-img hvrbox-layer_bottom\">\r\n                                        <img class=\"img-responsive\" src=\"assets/images/icon/1.png\">\r\n                                        <p>Ute</p>\r\n                                        <div class=\"hvrbox-layer_top hvrbox-layer_slideup\">\r\n                                           <div class=\"hvrbox-text\">\r\n                                              Lorem ipsum dolor sit amet, consectetur adipiscing elit.\r\n                                              <a href=\"#\" class=\"next-form\">select</a>\r\n                                           </div>\r\n                                        </div>\r\n                                     </div>\r\n                                  </div>\r\n                                  <div class=\"hvrbox col-lg-4 col-md-4 col-sm-6\">\r\n                                     <div class=\"vehicle-img hvrbox-layer_bottom\">\r\n                                        <img class=\"img-responsive\" src=\"assets/images/icon/2.png\">\r\n                                        <p>Ute</p>\r\n                                        <div class=\"hvrbox-layer_top hvrbox-layer_slideup\">\r\n                                           <div class=\"hvrbox-text\">\r\n                                              Lorem ipsum dolor sit amet, consectetur adipiscing elit.\r\n                                              <a href=\"#\" class=\"next-form\">select</a>\r\n                                           </div>\r\n                                        </div>\r\n                                     </div>\r\n                                  </div>\r\n                                  <div class=\"hvrbox col-lg-4 col-md-4 col-sm-6\">\r\n                                     <div class=\"vehicle-img hvrbox-layer_bottom\">\r\n                                        <img class=\"img-responsive\" src=\"assets/images/icon/3.png\">\r\n                                        <p>Ute</p>\r\n                                        <div class=\"hvrbox-layer_top hvrbox-layer_slideup\">\r\n                                           <div class=\"hvrbox-text\">\r\n                                              Lorem ipsum dolor sit amet, consectetur adipiscing elit.\r\n                                              <a href=\"#\" class=\"next-form\">select</a>\r\n                                           </div>\r\n                                        </div>\r\n                                     </div>\r\n                                  </div>\r\n                                  <div class=\"hvrbox col-lg-4 col-md-4 col-sm-6\">\r\n                                     <div class=\"vehicle-img hvrbox-layer_bottom\">\r\n                                        <img class=\"img-responsive\" src=\"assets/images/icon/4.png\">\r\n                                        <p>Ute</p>\r\n                                        <div class=\"hvrbox-layer_top hvrbox-layer_slideup\">\r\n                                           <div class=\"hvrbox-text\">\r\n                                              Lorem ipsum dolor sit amet, consectetur adipiscing elit.\r\n                                              <a href=\"#\" class=\"next-form\">select</a>\r\n                                           </div>\r\n                                        </div>\r\n                                     </div>\r\n                                  </div>\r\n                                  <div class=\"hvrbox col-lg-4 col-md-4 col-sm-6\">\r\n                                     <div class=\"vehicle-img hvrbox-layer_bottom\">\r\n                                        <img class=\"img-responsive\" src=\"assets/images/icon/5.png\">\r\n                                        <p>Ute</p>\r\n                                        <div class=\"hvrbox-layer_top hvrbox-layer_slideup\">\r\n                                           <div class=\"hvrbox-text\">\r\n                                              Lorem ipsum dolor sit amet, consectetur adipiscing elit.\r\n                                              <a href=\"#\" class=\"next-form\">select</a>\r\n                                           </div>\r\n                                        </div>\r\n                                     </div>\r\n                                  </div>\r\n                                  <div class=\"hvrbox col-lg-4 col-md-4 col-sm-6\">\r\n                                     <div class=\"vehicle-img hvrbox-layer_bottom\">\r\n                                        <img class=\"img-responsive\" src=\"assets/images/icon/6.png\">\r\n                                        <p>Ute</p>\r\n                                        <div class=\"hvrbox-layer_top hvrbox-layer_slideup\">\r\n                                           <div class=\"hvrbox-text\">\r\n                                              Lorem ipsum dolor sit amet, consectetur adipiscing elit.\r\n                                              <a href=\"#\" class=\"next-form\">select</a>\r\n                                           </div>\r\n                                        </div>\r\n                                     </div>\r\n                                  </div>\r\n                                  <div class=\"hvrbox col-lg-4 col-md-4 col-sm-6\">\r\n                                     <div class=\"vehicle-img hvrbox-layer_bottom\">\r\n                                        <img class=\"img-responsive\" src=\"assets/images/icon/7.png\">\r\n                                        <p>Ute</p>\r\n                                        <div class=\"hvrbox-layer_top hvrbox-layer_slideup\">\r\n                                           <div class=\"hvrbox-text\">\r\n                                              Lorem ipsum dolor sit amet, consectetur adipiscing elit.\r\n                                              <a href=\"\" class=\"next-form\">select</a>\r\n                                           </div>\r\n                                        </div>\r\n                                     </div>\r\n                                  </div>\r\n                               </div>\r\n                               <button id=\"payment-button\" type=\"submit\" class=\"btn btn-lg btn-info btn-block next-form\">\r\n                               Submit\r\n                               </button>\r\n                               <div style=\"clear: both;\"></div>\r\n                            </div>\r\n                         </div>\r\n                         <div class=\"truck-booking\" id=\"truck2\" style=\"display: none;\">\r\n                            <p>What Type of Goods</p>\r\n                            <form class=\"contact100-form validate-form\">\r\n                               <div class=\"form-group\">\r\n                                  <label>Job Title</label>\r\n                                  <input name=\"job title\" type=\"text\" class=\"form-control\" aria-required=\"true\">\r\n                               </div>\r\n                               <div class=\"form-group\">\r\n                                  <label>Total Weight</label>\r\n                                  <input name=\"job title\" type=\"text\" class=\"form-control\" aria-required=\"true\">\r\n                               </div>\r\n                               <div class=\"form-group\">\r\n                                  <label>Type Of Vehicle</label>\r\n                                  <input name=\"job title\" type=\"text\" class=\"form-control\" aria-required=\"true\">\r\n                               </div>\r\n                               <div class=\"form-group\">\r\n                                  <label>Source</label>\r\n                                  <input name=\"job title\" type=\"text\" class=\"form-control\" aria-required=\"true\">\r\n                               </div>\r\n                               <div class=\"form-group\">\r\n                                  <label>Destination</label>\r\n                                  <input name=\"job title\" type=\"text\" class=\"form-control\" aria-required=\"true\">\r\n                               </div>\r\n                            </form>\r\n                            <p>List Of Possible Routs</p>\r\n                            <div class=\"row Possible-routs\">\r\n                               <a href=\"#\" class=\"Routs\">\r\n                                  <div class=\"col-md-4\">\r\n                                     <div class=\"map-img\">\r\n                                        <img class=\"img-responsive\" src=\"assets/images/map.jpg\">\r\n                                     </div>\r\n                                  </div>\r\n                                  <div class=\"col-md-8\">\r\n                                     <div class=\"map-text\">\r\n                                        <h6>1150KM</h6>\r\n                                        <p>20 Hours</p>\r\n                                     </div>\r\n                                  </div>\r\n                               </a>\r\n                            </div>\r\n                            <div class=\"row Possible-routs\">\r\n                               <a href=\"#\" class=\"Routs\">\r\n                                  <div class=\"col-md-4\">\r\n                                     <div class=\"map-img\">\r\n                                        <img class=\"img-responsive\" src=\"assets/images/map.jpg\">\r\n                                     </div>\r\n                                  </div>\r\n                                  <div class=\"col-md-8\">\r\n                                     <div class=\"map-text\">\r\n                                        <h6>1150KM</h6>\r\n                                        <p>20 Hours</p>\r\n                                     </div>\r\n                                  </div>\r\n                               </a>\r\n                            </div>\r\n                            <div class=\"row Possible-routs\">\r\n                               <a href=\"#\" class=\"Routs\">\r\n                                  <div class=\"col-md-4\">\r\n                                     <div class=\"map-img\">\r\n                                        <img class=\"img-responsive\" src=\"assets/images/map.jpg\">\r\n                                     </div>\r\n                                  </div>\r\n                                  <div class=\"col-md-8\">\r\n                                     <div class=\"map-text\">\r\n                                        <h6>1150KM</h6>\r\n                                        <p>20 Hours</p>\r\n                                     </div>\r\n                                  </div>\r\n                               </a>\r\n                            </div>\r\n                         </div>\r\n                         <div class=\"truck-booking\" id=\"kuch\" style=\"display: none;\">\r\n                            <p>Find Best Route</p>\r\n                            <form class=\"contact100-form validate-form\">\r\n                               <div class=\"form-group\">\r\n                                  <label>Total Distance</label>\r\n                                  <input name=\"job title\" type=\"text\" class=\"form-control\" aria-required=\"true\">\r\n                               </div>\r\n                               <div class=\"form-group\">\r\n                                  <label>Total Time</label>\r\n                                  <input name=\"job title\" type=\"text\" class=\"form-control\" aria-required=\"true\">\r\n                               </div>\r\n                               <div class=\"form-group\">\r\n                                  <label>Total Cost</label>\r\n                                  <input name=\"job title\" type=\"text\" class=\"form-control\" aria-required=\"true\">\r\n                               </div>\r\n                               <div class=\"form-group\">\r\n                                  <label>Value Of Goods</label>\r\n                                  <input name=\"job title\" type=\"text\" class=\"form-control\" aria-required=\"true\">\r\n                               </div>\r\n                               <div class=\"form-group\">\r\n                                  <label>Nature Of Good</label>\r\n                                  <input name=\"job title\" type=\"text\" class=\"form-control\" aria-required=\"true\">\r\n                               </div>\r\n                               <div class=\"form-group\">\r\n                                  <label>Net Price</label>\r\n                                  <input name=\"job title\" type=\"text\" class=\"form-control\" aria-required=\"true\">\r\n                               </div>\r\n                               <div class=\"col-md-6\">\r\n                                  <div class=\"submit-btn\">\r\n                                     <a href=\"#\">Post</a>\r\n                                  </div>\r\n                               </div>\r\n                               <div class=\"col-md-6\">\r\n                                  <div class=\"submit-btn\">\r\n                                     <a href=\"#\">Back</a>\r\n                                  </div>\r\n                               </div>\r\n                            </form>\r\n                         </div>\r\n                      </div>\r\n                      <div class=\"col-lg-6\">\r\n                         <div class=\"demand-map\">\r\n                            <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3561.0912372245443!2d75.81046201555608!3d26.805223383174045!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x396db4f8c2aa0f2f%3A0x9b1fab8b662f3f36!2sAdvocosoft+IT+services+Pvt+Ltd!5e0!3m2!1sen!2sin!4v1518693227695\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>\r\n                         </div>\r\n                      </div>\r\n                   </div>\r\n                   <div class=\"tab-pane fade\" id=\"profile\" role=\"tabpanel\" aria-labelledby=\"profile-tab\">\r\n                      <div class=\"col-lg-6\">\r\n                         <div id=\"truck3\">\r\n                            <div style=\"clear: both;\"></div>\r\n                            <form action=\"\" method=\"post\" novalidate=\"novalidate\">\r\n                               <div class=\"form-group\">\r\n                                  <label>Name</label>\r\n                                  <input name=\"name\" type=\"text\" class=\"form-control\" aria-required=\"true\">\r\n                               </div>\r\n                               <div class=\"form-group\">\r\n                                  <label>UserName</label>\r\n                                  <input name=\"user-name\" type=\"text\" class=\"form-control\">\r\n                               </div>\r\n                            </form>\r\n                            <div style=\"clear: both;\"></div>\r\n                            <div class=\"vehicle-selection\">\r\n                               <div class=\"vehicle-list\" id=\"Vehicle-icon\">\r\n                                  <p>Select Vehicle</p>\r\n                                  <div class=\"hvrbox col-lg-4 col-md-4 col-sm-6\">\r\n                                     <div class=\"vehicle-img hvrbox-layer_bottom\">\r\n                                        <img class=\"img-responsive\" src=\"assets/images/icon/1.png\">\r\n                                        <p>Ute</p>\r\n                                        <div class=\"hvrbox-layer_top hvrbox-layer_slideup\">\r\n                                           <div class=\"hvrbox-text\">\r\n                                              Lorem ipsum dolor sit amet, consectetur adipiscing elit.\r\n                                              <a href=\"#\" class=\"next-form\">select</a>\r\n                                           </div>\r\n                                        </div>\r\n                                     </div>\r\n                                  </div>\r\n                                  <div class=\"hvrbox col-lg-4 col-md-4 col-sm-6\">\r\n                                     <div class=\"vehicle-img hvrbox-layer_bottom\">\r\n                                        <img class=\"img-responsive\" src=\"assets/images/icon/2.png\">\r\n                                        <p>Ute</p>\r\n                                        <div class=\"hvrbox-layer_top hvrbox-layer_slideup\">\r\n                                           <div class=\"hvrbox-text\">\r\n                                              Lorem ipsum dolor sit amet, consectetur adipiscing elit.\r\n                                              <a href=\"#\" class=\"next-form\">select</a>\r\n                                           </div>\r\n                                        </div>\r\n                                     </div>\r\n                                  </div>\r\n                                  <div class=\"hvrbox col-lg-4 col-md-4 col-sm-6\">\r\n                                     <div class=\"vehicle-img hvrbox-layer_bottom\">\r\n                                        <img class=\"img-responsive\" src=\"assets/images/icon/3.png\">\r\n                                        <p>Ute</p>\r\n                                        <div class=\"hvrbox-layer_top hvrbox-layer_slideup\">\r\n                                           <div class=\"hvrbox-text\">\r\n                                              Lorem ipsum dolor sit amet, consectetur adipiscing elit.\r\n                                              <a href=\"#\" class=\"next-form\">select</a>\r\n                                           </div>\r\n                                        </div>\r\n                                     </div>\r\n                                  </div>\r\n                                  <div class=\"hvrbox col-lg-4 col-md-4 col-sm-6\">\r\n                                     <div class=\"vehicle-img hvrbox-layer_bottom\">\r\n                                        <img class=\"img-responsive\" src=\"assets/images/icon/4.png\">\r\n                                        <p>Ute</p>\r\n                                        <div class=\"hvrbox-layer_top hvrbox-layer_slideup\">\r\n                                           <div class=\"hvrbox-text\">\r\n                                              Lorem ipsum dolor sit amet, consectetur adipiscing elit.\r\n                                              <a href=\"#\" class=\"next-form\">select</a>\r\n                                           </div>\r\n                                        </div>\r\n                                     </div>\r\n                                  </div>\r\n                                  <div class=\"hvrbox col-lg-4 col-md-4 col-sm-6\">\r\n                                     <div class=\"vehicle-img hvrbox-layer_bottom\">\r\n                                        <img class=\"img-responsive\" src=\"assets/images/icon/5.png\">\r\n                                        <p>Ute</p>\r\n                                        <div class=\"hvrbox-layer_top hvrbox-layer_slideup\">\r\n                                           <div class=\"hvrbox-text\">\r\n                                              Lorem ipsum dolor sit amet, consectetur adipiscing elit.\r\n                                              <a href=\"#\" class=\"next-form\">select</a>\r\n                                           </div>\r\n                                        </div>\r\n                                     </div>\r\n                                  </div>\r\n                                  <div class=\"hvrbox col-lg-4 col-md-4 col-sm-6\">\r\n                                     <div class=\"vehicle-img hvrbox-layer_bottom\">\r\n                                        <img class=\"img-responsive\" src=\"assets/images/icon/6.png\">\r\n                                        <p>Ute</p>\r\n                                        <div class=\"hvrbox-layer_top hvrbox-layer_slideup\">\r\n                                           <div class=\"hvrbox-text\">\r\n                                              Lorem ipsum dolor sit amet, consectetur adipiscing elit.\r\n                                              <a href=\"#\" class=\"next-form\">select</a>\r\n                                           </div>\r\n                                        </div>\r\n                                     </div>\r\n                                  </div>\r\n                                  <div class=\"hvrbox col-lg-4 col-md-4 col-sm-6\">\r\n                                     <div class=\"vehicle-img hvrbox-layer_bottom\">\r\n                                        <img class=\"img-responsive\" src=\"assets/images/icon/7.png\">\r\n                                        <p>Ute</p>\r\n                                        <div class=\"hvrbox-layer_top hvrbox-layer_slideup\">\r\n                                           <div class=\"hvrbox-text\">\r\n                                              Lorem ipsum dolor sit amet, consectetur adipiscing elit.\r\n                                              <a href=\"\" class=\"next-form\">select</a>\r\n                                           </div>\r\n                                        </div>\r\n                                     </div>\r\n                                  </div>\r\n                               </div>\r\n                               <button id=\"payment-button\" type=\"submit\" class=\"btn btn-lg btn-info btn-block next-form1\">\r\n                               Submit\r\n                               </button>\r\n                               <div style=\"clear: both;\"></div>\r\n                            </div>\r\n                         </div>\r\n                         <div class=\"truck-booking\" id=\"truck4\" style=\"display: none;\">\r\n                            <p>What Type of Goods</p>\r\n                            <form class=\"contact100-form validate-form\">\r\n                               <div class=\"form-group\">\r\n                                  <label>Job Title</label>\r\n                                  <input name=\"job title\" type=\"text\" class=\"form-control\" aria-required=\"true\">\r\n                               </div>\r\n                               <div class=\"form-group\">\r\n                                  <label>Total Weight</label>\r\n                                  <input name=\"job title\" type=\"text\" class=\"form-control\" aria-required=\"true\">\r\n                               </div>\r\n                               <div class=\"form-group\">\r\n                                  <label>Type of Vehicle</label>\r\n                                  <input name=\"job title\" type=\"text\" class=\"form-control\" aria-required=\"true\">\r\n                               </div>\r\n                               <div class=\"form-group\">\r\n                                  <label>Source</label>\r\n                                  <input name=\"job title\" type=\"text\" class=\"form-control\" aria-required=\"true\">\r\n                               </div>\r\n                               <div class=\"form-group\">\r\n                                  <label>Destination</label>\r\n                                  <input name=\"job title\" type=\"text\" class=\"form-control\" aria-required=\"true\">\r\n                               </div>\r\n                               <div class=\"form-group\">\r\n                                  <label>Date & Time</label>\r\n                                  <input name=\"job title\" type=\"text\" class=\"form-control\" aria-required=\"true\">\r\n                               </div>\r\n                               <div class=\"form-group\">\r\n                                  <label>Nature Of Good</label>\r\n                                  <input name=\"job title\" type=\"text\" class=\"form-control\" aria-required=\"true\">\r\n                               </div>\r\n                               <div class=\"form-group\">\r\n                                  <label>Value Of Good</label>\r\n                                  <input name=\"job title\" type=\"text\" class=\"form-control\" aria-required=\"true\">\r\n                               </div>\r\n                               <div class=\"form-group\">\r\n                                  <label>Your Budget</label>\r\n                                  <input name=\"job title\" type=\"text\" class=\"form-control\" aria-required=\"true\">\r\n                               </div>\r\n                               <div class=\"col-md-12\">\r\n                                  <div class=\"submit-btn\">\r\n                                     <a href=\"#\">Post</a> \r\n                                  </div>\r\n                               </div>\r\n                            </form>\r\n                         </div>\r\n                      </div>\r\n                      <div class=\"col-lg-6\">\r\n                          <div class=\"standard-img\">\r\n                              <img class=\"img-responsive\" src=\"assets/images/truck-8.jpg\">\r\n                          </div>\r\n                      </div>\r\n                   </div>\r\n             </div>\r\n          </div>\r\n       </div>\r\n    </div>\r\n</div> -->"

/***/ }),

/***/ "../../../../../src/app/component/add-job/add-job.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddJobComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__agm_core__ = __webpack_require__("../../../../@agm/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sdk__ = __webpack_require__("../../../../../src/app/sdk/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_direction_direction_service__ = __webpack_require__("../../../../../src/app/services/direction/direction.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AddJobComponent = (function () {
    function AddJobComponent(directionService, ref, router, route, jobApi, vehicleTypeApi, gMaps, mapsAPILoader) {
        this.directionService = directionService;
        this.ref = ref;
        this.router = router;
        this.route = route;
        this.jobApi = jobApi;
        this.vehicleTypeApi = vehicleTypeApi;
        this.gMaps = gMaps;
        this.mapsAPILoader = mapsAPILoader;
        this.job = {};
        this.vehicleTypes = [];
        this.selectedVehicleTypes = [];
        this.params = {};
        this.showFormValue = "firstForm";
        this.routes = [];
        this.isFormValidate = {
            firstForm: true,
            firstFormSubmit: false,
            secondForm: true,
            secondFormSubmit: false,
            thirdForm: true,
            thirdFormSubmit: false
        };
        this.firstFormError = {
            pickLocation: false,
            dropLocation: false,
            startDate: false,
            startTime: false,
            endDate: false,
            endTime: false,
            typeOfVehicle: false
        };
        this.secondFormError = {
            title: false,
            description: false,
            selectedRoute: false,
        };
        this.thirdformError = {};
        this.selectedRoute = {};
    }
    AddJobComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.params = params;
            _this.job.typeOfJob = _this.params.type == "on-demand" ? "on demand" : "standard rate";
            _this.getVehicleType(_this.job.typeOfJob);
            _this.job.requiredType = "Vehicle";
        });
        this.baseURL = __WEBPACK_IMPORTED_MODULE_2__sdk__["c" /* LoopBackConfig */].getPath();
        this.mapsAPILoader.load().then(function () {
            _this.initializePickupMap();
            _this.initializeDropMap();
        });
        this.initializeDatepicker();
        this.initializeClockpicker();
        this.initializeDatepicker1();
        this.initializeClockpicker1();
    };
    AddJobComponent.prototype.back = function () {
        if (this.showFormValue === "secondForm") {
            this.showFormValue = "firstForm";
        }
        else {
            this.showFormValue = "secondForm";
        }
        this.ref.detectChanges();
    };
    AddJobComponent.prototype.selectVehicle = function (typeOfVehicle) {
        this.job.typeOfVehicle = typeOfVehicle;
        this.firstFormValidator();
    };
    AddJobComponent.prototype.getVehicleType = function (typeOfJob) {
        var _this = this;
        this.vehicleTypeApi.getVehicleType(undefined, typeOfJob).subscribe(function (vehicleTypes) {
            _this.vehicleTypes = vehicleTypes.success.data;
            if (_this.job.requiredType == "Vehicle")
                _this.selectedVehicleTypes = _this.vehicleTypes.vehicle;
            else
                _this.selectedVehicleTypes = _this.vehicleTypes.machinery;
            _this.selectVehicle(undefined);
        }, function (error) {
        });
    };
    AddJobComponent.prototype.changeRequireType = function () {
        if (this.job.requiredType == "Vehicle")
            this.selectedVehicleTypes = this.vehicleTypes.vehicle;
        else
            this.selectedVehicleTypes = this.vehicleTypes.machinery;
        this.selectVehicle(undefined);
    };
    AddJobComponent.prototype.firstForm = function () {
        this.showFormValue = "firstForm";
    };
    AddJobComponent.prototype.firstFormValidator = function () {
        if (!this.isFormValidate.firstFormSubmit)
            return;
        this.isFormValidate.firstForm = true;
        if (typeof this.job.source == 'object' && this.job.source.address != "" && this.job.source.location.lat != undefined && this.job.source.location.lng != undefined) {
            this.firstFormError.pickLocation = false;
        }
        else {
            console.log("come in second part");
            this.firstFormError.pickLocation = true;
            this.isFormValidate.firstForm = false;
        }
        if (this.job.requiredType == "Machinery" || (typeof this.job.destination == 'object' && this.job.destination.address != "" && this.job.destination.location.lat != undefined && this.job.destination.location.lng != undefined)) {
            this.firstFormError.dropLocation = false;
        }
        else {
            this.firstFormError.dropLocation = true;
            this.isFormValidate.firstForm = false;
        }
        if (this.job.time != undefined && this.job.time != "" && this.job.time != null) {
            this.firstFormError.startTime = false;
        }
        else {
            this.firstFormError.startTime = true;
            this.isFormValidate.firstForm = false;
        }
        if (this.job.date != undefined && this.job.date != "" && this.job.date != null) {
            this.firstFormError.date = false;
        }
        else {
            this.firstFormError.date = true;
            this.isFormValidate.firstForm = false;
        }
        if (this.job.requiredType == "Vehicle" || (this.job.endDate != undefined && this.job.endDate != "" && this.job.endDate != null)) {
            this.firstFormError.endDate = false;
        }
        else {
            this.firstFormError.endDate = true;
            this.isFormValidate.firstForm = false;
        }
        if (this.job.requiredType == "Vehicle" || (this.job.endTime != undefined && this.job.endTime != "" && this.job.endTime != null)) {
            this.firstFormError.endTime = false;
        }
        else {
            this.firstFormError.endTime = true;
            this.isFormValidate.firstForm = false;
        }
        if (this.job.typeOfVehicle != undefined && this.job.typeOfVehicle != "" && this.job.typeOfVehicle != null) {
            this.firstFormError.typeOfVehicle = false;
        }
        else {
            this.firstFormError.typeOfVehicle = true;
            this.isFormValidate.firstForm = false;
        }
        this.ref.detectChanges();
    };
    AddJobComponent.prototype.selectRoute = function (route) {
        this.selectedRoute = route;
        console.log(this.selectedRoute);
    };
    AddJobComponent.prototype.secondForm = function () {
        var _self = this;
        this.isFormValidate.firstFormSubmit = true;
        this.firstFormValidator();
        if (this.isFormValidate.firstForm) {
            if (this.job.requiredType == "Vehicle") {
                this.getDirection(this.job.source.location, this.job.destination.location, function (error, routes) {
                    if (error) {
                    }
                    else {
                        _self.routes = routes;
                        _self.showFormValue = "secondForm";
                        console.log(_self.routes);
                    }
                    _self.ref.detectChanges();
                });
            }
            else {
                this.routes = [];
                this.showFormValue = "secondForm";
            }
        }
    };
    AddJobComponent.prototype.secondFormValidator = function () {
        if (!this.isFormValidate.secondFormSubmit)
            return;
        this.isFormValidate.secondForm = true;
        if (this.job.source.title != "" && this.job.title != undefined) {
            this.firstFormError.title = false;
        }
        else {
            this.firstFormError.title = true;
            this.isFormValidate.secondForm = false;
        }
    };
    AddJobComponent.prototype.thirdForm = function () {
        this.showFormValue = "thirdForm";
    };
    AddJobComponent.prototype.initializePickupMap = function () {
        var _this = this;
        var options = {
            componentRestrictions: { country: 'ke' }
        };
        var pickupAutocomplete = new google.maps.places.Autocomplete(document.getElementById("pickupAutocomplete"), options);
        pickupAutocomplete.addListener('place_changed', function () {
            _this.job.source = {};
            var place = pickupAutocomplete.getPlace();
            _this.job.source = {
                address: place.formatted_address,
                location: {
                    lat: place.geometry.location.lat(),
                    lng: place.geometry.location.lng()
                }
            };
            _this.firstFormValidator();
        });
    };
    AddJobComponent.prototype.initializeDropMap = function () {
        var _this = this;
        var _self = this;
        var options = {
            componentRestrictions: { country: 'ke' }
        };
        var pickupAutocomplete = new google.maps.places.Autocomplete(document.getElementById("dropAutocomplete"), options);
        pickupAutocomplete.addListener('place_changed', function () {
            _this.job.destination = {};
            var place = pickupAutocomplete.getPlace();
            _this.job.destination = {
                address: place.formatted_address,
                location: {
                    lat: place.geometry.location.lat(),
                    lng: place.geometry.location.lng()
                }
            };
            _self.firstFormValidator();
        });
    };
    AddJobComponent.prototype.initializeDatepicker = function () {
        var _this = this;
        setTimeout(function () {
            var date_input = $('#date'); //our date input has the name "date"
            var _self = _this;
            date_input.datepicker({
                format: 'D d. M yyyy',
                todayHighlight: true,
                autoclose: true,
                numberOfMonths: 1,
                defaultDate: "+1d",
                startDate: '+0d',
                endDate: '+30d'
            }).on('changeDate', function (e) {
                _self.job.date = e.date;
                _self.firstFormValidator();
            });
        }, 0);
    };
    AddJobComponent.prototype.initializeClockpicker = function () {
        var _self = this;
        setTimeout(function () {
            $('#time').clockpicker({
                'default': 'now',
                fromnow: 0,
                placement: 'bottom',
                align: 'left',
                donetext: 'Done',
                autoclose: true,
                twelvehour: false,
                vibrate: false,
                afterDone: function () {
                    _self.job.time = $("#time")[0].value;
                    _self.firstFormValidator();
                }
            });
        }, 0);
    };
    AddJobComponent.prototype.initializeDatepicker1 = function () {
        var _this = this;
        setTimeout(function () {
            var date_input = $('#endDate'); //our date input has the name "date"
            var _self = _this;
            date_input.datepicker({
                format: 'D d. M yyyy',
                todayHighlight: true,
                autoclose: true,
                numberOfMonths: 1,
                defaultDate: "+1d",
                startDate: '+0d',
                endDate: '+30d'
            }).on('changeDate', function (e) {
                _self.job.date = e.date;
                _self.firstFormValidator();
            });
        }, 0);
    };
    AddJobComponent.prototype.initializeClockpicker1 = function () {
        var _self = this;
        setTimeout(function () {
            $('#endTime').clockpicker({
                'default': 'now',
                fromnow: 0,
                placement: 'bottom',
                align: 'left',
                donetext: 'Done',
                autoclose: true,
                twelvehour: false,
                vibrate: false,
                afterDone: function () {
                    _self.job.endTime = $("#endTime")[0].value;
                    _self.firstFormValidator();
                }
            });
        }, 0);
    };
    AddJobComponent.prototype.getDirection = function (source, destination, cb) {
        var _self = this;
        this.directionService.getDirection(source, destination, function (response, status) {
            if (status === 'OK') {
                _self.routes = response.routes;
                console.log(_self.routes);
                console.log("routes : ", _self.routes);
                cb(null, response.routes);
                // _self.setPosition();
            }
            else {
                cb("error", null);
            }
        });
    };
    /*firstFormError: any= {
        pickLocation:false,
        dropLocation:false,
        startDate:false,
        startTime:false,
        endDate:false,
        endTime:false,
        typeOfVehicle:false
    }*/
    AddJobComponent.prototype.thirdFormValidator = function () {
    };
    return AddJobComponent;
}());
AddJobComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-add-job',
        template: __webpack_require__("../../../../../src/app/component/add-job/add-job.component.html"),
        styles: [__webpack_require__("../../../../../src/app/component/add-job/add-job.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4__services_direction_direction_service__["a" /* DirectionService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_direction_direction_service__["a" /* DirectionService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["l" /* ChangeDetectorRef */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["l" /* ChangeDetectorRef */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* Router */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* ActivatedRoute */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2__sdk__["a" /* JobApi */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__sdk__["a" /* JobApi */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_2__sdk__["g" /* VehicleTypeApi */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__sdk__["g" /* VehicleTypeApi */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_1__agm_core__["b" /* GoogleMapsAPIWrapper */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__agm_core__["b" /* GoogleMapsAPIWrapper */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_1__agm_core__["c" /* MapsAPILoader */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__agm_core__["c" /* MapsAPILoader */]) === "function" && _h || Object])
], AddJobComponent);

var _a, _b, _c, _d, _e, _f, _g, _h;
//# sourceMappingURL=add-job.component.js.map

/***/ }),

/***/ "../../../../../src/app/component/add-user/add-user.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/component/add-user/add-user.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\r\n    <div class=\"row\">\r\n        <div class=\"col-lg-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <strong class=\"card-title\">Register a {{user.realm}}</strong>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <div id=\"pay-invoice\">\r\n                        <div class=\"card-body\">\r\n                            <form  (submit)=\"signup(signupForm)\" class=\"validate-form\" #signupForm=\"ngForm\">\r\n                                \r\n                                <div class=\"form-group img-section\">\r\n                                    <img class=\"profile-pic\" [src]=\"defaultUserImg\" />\r\n                                    <div style=\"clear: both;\"></div>\r\n                                    <div class=\"upload-button\">\r\n                                        <p>Upload Image</p>\r\n                                    </div>\r\n                                    <input class=\"file-upload image-upload\" type=\"file\" accept=\"image/*\" />\r\n                                </div>\r\n                                \r\n                                <div style=\"clear: both;\"></div>\r\n                                \r\n                                <div class=\"col-md-12\">\r\n                                    <div class=\"form-group\">\r\n                                        <label>Name</label>\r\n                                        <input type=\"text\" [(ngModel)]=\"user.name\" required name=\"name\" placeholder=\"Name\" #name=\"ngModel\"  class=\"form-control\">\r\n                                    </div>\r\n                                    <div class=\"error-div\" *ngIf=\"signupForm._submitted && signupForm.hasError('required', 'name')\">\r\n                                        Name is required\r\n                                    </div>  \r\n                                </div>\r\n                                \r\n\r\n                                <div class=\"col-md-12\">\r\n                                    <div class=\"form-group\">\r\n                                        <label>Password</label>\r\n                                        <input type=\"password\" [(ngModel)]=\"user.password\" name=\"password\" placeholder=\"Password\" required minlength=\"6\" #password=\"ngModel\" class=\"form-control\">\r\n                                    </div>\r\n                                    <div class=\"error-div\" *ngIf=\"signupForm._submitted && signupForm.hasError('required', 'password')\">\r\n                                        Password is required\r\n                                    </div>  \r\n                                </div>\r\n                                \r\n                                <div class=\"col-md-12\">\r\n                                    <div class=\"form-group\">\r\n                                        <label>Confirm Password</label>\r\n                                        <input type=\"password\" [(ngModel)]=\"user.confirmPassword\" name=\"confirmPassword\" placeholder=\"Confirm-Password\" required minlength=\"6\" #conPassword=\"ngModel\" class=\"form-control\">\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"error-div\" *ngIf=\"signupForm._submitted && signupForm.hasError('required', 'conPassword')\">\r\n                                    Confirm Password is required\r\n                                </div>  \r\n                                <div class=\"error-div\" *ngIf=\"signupForm._submitted && !signupForm.hasError('required', 'conPassword') && user.password!=user.confirmPassword\">\r\n                                    Password doesn't match\r\n                                </div>\r\n\r\n                                <div class=\"col-md-12\" [hidden]=\"user.realm != 'owner'\">\r\n                                    <div class=\"form-group\">\r\n                                        <label>Company Name</label>\r\n                                        <input  class=\"form-control\"  type=\"text\" [required]=\"user.realm == 'owner'\" [(ngModel)]=\"user.companyName\" name=\"companyName\" placeholder=\"Company Name\" #companyName=\"ngModel\">\r\n                                    </div>\r\n                                    <div class=\"error-div\" *ngIf=\"signupForm._submitted && signupForm.hasError('required', 'companyName')\">\r\n                                        Company Name is required\r\n                                    </div>  \r\n                                </div>\r\n                                \r\n\r\n                                <div class=\"col-md-12\">\r\n                                    <div class=\"form-group\">\r\n                                        <label>Email</label>\r\n                                        <input class=\"form-control\"  type=\"text\" [(ngModel)]=\"user.email\" [pattern]=\"re\" required name=\"email\" placeholder=\"Email\" #email=\"ngModel\">\r\n                                    </div>\r\n                                    <div class=\"error-div\" *ngIf=\"signupForm._submitted && signupForm.hasError('required', 'email')\">\r\n                                        Email is required\r\n                                    </div>  \r\n                                    <div class=\"error-div\" *ngIf=\"signupForm._submitted && !signupForm.hasError('required', 'email') && signupForm.hasError('pattern', 'email')\">\r\n                                        Email is not Valid\r\n                                    </div>\r\n                                </div>\r\n                                \r\n\r\n                                <div class=\"col-md-12\">\r\n                                    <div class=\"form-group\">\r\n                                        <label>Mobile</label>\r\n                                        <input class=\"form-control\"  type=\"text\" [(ngModel)]=\"user.mobile\" required name=\"mobile\" placeholder=\"Mobile Number\" #mobile=\"ngModel\">\r\n                                    </div>\r\n                                    <div class=\"error-div\" *ngIf=\"signupForm._submitted && signupForm.hasError('required', 'mobile')\">\r\n                                        Mobile number is required\r\n                                    </div>  \r\n                                    <!-- <div class=\"error-div\" *ngIf=\"signupForm._submitted && !signupForm.hasError('required', 'mobile') && signupForm.hasError('pattern', 'mobile')\">\r\n                                        Email is not Valid\r\n                                    </div> -->\r\n                                </div>\r\n                               \r\n\r\n                                <div class=\"col-6 document-demo\">\r\n                                    <label>Upload Your Driving Licence</label>\r\n                                    <div class=\"input-group\">\r\n                                        <input type=\"file\" id=\"base-input\" class=\" form-style-base input100\">\r\n                                        <input type=\"text\" id=\"fake-input\" class=\"form-style-fake input100\" placeholder=\"Upload your Driving Licence\" readonly>\r\n                                        <span class=\"glyphicon glyphicon-open input-place\"></span>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"col-6 document-demo\">\r\n                                    <label>Any Goverment ID</label>\r\n                                    <div class=\"input-group\">\r\n                                        <input type=\"file\" id=\"base-input1\" class=\" form-style-base input100\">\r\n                                        <input type=\"text\" id=\"fake-input1\" class=\"form-style-fake input100\" placeholder=\"Any Goverment ID\" readonly>\r\n                                        <span class=\"glyphicon glyphicon-open input-place\"></span>\r\n                                    </div>\r\n                                </div>\r\n                                <div>\r\n                                    <button id=\"payment-button\" type=\"submit\" class=\"btn btn-lg btn-info btn-block\">\r\n                                        Submit\r\n                                    </button>\r\n                                </div>\r\n                            </form>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<!-- old code -->\r\n\r\n<!-- <div class=\"animated fadeIn\">\r\n    <div class=\"row\">\r\n        <div class=\"col-lg-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <strong class=\"card-title\">Registration</strong>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    \r\n                    <div id=\"pay-invoice\">\r\n                        <div class=\"card-body\">\r\n                            <form action=\"\" method=\"post\" novalidate=\"novalidate\">\r\n                                \r\n\r\n                                <div class=\"form-group\">\r\n                                    <label>Name</label>\r\n                                    <input name=\"name\" type=\"text\" class=\"form-control\" aria-required=\"true\">\r\n                                </div>\r\n                                <div class=\"form-group\">\r\n                                    <label>UserName</label>\r\n                                    <input name=\"user-name\" type=\"text\" class=\"form-control\">\r\n                                </div>\r\n                                <div class=\"form-group\">\r\n                                    <label>Mobile Number</label>\r\n                                    <input name=\"mobile-number\"  class=\"form-control\" type=\"text\">\r\n                                </div>\r\n                                <div class=\"row\">\r\n                                    <div class=\"col-md-12\">\r\n                                        <div class=\"form-group\">\r\n                                            <label>Email</label>\r\n                                            <input type=\"text\" class=\"form-control\" name=\"Email\">\r\n                                        </div>\r\n                                    </div>\r\n                                    <div class=\"col-6\">\r\n                                        <label>Password</label>\r\n                                        <div class=\"input-group\">\r\n                                            <input class=\"form-control\" type=\"Password\" name=\"Password\">\r\n                                        </div>\r\n                                    </div>\r\n                                    <div class=\"col-6\">\r\n                                        <label>Confirm Password</label>\r\n                                        <div class=\"input-group\">\r\n                                            <input class=\"form-control\" type=\"Password\" name=\"Password\">\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                                <br>\r\n                                <div>\r\n                                    <button id=\"payment-button\" type=\"submit\" class=\"btn btn-lg btn-info btn-block\">\r\n                                       Submit\r\n                                    </button>\r\n                                </div>\r\n                            </form>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n          \r\n        </div>\r\n        \r\n    </div>\r\n</div> -->\r\n\r\n<!--     <script>\r\n    $(document).ready(function() {\r\n\r\n\r\n        var readURL = function(input) {\r\n            if (input.files && input.files[0]) {\r\n                var reader = new FileReader();\r\n\r\n                reader.onload = function(e) {\r\n                    $('.profile-pic').attr('src', e.target.result);\r\n                }\r\n\r\n                reader.readAsDataURL(input.files[0]);\r\n            }\r\n        }\r\n\r\n\r\n        $(\".file-upload\").on('change', function() {\r\n            readURL(this);\r\n        });\r\n\r\n        $(\".upload-button\").on('click', function() {\r\n            $(\".file-upload\").click();\r\n        });\r\n    });\r\n    </script>\r\n    <script>\r\n    $('input[id=base-input]').change(function() {\r\n        $('#fake-input').val($(this).val().replace(\"C:\\\\fakepath\\\\\", \"\"));\r\n    });\r\n    $('input[id=main-input]').change(function() {\r\n        console.log($(this).val());\r\n        var mainValue = $(this).val();\r\n        if (mainValue == \"\") {\r\n            document.getElementById(\"fake-btn\").innerHTML = \"Choose File\";\r\n        } else {\r\n            document.getElementById(\"fake-btn\").innerHTML = mainValue.replace(\"C:\\\\fakepath\\\\\", \"\");\r\n        }\r\n    });\r\n    </script>\r\n    <script>\r\n    $('input[id=base-input1]').change(function() {\r\n        $('#fake-input1').val($(this).val().replace(\"C:\\\\fakepath\\\\\", \"\"));\r\n    });\r\n    $('input[id=main-input]').change(function() {\r\n        console.log($(this).val());\r\n        var mainValue = $(this).val();\r\n        if (mainValue == \"\") {\r\n            document.getElementById(\"fake-btn\").innerHTML = \"Choose File\";\r\n        } else {\r\n            document.getElementById(\"fake-btn\").innerHTML = mainValue.replace(\"C:\\\\fakepath\\\\\", \"\");\r\n        }\r\n    });\r\n    </script> -->"

/***/ }),

/***/ "../../../../../src/app/component/add-user/add-user.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddUserComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_default_params_default_params_service__ = __webpack_require__("../../../../../src/app/services/default-params/default-params.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__sdk_index__ = __webpack_require__("../../../../../src/app/sdk/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_multipart_multipart_service__ = __webpack_require__("../../../../../src/app/services/multipart/multipart.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AddUserComponent = (function () {
    function AddUserComponent(multipartApi, dPService, peopleApi, route, router) {
        this.multipartApi = multipartApi;
        this.dPService = dPService;
        this.peopleApi = peopleApi;
        this.route = route;
        this.router = router;
        this.files = {};
        this.user = {};
        this.params = {};
        this.errSuccObj = {};
        this.userErr = {};
        this.isLogin = false;
        this.re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        this.defaultUserImg = this.dPService.defaultUserImg();
    }
    AddUserComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.isLogin = true;
        this.route.params.subscribe(function (params) {
            _this.params = params;
            _this.user.realm = _this.params.type;
        });
    };
    AddUserComponent.prototype.signup = function (signupForm) {
        var _this = this;
        var _self = this;
        if (signupForm.valid && this.user.confirmPassword == this.user.password) {
            this.multipartApi.signupApi({ data: this.user, files: this.files }).subscribe(function (success) {
                _this.errSuccObj.error = success.success.msg;
                _this.errSuccObj.error = undefined;
            }, function (error) {
                _this.errSuccObj.error = error.message;
                _this.errSuccObj.success = undefined;
            });
        }
    };
    AddUserComponent.prototype.checkPassword = function () {
        if (this.user.confirmPassword != undefined && this.user.confirmPassword.length > 0) {
            if (this.user.password !== this.user.confirmPassword) {
                this.userErr.passMatch = true;
            }
            if (this.user.password === this.user.confirmPassword) {
                this.userErr.passMatch = false;
                this.userErr.password = false;
                this.userErr.confirmPassword = false;
            }
        }
        if ((this.user.confirmPassword != undefined && this.user.password != undefined) && (this.user.confirmPassword.length == 0 || this.user.password.length == 0)) {
            this.userErr.passMatch = false;
        }
    };
    AddUserComponent.prototype.validateEmail = function () {
        if (this.user.email.length > 0) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (re.test(this.user.email)) {
                this.userErr.emailValid = false;
            }
            else {
                this.userErr.emailValid = true;
            }
        }
        else {
            this.userErr.emailValid = false;
        }
    };
    return AddUserComponent;
}());
AddUserComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-add-user',
        template: __webpack_require__("../../../../../src/app/component/add-user/add-user.component.html"),
        styles: [__webpack_require__("../../../../../src/app/component/add-user/add-user.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4__services_multipart_multipart_service__["a" /* MultipartService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_multipart_multipart_service__["a" /* MultipartService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__services_default_params_default_params_service__["a" /* DefaultParamsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_default_params_default_params_service__["a" /* DefaultParamsService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__sdk_index__["d" /* PeopleApi */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__sdk_index__["d" /* PeopleApi */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */]) === "function" && _e || Object])
], AddUserComponent);

var _a, _b, _c, _d, _e;
//# sourceMappingURL=add-user.component.js.map

/***/ }),

/***/ "../../../../../src/app/component/dashboard/dashboard.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/component/dashboard/dashboard.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"col-md-12\">\r\n    <div class=\"truck-img\">\r\n        <img class=\"img-responsive\" src=\"assets/images/admin-truck.jpg\">\r\n    </div>\r\n</div>\r\n\r\n<div class=\"col-xl-3 col-lg-6\">\r\n    <div class=\"card\">\r\n        <div class=\"card-body\">\r\n            <div class=\"stat-widget-one\">\r\n                <div class=\"stat-icon dib\"><i class=\"ti-money text-success border-success\"></i></div>\r\n                <div class=\"stat-content dib\">\r\n                    <div class=\"stat-text\">Total Profit</div>\r\n                    <div class=\"stat-digit\">1,012</div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"col-xl-3 col-lg-6\">\r\n    <div class=\"card\">\r\n        <div class=\"card-body\">\r\n            <div class=\"stat-widget-one\">\r\n                <div class=\"stat-icon dib\"><i class=\"ti-user text-primary border-primary\"></i></div>\r\n                <div class=\"stat-content dib\">\r\n                    <div class=\"stat-text\">New Customer</div>\r\n                    <div class=\"stat-digit\">961</div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"col-xl-3 col-lg-6\">\r\n    <div class=\"card\">\r\n        <div class=\"card-body\">\r\n            <div class=\"stat-widget-one\">\r\n                <div class=\"stat-icon dib\"><i class=\"ti-layout-grid2 text-warning border-warning\"></i></div>\r\n                <div class=\"stat-content dib\">\r\n                    <div class=\"stat-text\">Active Projects</div>\r\n                    <div class=\"stat-digit\">770</div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/component/dashboard/dashboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DashboardComponent = (function () {
    function DashboardComponent() {
    }
    DashboardComponent.prototype.ngOnInit = function () {
    };
    return DashboardComponent;
}());
DashboardComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-dashboard',
        template: __webpack_require__("../../../../../src/app/component/dashboard/dashboard.component.html"),
        styles: [__webpack_require__("../../../../../src/app/component/dashboard/dashboard.component.css")]
    }),
    __metadata("design:paramtypes", [])
], DashboardComponent);

//# sourceMappingURL=dashboard.component.js.map

/***/ }),

/***/ "../../../../../src/app/component/job-list/job-list.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/component/job-list/job-list.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\r\n    <div class=\"row\">\r\n        <div class=\"col-lg-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <strong class=\"card-title\">Jobs</strong>\r\n                </div>\r\n                <div class=\"card-body Vehicle\" *ngFor=\"let job of jobs\">\r\n                    <div class=\"col-md-2\">\r\n                        <div class=\"driver-image\">\r\n                            <img class=\"img-responsive\" src=\"assets/images/admin.jpg\">\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-md-10\">\r\n                        <div class=\"Vehicle-list fuel-list\">\r\n                            <h3 *ngIf=\"job.title\">{{job.title}}</h3>\r\n                            <span *ngIf=\"job.createdAt\">Date - {{job.createdAt|date : 'dd MMMM, yyyy'}}</span>\r\n                            <ul>\r\n                                <li *ngIf=\"job.requiredType\">Require Type - {{job.requiredType}}</li>\r\n                                <li *ngIf=\"job.weight\">Weight - {{job.weight}}</li>\r\n                                <li *ngIf=\"job.valueOfGoods\">value of goods - {{job.valueOfGoods}}</li>\r\n                                <li *ngIf=\"job.netPrice\">Price - {{job.netPrice}}</li>\r\n                                <li *ngIf=\"job.typeOfVehicle\">type of vehicle - {{job.typeOfVehicle}}</li>\r\n                                <li *ngIf=\"job.description\">Description - {{job.description}}</li>\r\n                                <li *ngIf=\"job.natureOfGoods\">Nature of goods - {{job.natureOfGoods}}</li>\r\n                                <li *ngIf=\"job.typeOfJob\">Type of job - {{job.typeOfJob}}</li>\r\n                                <li *ngIf=\"job.distance\">Distance - {{job.distance.text}}</li>\r\n                                <li *ngIf=\"job.vehicleTonnage\">Vehicle Tonnage - {{job.vehicleTonnage}}</li>\r\n                                <li *ngIf=\"job.status\">Status - {{job.status}}</li>\r\n                                \r\n                            </ul>\r\n                            <div style=\"clear: both;\"></div>\r\n                            <h5>{{job.netPrice}} KES</h5>\r\n                            <a href=\"javascript:;\" *ngIf=\"job.jobApproveStatus =='pending'\" (click)=\"openModel('approved',job.id)\">Approve </a>\r\n\t                        <a href=\"javascript:;\" *ngIf=\"job.jobApproveStatus =='pending'\" (click)=\"openModel('reject',job.id)\">Reject </a>\r\n\t                        \r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"modal fade\" id=\"jobApproval\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"largeModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog modal-lg\" role=\"document\">\r\n        <div class=\"modal-content\">\r\n            <div class=\"modal-header\">\r\n                <h5 class=\"modal-title\" id=\"largeModalLabel\">Job Approval</h5>\r\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n                    <span aria-hidden=\"true\">&times;</span>\r\n                </button>\r\n            </div>\r\n            <form (submit)=\"verify(jobApprovalForm)\" #jobApprovalForm=\"ngForm\">\r\n                <div class=\"modal-body\">\r\n\r\n                    <p>Are you sure you want to {{approveOps.status==\"approved\"?'approve':'reject'}} this user</p>\r\n\r\n                    <!-- <div class=\"form-group\">\r\n                        <label for=\"comment\">Comment:</label>\r\n                        <textarea class=\"form-control\" [(ngModel)]=\"approveOps.reason\" name=\"Email\" rows=\"5\" id=\"comment\" #reason=\"ngModel\"></textarea>\r\n                    </div> -->\r\n                </div>\r\n                <div class=\"modal-footer\">\r\n                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Cancel</button>\r\n                    <button type=\"submit\" class=\"btn btn-primary\" >Confirm</button>\r\n                </div>\r\n            </form>    \r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/component/job-list/job-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JobListComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__sdk_index__ = __webpack_require__("../../../../../src/app/sdk/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_default_params_default_params_service__ = __webpack_require__("../../../../../src/app/services/default-params/default-params.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var JobListComponent = (function () {
    function JobListComponent(dPService, jobApi, route, router) {
        this.dPService = dPService;
        this.jobApi = jobApi;
        this.route = route;
        this.router = router;
        this.params = {};
        this.jobs = [];
        this.approveOps = {};
    }
    JobListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.params = params;
            console.log(_this.params);
            _this.getJobs();
        });
    };
    JobListComponent.prototype.getType = function (type) {
        if (type == "rejected")
            return "reject";
        else
            return type;
    };
    JobListComponent.prototype.getJobs = function () {
        var _this = this;
        this.jobApi.getJobsByAdmin(this.getType(this.params.status)).subscribe(function (success) {
            _this.jobs = success.success.data;
            console.log(_this.jobs);
        }, function (error) {
        });
    };
    JobListComponent.prototype.openModel = function (status, jobId) {
        this.approveOps.status = status;
        this.approveOps.jobId = jobId;
        console.log(this.approveOps);
        $('#jobApproval').modal('show');
    };
    JobListComponent.prototype.verify = function () {
        var _this = this;
        this.jobApi.approveByAdmin(this.approveOps.jobId, this.approveOps.status).subscribe(function (success) {
            console.log(success);
            _this.getJobs();
            $('#jobApproval').modal('hide');
        }, function (error) {
        });
    };
    return JobListComponent;
}());
JobListComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-job-list',
        template: __webpack_require__("../../../../../src/app/component/job-list/job-list.component.html"),
        styles: [__webpack_require__("../../../../../src/app/component/job-list/job-list.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__services_default_params_default_params_service__["a" /* DefaultParamsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_default_params_default_params_service__["a" /* DefaultParamsService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__sdk_index__["a" /* JobApi */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__sdk_index__["a" /* JobApi */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */]) === "function" && _d || Object])
], JobListComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=job-list.component.js.map

/***/ }),

/***/ "../../../../../src/app/component/login/login.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".login-page{\r\n\tmin-width:100vw;\r\n\tmin-height:100vh;\r\n\tbackground: #343a40!important;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/component/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"login-page\">\r\n    <div class=\"sufee-login d-flex align-content-center flex-wrap\">\r\n        <div class=\"container\">\r\n            <div class=\"login-content\">\r\n                <div class=\"login-logo\">\r\n                    <a href=\"index.html\">\r\n                        <img class=\"align-content\" src=\"assets/images/logo.png\" alt=\"\">\r\n                    </a>\r\n                </div>\r\n                <div class=\"login-form\">\r\n                    <form  class=\"contact100-form validate-form\" (submit)=\"login(loginForm)\" #loginForm=\"ngForm\">\r\n                        <div class=\"form-group\">\r\n                          <label >\r\n                            <input type=\"radio\" [(ngModel)]=\"user.realm\" value=\"admin\" id=\"admin\" name=\"type\"> Admin\r\n                            <span class=\"checkmark\"></span>\r\n                          </label>\r\n                          <label >\r\n                            <input type=\"radio\" [(ngModel)]=\"user.realm\" value=\"super_admin\" name=\"type\" id=\"super_admin\"> Super Admin\r\n                            <span class=\"checkmark\"></span>\r\n                          </label>\r\n                        </div>\r\n\r\n                         <div class=\"alert alert-danger\" *ngIf=\"errSuccObj.error\" role=\"alert\">{{errSuccObj.error}}</div>\r\n\r\n                        <div class=\"form-group\">\r\n                            <label>Email address</label>\r\n                            <input type=\"text\" class=\"form-control\" [(ngModel)]=\"user.email\" (ngModelChange)=\"validateEmail()\" [required]=\"true\" name=\"Email\" placeholder=\"Email\" #email=\"ngModel\">\r\n                        </div>\r\n                        <div class=\"error-div\" *ngIf=\"userErr.email\">\r\n                          Email is required\r\n                        </div>\r\n                        <div class=\"error-div\" *ngIf=\"userErr.emailValid\">\r\n                          Invalid email\r\n                        </div>\r\n\r\n                        <div class=\"form-group\">\r\n                            <label>Password</label>\r\n                            <input type=\"password\" class=\"form-control\" required [(ngModel)]=\"user.password\" required name=\"password\" placeholder=\"Password\" #password=\"ngModel\">\r\n                        </div>\r\n                        <div class=\"error-div\" *ngIf=\"userErr.password\">\r\n                          Password is required\r\n                        </div>\r\n\r\n                        <div class=\"checkbox\">\r\n                            <label>\r\n                                <input type=\"checkbox\"> Remember Me\r\n                            </label>\r\n                            <label class=\"pull-right\">\r\n                                <a href=\"#\">Forgotten Password?</a>\r\n                            </label>\r\n\r\n                        </div>\r\n                        <button class=\"btn btn-success btn-flat m-b-30 m-t-30\" type=\"submit\" id=\"loginBtn\" data-loading-text=\"Login...\" autocomplete=\"off\">Sign in</button>\r\n                    </form>\r\n\r\n\r\n\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>    \r\n"

/***/ }),

/***/ "../../../../../src/app/component/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__sdk_index__ = __webpack_require__("../../../../../src/app/sdk/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LoginComponent = (function () {
    function LoginComponent(peopleApi, router) {
        this.peopleApi = peopleApi;
        this.router = router;
        this.userErr = {};
        this.user = {};
        this.errSuccObj = {};
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.validate = function () {
        var error = false;
        if (!this.user.email) {
            this.userErr.email = true;
            error = true;
        }
        else {
            this.userErr.email = false;
        }
        if (!this.user.password) {
            this.userErr.password = true;
            error = true;
        }
        else {
            this.userErr.password = false;
        }
        return error;
    };
    LoginComponent.prototype.validateEmail = function () {
        if (this.user.email.length > 0) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (re.test(this.user.email)) {
                this.userErr.emailValid = false;
                if (this.userErr.email) {
                    this.userErr.email = false;
                }
            }
            else {
                this.userErr.emailValid = true;
            }
        }
        else {
            this.userErr.emailValid = false;
        }
    };
    LoginComponent.prototype.login = function (loginForm) {
        var _this = this;
        // let firebaseToken = this.formatingFunctionsService.getToken();
        // this.user.firebaseToken = firebaseToken;
        var _self = this;
        if (!this.validate() && !this.userErr.emailValid) {
            console.log(this.user);
            var rememberMe = false;
            this.peopleApi.login(this.user, 'user', rememberMe).subscribe(function (success) {
                _this.router.navigate(['/']);
            }, function (error) {
                _this.errSuccObj.error = error.message;
            });
        }
    };
    return LoginComponent;
}());
LoginComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-login',
        template: __webpack_require__("../../../../../src/app/component/login/login.component.html"),
        styles: [__webpack_require__("../../../../../src/app/component/login/login.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__sdk_index__["d" /* PeopleApi */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__sdk_index__["d" /* PeopleApi */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */]) === "function" && _b || Object])
], LoginComponent);

var _a, _b;
//# sourceMappingURL=login.component.js.map

/***/ }),

/***/ "../../../../../src/app/component/user-details/user-details.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/component/user-details/user-details.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\" *ngIf=\"peopleInfo.id\">\r\n    <div class=\"row\">\r\n        <div class=\"col-lg-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <strong class=\"card-title\">Detail Page</strong>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <div class=\"detail-text\">\r\n                        <div class=\"col-md-4\">\r\n                            <div class=\"detail-page-image\">\r\n                            <img class=\"img-responsive\" [src]=\"peopleInfo.profileImage==''?defaultUserImg:baseURL+peopleInfo.profileImage\">\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"col-md-8\">\r\n                            <h5>{{peopleInfo.name}}</h5>\r\n                            <div class=\"Vehicle-detail-text\">\r\n                                <table class=\"table\">\r\n                                    \r\n                                    <tbody>\r\n                                        <tr *ngIf=\"peopleInfo.createdAt\">\r\n                                            <td>Created At</td>\r\n                                            <td>-</td>\r\n                                            <td>{{peopleInfo.createdAt| date:\"dd MMMM, yyyy\"}}</td>\r\n                                        </tr>\r\n                                        <tr *ngIf=\"peopleInfo.realm\">\r\n                                            <td>Role</td>\r\n                                            <td>-</td>\r\n                                            <td>{{peopleInfo.realm}}</td>\r\n                                        </tr>\r\n                                        <tr *ngIf=\"peopleInfo.companyName\">\r\n                                            <td>Company Name</td>\r\n                                            <td>-</td>\r\n                                            <td>\r\n                                                {{peopleInfo.companyName}}\r\n                                            </td>\r\n                                        </tr>\r\n                                        <tr *ngIf=\"peopleInfo.email\">\r\n                                            <td>Email</td>\r\n                                            <td>-</td>\r\n                                            <td>\r\n                                                {{peopleInfo.email}}\r\n                                                <img *ngIf=\"peopleInfo.emailVerified\" height=\"15px\" src=\"assets/images/icon/verified.png\">\r\n                                                <img *ngIf=\"!peopleInfo.emailVerified\" height=\"15px\" src=\"assets/images/icon/not_verified.png\">\r\n                                            </td>\r\n                                        </tr>\r\n                                        <tr *ngIf=\"peopleInfo.mobile\">\r\n                                            <td>Mobile</td>\r\n                                            <td>-</td>\r\n                                            <td>\r\n                                                {{peopleInfo.mobile}}\r\n                                                <img *ngIf=\"peopleInfo.mobileVerified\" height=\"15px\" src=\"assets/images/icon/verified.png\">\r\n                                                <img *ngIf=\"!peopleInfo.mobileVerified\" height=\"15px\" src=\"assets/images/icon/not_verified.png\">\r\n                                            </td>\r\n                                        </tr>\r\n                                        <tr *ngIf=\"peopleInfo.adminVerifiedStatus\">\r\n                                            <td>Admin Verified</td>\r\n                                            <td>-</td>\r\n                                            <td>{{peopleInfo.adminVerifiedStatus}}</td>\r\n                                        </tr>\r\n                                        <tr >\r\n                                            <td>Rating</td>\r\n                                            <td>-</td>\r\n                                            <td>{{peopleInfo.rating.avgRating}}</td>\r\n                                        </tr>\r\n                                        <tr *ngIf=\"peopleInfo.canDrive\">\r\n                                            <td>Can Drive</td>\r\n                                            <td>-</td>\r\n                                            <td>{{peopleInfo.canDrive}}</td>\r\n                                        </tr>\r\n                                        \r\n                                    </tbody>\r\n                                </table>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/component/user-details/user-details.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserDetailsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__sdk_index__ = __webpack_require__("../../../../../src/app/sdk/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_default_params_default_params_service__ = __webpack_require__("../../../../../src/app/services/default-params/default-params.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UserDetailsComponent = (function () {
    function UserDetailsComponent(dPService, peopleApi, route, router) {
        this.dPService = dPService;
        this.peopleApi = peopleApi;
        this.route = route;
        this.router = router;
        this.peopleInfo = {};
        this.params = {};
        this.defaultUserImg = this.dPService.defaultUserImg();
    }
    UserDetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.params = params;
            console.log(_this.params);
        });
        this.baseURL = __WEBPACK_IMPORTED_MODULE_1__sdk_index__["c" /* LoopBackConfig */].getPath();
        this.getUserInfo();
    };
    UserDetailsComponent.prototype.getUserInfo = function () {
        var _this = this;
        this.peopleApi.getPeopleInfo(this.params.peopleId).subscribe(function (success) {
            _this.peopleInfo = success.success.data;
        }, function (error) {
        });
    };
    return UserDetailsComponent;
}());
UserDetailsComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-user-details',
        template: __webpack_require__("../../../../../src/app/component/user-details/user-details.component.html"),
        styles: [__webpack_require__("../../../../../src/app/component/user-details/user-details.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__services_default_params_default_params_service__["a" /* DefaultParamsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_default_params_default_params_service__["a" /* DefaultParamsService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__sdk_index__["d" /* PeopleApi */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__sdk_index__["d" /* PeopleApi */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */]) === "function" && _d || Object])
], UserDetailsComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=user-details.component.js.map

/***/ }),

/***/ "../../../../../src/app/component/user-list/user-list.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/component/user-list/user-list.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\r\n    <div class=\"row\">\r\n        <div class=\"col-lg-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <strong class=\"card-title\">{{this.params.type | titlecase}} List</strong>\r\n                </div>\r\n                <div class=\"card-body Vehicle\" *ngFor=\"let people of peopleList\">\r\n                    <div class=\"col-md-2\">\r\n                        <div class=\"driver-image\">\r\n                            <img class=\"img-responsive\" [src]=\"people.profileImage==''?defaultUserImg:baseURL+people.profileImage\">\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-md-10\">\r\n                        <div class=\"Vehicle-list\">\r\n                            <h3>{{people.name}}</h3>\r\n                            <span *ngIf=\"people.createdAt\">Date - {{people.createdAt | date:\"dd MMMM, yyyy\"}}</span>\r\n                            <ul>\r\n                                <li *ngIf=\"people.email\">Email - {{people.email}}</li>\r\n                                <li *ngIf=\"people.mobile\">Mobile - {{people.mobile}}</li>\r\n                                <li *ngIf=\"people.adminVerifiedStatus\">Status - {{people.adminVerifiedStatus}}</li>\r\n                                <li *ngIf=\"people.mobile\">Mobile - {{people.mobile}}</li>\r\n                            </ul>\r\n                            \r\n                            <a href=\"javascript:;\" routerLink=\"/user-details/{{people.id}}\">Details </a>\r\n                            <a href=\"javascript:;\" *ngIf=\"people.adminVerifiedStatus=='pending'\" (click)=\"openModel('approved',people.id)\">Approve </a>\r\n                            <a href=\"javascript:;\" *ngIf=\"people.adminVerifiedStatus=='pending'\" (click)=\"openModel('rejected',people.id)\">Reject </a>\r\n                        \r\n                            \r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <!-- .card -->\r\n        </div>\r\n        <!--/.col-->\r\n    </div>\r\n</div>\r\n\r\n<div class=\"modal fade\" id=\"userApproval\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"largeModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog modal-lg\" role=\"document\">\r\n        <div class=\"modal-content\">\r\n            <div class=\"modal-header\">\r\n                <h5 class=\"modal-title\" id=\"largeModalLabel\">User Approval</h5>\r\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n                    <span aria-hidden=\"true\">&times;</span>\r\n                </button>\r\n            </div>\r\n            <form (submit)=\"verify(userApprovalForm)\" #userApprovalForm=\"ngForm\">\r\n                <div class=\"modal-body\">\r\n\r\n                    <p>Are you sure you want to {{approveOps.status==\"approved\"?'approve':'reject'}} this user</p>\r\n\r\n                    <!-- <div class=\"form-group\">\r\n                        <label for=\"comment\">Comment:</label>\r\n                        <textarea class=\"form-control\" [(ngModel)]=\"approveOps.reason\" name=\"Email\" rows=\"5\" id=\"comment\" #reason=\"ngModel\"></textarea>\r\n                    </div> -->\r\n                </div>\r\n                <div class=\"modal-footer\">\r\n                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Cancel</button>\r\n                    <button type=\"submit\" class=\"btn btn-primary\" >Confirm</button>\r\n                </div>\r\n            </form>    \r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/component/user-list/user-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserListComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__sdk_index__ = __webpack_require__("../../../../../src/app/sdk/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_default_params_default_params_service__ = __webpack_require__("../../../../../src/app/services/default-params/default-params.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UserListComponent = (function () {
    function UserListComponent(dPService, peopleApi, route, router) {
        this.dPService = dPService;
        this.peopleApi = peopleApi;
        this.route = route;
        this.router = router;
        this.peopleList = [];
        this.params = {};
        this.approveOps = {};
        this.defaultUserImg = this.dPService.defaultUserImg();
    }
    UserListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.baseURL = __WEBPACK_IMPORTED_MODULE_1__sdk_index__["c" /* LoopBackConfig */].getPath();
        this.route.params.subscribe(function (params) {
            _this.params = params;
            console.log(_this.params);
            _this.getUserList();
        });
    };
    UserListComponent.prototype.getType = function (type) {
        var length = type.length;
        return type.substring(0, length - 1);
    };
    UserListComponent.prototype.getUserList = function () {
        var _this = this;
        this.peopleApi.getPeoplesByAdmin(this.getType(this.params.type)).subscribe(function (success) {
            _this.peopleList = success.success.data;
        }, function (error) {
        });
    };
    UserListComponent.prototype.openModel = function (status, peopleId) {
        this.approveOps.status = status;
        this.approveOps.peopleId = peopleId;
        console.log(this.approveOps);
        $('#userApproval').modal('show');
    };
    UserListComponent.prototype.verify = function () {
        var _this = this;
        this.peopleApi.userApproval(this.approveOps.peopleId, this.approveOps.status).subscribe(function (success) {
            console.log(success);
            _this.getUserList();
            $('#userApproval').modal('hide');
        }, function (error) {
        });
    };
    return UserListComponent;
}());
UserListComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-user-list',
        template: __webpack_require__("../../../../../src/app/component/user-list/user-list.component.html"),
        styles: [__webpack_require__("../../../../../src/app/component/user-list/user-list.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__services_default_params_default_params_service__["a" /* DefaultParamsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_default_params_default_params_service__["a" /* DefaultParamsService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__sdk_index__["d" /* PeopleApi */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__sdk_index__["d" /* PeopleApi */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */]) === "function" && _d || Object])
], UserListComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=user-list.component.js.map

/***/ }),

/***/ "../../../../../src/app/component/vehicle-details/vehicle-details.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/component/vehicle-details/vehicle-details.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\" *ngIf=\"vehicleInfo.owner\">\r\n    <div class=\"row\">\r\n        <div class=\"col-lg-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <strong class=\"card-title\">Vehicle Details</strong>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <div class=\"detail-text\">\r\n                        <h5 *ngIf=\"vehicleInfo.type\">\r\n                            {{vehicleInfo.type}}\r\n                        </h5>\r\n                        <div class=\"col-md-6\">\r\n                            <div class=\"Vehicle-detail-text\">\r\n                                <table class=\"table\">\r\n                                    <tbody>\r\n                                        <tr *ngIf=\"vehicleInfo.assesmentStart\">\r\n                                            <td>Assesment Start</td>\r\n                                            <td>-</td>\r\n                                            <td>{{vehicleInfo.assesmentStart | date:\"dd MMMM, yyyy\"}}</td>\r\n                                        </tr>\r\n                                        <tr *ngIf=\"vehicleInfo.assessmentEnd\">\r\n                                            <td>Assesment End</td>\r\n                                            <td>-</td>\r\n                                            <td>{{vehicleInfo.assessmentEnd  | date:\"dd MMMM, yyyy\"}}</td>\r\n                                        </tr>\r\n                                        <tr *ngIf=\"vehicleInfo.vehicleNumber\">\r\n                                            <td>Vehicle No.</td>\r\n                                            <td>-</td>\r\n                                            <td>{{vehicleInfo.vehicleNumber}}</td>\r\n                                        </tr>\r\n                                        <tr *ngIf=\"vehicleInfo.ton\">\r\n                                            <td>Tonnage</td>\r\n                                            <td>-</td>\r\n                                            <td>{{vehicleInfo.ton}}</td>\r\n                                        </tr>\r\n                                        <tr *ngIf=\"vehicleInfo.bodyType\">\r\n                                            <td>Body Type</td>\r\n                                            <td>-</td>\r\n                                            <td>{{vehicleInfo.bodyType}}</td>\r\n                                        </tr>\r\n                                        <tr *ngIf=\"vehicleInfo.approveStatus\">\r\n                                            <td>Approval</td>\r\n                                            <td>-</td>\r\n                                            <td>{{vehicleInfo.approveStatus}}</td>\r\n                                        </tr>\r\n                                        <tr *ngIf=\"vehicleInfo.owner.name\">\r\n                                            <td>Owner Name</td>\r\n                                            <td>-</td>\r\n                                            <td>{{vehicleInfo.owner.name}}</td>\r\n                                        </tr>\r\n                                        <tr *ngIf=\"vehicleInfo.owner.email\">\r\n                                            <td>Owner Email</td>\r\n                                            <td>-</td>\r\n                                            <td>{{vehicleInfo.owner.email}}</td>\r\n                                        </tr>\r\n                                    </tbody>\r\n                                </table>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <!-- .card -->\r\n        </div>\r\n        <!--/.col-->\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/component/vehicle-details/vehicle-details.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VehicleDetailsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__sdk_index__ = __webpack_require__("../../../../../src/app/sdk/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_default_params_default_params_service__ = __webpack_require__("../../../../../src/app/services/default-params/default-params.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var VehicleDetailsComponent = (function () {
    function VehicleDetailsComponent(dPService, vehicleApi, route, router) {
        this.dPService = dPService;
        this.vehicleApi = vehicleApi;
        this.route = route;
        this.router = router;
        this.vehicleInfo = {};
    }
    VehicleDetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.params = params;
            console.log(_this.params);
            _this.getVehicleInfo();
        });
    };
    VehicleDetailsComponent.prototype.getVehicleInfo = function () {
        var _this = this;
        this.vehicleApi.getVehicleById(this.params.vehicleId).subscribe(function (success) {
            _this.vehicleInfo = success.success.data;
            console.log(_this.vehicleInfo.owner);
        }, function (error) {
        });
    };
    return VehicleDetailsComponent;
}());
VehicleDetailsComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-vehicle-details',
        template: __webpack_require__("../../../../../src/app/component/vehicle-details/vehicle-details.component.html"),
        styles: [__webpack_require__("../../../../../src/app/component/vehicle-details/vehicle-details.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__services_default_params_default_params_service__["a" /* DefaultParamsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_default_params_default_params_service__["a" /* DefaultParamsService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__sdk_index__["f" /* VehicleApi */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__sdk_index__["f" /* VehicleApi */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */]) === "function" && _d || Object])
], VehicleDetailsComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=vehicle-details.component.js.map

/***/ }),

/***/ "../../../../../src/app/component/vehicle-list/vehicle-list.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/component/vehicle-list/vehicle-list.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\r\n    <div class=\"row\">\r\n        <div class=\"col-lg-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <strong class=\"card-title\">{{params.status|titlecase}} Vehicle List</strong>\r\n                </div>\r\n                <div class=\"card-body Vehicle\" *ngFor=\"let vehicle of vehicles\">\r\n                    <div class=\"Vehicle-list\">\r\n                        <h3>{{vehicle.type}}</h3>\r\n                        <span *ngIf=\"vehicle.createdAt\">Date - {{vehicle.createdAt| date:\"dd MMMM, yyyy\"}}</span>\r\n                        <ul>\r\n                            <li *ngIf=\"vehicle.assesmentStart\">Assessment Start - {{vehicle.assesmentStart | date:\"dd MMMM, yyyy\"}}</li>\r\n                            <li *ngIf=\"vehicle.assessmentEnd\">Assessment End - {{vehicle.assessmentEnd | date:\"dd MMMM, yyyy\"}}</li>\r\n                            <li *ngIf=\"vehicle.vehicleNumber\">Vehicle Number - {{vehicle.vehicleNumber}}</li>\r\n                            <li  *ngIf=\"vehicle.registrationNumber\">Registration No. - {{vehicle.registrationNumber}}</li>\r\n                            <li *ngIf=\"vehicle.ton\">Tonnage - {{vehicle.ton}}</li>\r\n                            <li *ngIf=\"vehicle.bodyType\">Body Type - {{vehicle.bodyType}}</li>\r\n                            <li *ngIf=\"vehicle.approveStatus\">Approval - {{vehicle.approveStatus}}</li>\r\n                            \r\n                        </ul>\r\n                        <a href=\"javascript:;\" *ngIf=\"vehicle.approveStatus =='pending'\" (click)=\"openModel('approved',vehicle.id)\">Approve </a>\r\n                        <a href=\"javascript:;\" *ngIf=\"vehicle.approveStatus =='pending'\" (click)=\"openModel('reject',vehicle.id)\">Reject </a>\r\n                        <a routerLink=\"/vehicle-details/{{vehicle.id}}\">Details </a>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <!-- .card -->\r\n        </div>\r\n        <!--/.col-->\r\n    </div>\r\n</div>\r\n\r\n<div class=\"modal fade\" id=\"vehicleApproval\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"largeModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog modal-lg\" role=\"document\">\r\n        <div class=\"modal-content\">\r\n            <div class=\"modal-header\">\r\n                <h5 class=\"modal-title\" id=\"largeModalLabel\">Vehicle Approval</h5>\r\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n                    <span aria-hidden=\"true\">&times;</span>\r\n                </button>\r\n            </div>\r\n            <form (submit)=\"verify(vehicleApprovalForm)\" #vehicleApprovalForm=\"ngForm\">\r\n                <div class=\"modal-body\">\r\n\r\n                    <p>Are you sure you want to {{approveOps.status==\"approved\"?'approve':'reject'}} this user</p>\r\n\r\n                    <!-- <div class=\"form-group\">\r\n                        <label for=\"comment\">Comment:</label>\r\n                        <textarea class=\"form-control\" [(ngModel)]=\"approveOps.reason\" name=\"Email\" rows=\"5\" id=\"comment\" #reason=\"ngModel\"></textarea>\r\n                    </div> -->\r\n                </div>\r\n                <div class=\"modal-footer\">\r\n                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Cancel</button>\r\n                    <button type=\"submit\" class=\"btn btn-primary\" >Confirm</button>\r\n                </div>\r\n            </form>    \r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/component/vehicle-list/vehicle-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VehicleListComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__sdk_index__ = __webpack_require__("../../../../../src/app/sdk/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_default_params_default_params_service__ = __webpack_require__("../../../../../src/app/services/default-params/default-params.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var VehicleListComponent = (function () {
    function VehicleListComponent(dPService, vehicleApi, route, router) {
        this.dPService = dPService;
        this.vehicleApi = vehicleApi;
        this.route = route;
        this.router = router;
        this.params = {};
        this.vehicles = [];
        this.approveOps = {};
    }
    VehicleListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.params = params;
            console.log(_this.params);
            _this.getVehicles();
        });
    };
    VehicleListComponent.prototype.getVehicles = function () {
        var _this = this;
        this.vehicleApi.getVehicleByAdmin(this.params.status).subscribe(function (success) {
            _this.vehicles = success.success.data;
        }, function (error) {
        });
    };
    VehicleListComponent.prototype.openModel = function (status, vehicleId) {
        this.approveOps.status = status;
        this.approveOps.vehicleId = vehicleId;
        console.log(this.approveOps);
        $('#vehicleApproval').modal('show');
    };
    VehicleListComponent.prototype.verify = function () {
        var _this = this;
        this.vehicleApi.approveAdmin(this.approveOps.vehicleId, this.approveOps.status).subscribe(function (success) {
            console.log(success);
            _this.getVehicles();
            $('#vehicleApproval').modal('hide');
        }, function (error) {
        });
    };
    return VehicleListComponent;
}());
VehicleListComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-vehicle-list',
        template: __webpack_require__("../../../../../src/app/component/vehicle-list/vehicle-list.component.html"),
        styles: [__webpack_require__("../../../../../src/app/component/vehicle-list/vehicle-list.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__services_default_params_default_params_service__["a" /* DefaultParamsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_default_params_default_params_service__["a" /* DefaultParamsService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__sdk_index__["f" /* VehicleApi */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__sdk_index__["f" /* VehicleApi */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */]) === "function" && _d || Object])
], VehicleListComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=vehicle-list.component.js.map

/***/ }),

/***/ "../../../../../src/app/routing/routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__component_dashboard_dashboard_component__ = __webpack_require__("../../../../../src/app/component/dashboard/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__component_login_login_component__ = __webpack_require__("../../../../../src/app/component/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__component_vehicle_list_vehicle_list_component__ = __webpack_require__("../../../../../src/app/component/vehicle-list/vehicle-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__component_vehicle_details_vehicle_details_component__ = __webpack_require__("../../../../../src/app/component/vehicle-details/vehicle-details.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__component_user_list_user_list_component__ = __webpack_require__("../../../../../src/app/component/user-list/user-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__component_user_details_user_details_component__ = __webpack_require__("../../../../../src/app/component/user-details/user-details.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__component_job_list_job_list_component__ = __webpack_require__("../../../../../src/app/component/job-list/job-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__component_add_job_add_job_component__ = __webpack_require__("../../../../../src/app/component/add-job/add-job.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__component_add_user_add_user_component__ = __webpack_require__("../../../../../src/app/component/add-user/add-user.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var routes = [
    { path: '', redirectTo: '/', pathMatch: 'full' },
    { path: '', component: __WEBPACK_IMPORTED_MODULE_3__component_dashboard_dashboard_component__["a" /* DashboardComponent */] },
    { path: 'login', component: __WEBPACK_IMPORTED_MODULE_4__component_login_login_component__["a" /* LoginComponent */] },
    { path: 'vehicle-list/:status', component: __WEBPACK_IMPORTED_MODULE_5__component_vehicle_list_vehicle_list_component__["a" /* VehicleListComponent */] },
    { path: 'vehicle-details/:vehicleId', component: __WEBPACK_IMPORTED_MODULE_6__component_vehicle_details_vehicle_details_component__["a" /* VehicleDetailsComponent */] },
    { path: 'user-list/:type', component: __WEBPACK_IMPORTED_MODULE_7__component_user_list_user_list_component__["a" /* UserListComponent */] },
    { path: 'user-details/:peopleId', component: __WEBPACK_IMPORTED_MODULE_8__component_user_details_user_details_component__["a" /* UserDetailsComponent */] },
    { path: 'job-list/:status', component: __WEBPACK_IMPORTED_MODULE_9__component_job_list_job_list_component__["a" /* JobListComponent */] },
    { path: 'add-job/:type', component: __WEBPACK_IMPORTED_MODULE_10__component_add_job_add_job_component__["a" /* AddJobComponent */] },
    { path: 'add-user/:type', component: __WEBPACK_IMPORTED_MODULE_11__component_add_user_add_user_component__["a" /* AddUserComponent */] },
];
var RoutingModule = (function () {
    function RoutingModule() {
    }
    return RoutingModule;
}());
RoutingModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgModule */])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_2__angular_common__["b" /* CommonModule */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forRoot(routes)
        ],
        exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */]],
        declarations: []
    })
], RoutingModule);

//# sourceMappingURL=routing.module.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return SDKBrowserModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__services_core_search_params__ = __webpack_require__("../../../../../src/app/sdk/services/core/search.params.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_core_error_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_core_auth_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_custom_logger_service__ = __webpack_require__("../../../../../src/app/sdk/services/custom/logger.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_custom_SDKModels__ = __webpack_require__("../../../../../src/app/sdk/services/custom/SDKModels.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__storage_storage_swaps__ = __webpack_require__("../../../../../src/app/sdk/storage/storage.swaps.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__storage_cookie_browser__ = __webpack_require__("../../../../../src/app/sdk/storage/cookie.browser.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__storage_storage_browser__ = __webpack_require__("../../../../../src/app/sdk/storage/storage.browser.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__sockets_socket_browser__ = __webpack_require__("../../../../../src/app/sdk/sockets/socket.browser.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__sockets_socket_driver__ = __webpack_require__("../../../../../src/app/sdk/sockets/socket.driver.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__sockets_socket_connections__ = __webpack_require__("../../../../../src/app/sdk/sockets/socket.connections.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__services_core_real_time__ = __webpack_require__("../../../../../src/app/sdk/services/core/real.time.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__services_custom_Email__ = __webpack_require__("../../../../../src/app/sdk/services/custom/Email.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__services_custom_People__ = __webpack_require__("../../../../../src/app/sdk/services/custom/People.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__services_custom_Transaction__ = __webpack_require__("../../../../../src/app/sdk/services/custom/Transaction.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__services_custom_Vehicle__ = __webpack_require__("../../../../../src/app/sdk/services/custom/Vehicle.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__services_custom_Container__ = __webpack_require__("../../../../../src/app/sdk/services/custom/Container.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__services_custom_VehicleType__ = __webpack_require__("../../../../../src/app/sdk/services/custom/VehicleType.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__services_custom_Job__ = __webpack_require__("../../../../../src/app/sdk/services/custom/Job.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__services_custom_Notification__ = __webpack_require__("../../../../../src/app/sdk/services/custom/Notification.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__services_custom_JobApply__ = __webpack_require__("../../../../../src/app/sdk/services/custom/JobApply.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__services_custom_Requestfordriver__ = __webpack_require__("../../../../../src/app/sdk/services/custom/Requestfordriver.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__services_custom_Wallet__ = __webpack_require__("../../../../../src/app/sdk/services/custom/Wallet.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__services_custom_Otp__ = __webpack_require__("../../../../../src/app/sdk/services/custom/Otp.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__services_custom_Rating__ = __webpack_require__("../../../../../src/app/sdk/services/custom/Rating.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__models_index__ = __webpack_require__("../../../../../src/app/sdk/models/index.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__services_index__ = __webpack_require__("../../../../../src/app/sdk/services/index.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_29__services_index__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_29__services_index__["b"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_29__services_index__["c"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "f", function() { return __WEBPACK_IMPORTED_MODULE_29__services_index__["d"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "g", function() { return __WEBPACK_IMPORTED_MODULE_29__services_index__["e"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__lb_config__ = __webpack_require__("../../../../../src/app/sdk/lb.config.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_30__lb_config__["a"]; });
/* unused harmony namespace reexport */
/* unused harmony reexport CookieBrowser */
/* unused harmony reexport StorageBrowser */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
/* tslint:disable */
/**
* @module SDKModule
* @author Jonathan Casarrubias <t:@johncasarrubias> <gh:jonathan-casarrubias>
* @license MIT 2016 Jonathan Casarrubias
* @version 2.1.0
* @description
* The SDKModule is a generated Software Development Kit automatically built by
* the LoopBack SDK Builder open source module.
*
* The SDKModule provides Angular 2 >= RC.5 support, which means that NgModules
* can import this Software Development Kit as follows:
*
*
* APP Route Module Context
* ============================================================================
* import { NgModule }       from '@angular/core';
* import { BrowserModule }  from '@angular/platform-browser';
* // App Root
* import { AppComponent }   from './app.component';
* // Feature Modules
* import { SDK[Browser|Node|Native]Module } from './shared/sdk/sdk.module';
* // Import Routing
* import { routing }        from './app.routing';
* @NgModule({
*  imports: [
*    BrowserModule,
*    routing,
*    SDK[Browser|Node|Native]Module.forRoot()
*  ],
*  declarations: [ AppComponent ],
*  bootstrap:    [ AppComponent ]
* })
* export class AppModule { }
*
**/




























/**
* @module SDKBrowserModule
* @description
* This module should be imported when building a Web Application in the following scenarios:
*
*  1.- Regular web application
*  2.- Angular universal application (Browser Portion)
*  3.- Progressive applications (Angular Mobile, Ionic, WebViews, etc)
**/
var SDKBrowserModule = SDKBrowserModule_1 = (function () {
    function SDKBrowserModule() {
    }
    SDKBrowserModule.forRoot = function (internalStorageProvider) {
        if (internalStorageProvider === void 0) { internalStorageProvider = {
            provide: __WEBPACK_IMPORTED_MODULE_5__storage_storage_swaps__["a" /* InternalStorage */],
            useClass: __WEBPACK_IMPORTED_MODULE_9__storage_cookie_browser__["a" /* CookieBrowser */]
        }; }
        return {
            ngModule: SDKBrowserModule_1,
            providers: [
                __WEBPACK_IMPORTED_MODULE_2__services_core_auth_service__["a" /* LoopBackAuth */],
                __WEBPACK_IMPORTED_MODULE_3__services_custom_logger_service__["a" /* LoggerService */],
                __WEBPACK_IMPORTED_MODULE_0__services_core_search_params__["a" /* JSONSearchParams */],
                __WEBPACK_IMPORTED_MODULE_4__services_custom_SDKModels__["a" /* SDKModels */],
                __WEBPACK_IMPORTED_MODULE_14__services_core_real_time__["a" /* RealTime */],
                __WEBPACK_IMPORTED_MODULE_15__services_custom_Email__["a" /* EmailApi */],
                __WEBPACK_IMPORTED_MODULE_16__services_custom_People__["a" /* PeopleApi */],
                __WEBPACK_IMPORTED_MODULE_17__services_custom_Transaction__["a" /* TransactionApi */],
                __WEBPACK_IMPORTED_MODULE_18__services_custom_Vehicle__["a" /* VehicleApi */],
                __WEBPACK_IMPORTED_MODULE_19__services_custom_Container__["a" /* ContainerApi */],
                __WEBPACK_IMPORTED_MODULE_20__services_custom_VehicleType__["a" /* VehicleTypeApi */],
                __WEBPACK_IMPORTED_MODULE_21__services_custom_Job__["a" /* JobApi */],
                __WEBPACK_IMPORTED_MODULE_22__services_custom_Notification__["a" /* NotificationApi */],
                __WEBPACK_IMPORTED_MODULE_23__services_custom_JobApply__["a" /* JobApplyApi */],
                __WEBPACK_IMPORTED_MODULE_24__services_custom_Requestfordriver__["a" /* RequestfordriverApi */],
                __WEBPACK_IMPORTED_MODULE_25__services_custom_Wallet__["a" /* WalletApi */],
                __WEBPACK_IMPORTED_MODULE_26__services_custom_Otp__["a" /* OtpApi */],
                __WEBPACK_IMPORTED_MODULE_27__services_custom_Rating__["a" /* RatingApi */],
                internalStorageProvider,
                { provide: __WEBPACK_IMPORTED_MODULE_5__storage_storage_swaps__["b" /* SDKStorage */], useClass: __WEBPACK_IMPORTED_MODULE_10__storage_storage_browser__["a" /* StorageBrowser */] },
                { provide: __WEBPACK_IMPORTED_MODULE_12__sockets_socket_driver__["a" /* SocketDriver */], useClass: __WEBPACK_IMPORTED_MODULE_11__sockets_socket_browser__["a" /* SocketBrowser */] }
            ]
        };
    };
    return SDKBrowserModule;
}());
SDKBrowserModule = SDKBrowserModule_1 = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_8__angular_core__["M" /* NgModule */])({
        imports: [__WEBPACK_IMPORTED_MODULE_7__angular_common__["b" /* CommonModule */], __WEBPACK_IMPORTED_MODULE_6__angular_http__["c" /* HttpModule */]],
        declarations: [],
        exports: [],
        providers: [
            __WEBPACK_IMPORTED_MODULE_1__services_core_error_service__["a" /* ErrorHandler */],
            __WEBPACK_IMPORTED_MODULE_13__sockets_socket_connections__["a" /* SocketConnection */]
        ]
    })
], SDKBrowserModule);

/**
* Have Fun!!!
* - Jon
**/






var SDKBrowserModule_1;
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/lb.config.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoopBackConfig; });
/* tslint:disable */
/**
* @module LoopBackConfig
* @description
*
* The LoopBackConfig module help developers to externally
* configure the base url and api version for loopback.io
*
* Example
*
* import { LoopBackConfig } from './sdk';
*
* @Component() // No metadata needed for this module
*
* export class MyApp {
*   constructor() {
*     LoopBackConfig.setBaseURL('http://localhost:3000');
*     LoopBackConfig.setApiVersion('api');
*   }
* }
**/
var LoopBackConfig = (function () {
    function LoopBackConfig() {
    }
    LoopBackConfig.setApiVersion = function (version) {
        if (version === void 0) { version = 'api'; }
        LoopBackConfig.version = version;
    };
    LoopBackConfig.getApiVersion = function () {
        return LoopBackConfig.version;
    };
    LoopBackConfig.setBaseURL = function (url) {
        if (url === void 0) { url = '/'; }
        LoopBackConfig.path = url;
    };
    LoopBackConfig.getPath = function () {
        return LoopBackConfig.path;
    };
    LoopBackConfig.setAuthPrefix = function (authPrefix) {
        if (authPrefix === void 0) { authPrefix = ''; }
        LoopBackConfig.authPrefix = authPrefix;
    };
    LoopBackConfig.getAuthPrefix = function () {
        return LoopBackConfig.authPrefix;
    };
    LoopBackConfig.setDebugMode = function (isEnabled) {
        LoopBackConfig.debug = isEnabled;
    };
    LoopBackConfig.debuggable = function () {
        return LoopBackConfig.debug;
    };
    LoopBackConfig.filterOnUrl = function () {
        LoopBackConfig.filterOn = 'url';
    };
    LoopBackConfig.filterOnHeaders = function () {
        LoopBackConfig.filterOn = 'headers';
    };
    LoopBackConfig.isHeadersFilteringSet = function () {
        return (LoopBackConfig.filterOn === 'headers');
    };
    LoopBackConfig.setSecureWebSockets = function () {
        LoopBackConfig.secure = true;
    };
    LoopBackConfig.unsetSecureWebSockets = function () {
        LoopBackConfig.secure = false;
    };
    LoopBackConfig.isSecureWebSocketsSet = function () {
        return LoopBackConfig.secure;
    };
    LoopBackConfig.setRequestOptionsCredentials = function (withCredentials) {
        if (withCredentials === void 0) { withCredentials = false; }
        LoopBackConfig.withCredentials = withCredentials;
    };
    LoopBackConfig.getRequestOptionsCredentials = function () {
        return LoopBackConfig.withCredentials;
    };
    return LoopBackConfig;
}());

LoopBackConfig.path = '//0.0.0.0:3000';
LoopBackConfig.version = 'api';
LoopBackConfig.authPrefix = '';
LoopBackConfig.debug = true;
LoopBackConfig.filterOn = 'headers';
LoopBackConfig.secure = false;
LoopBackConfig.withCredentials = false;
//# sourceMappingURL=lb.config.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/models/BaseModels.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export AccessToken */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SDKToken; });
/* tslint:disable */
var AccessToken = (function () {
    function AccessToken(data) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `AccessToken`.
     */
    AccessToken.getModelName = function () {
        return "AccessToken";
    };
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of AccessToken for dynamic purposes.
    **/
    AccessToken.factory = function (data) {
        return new AccessToken(data);
    };
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    AccessToken.getModelDefinition = function () {
        return {
            name: 'AccessToken',
            plural: 'AccessTokens',
            properties: {
                "id": {
                    name: 'id',
                    type: 'string'
                },
                "ttl": {
                    name: 'ttl',
                    type: 'number',
                    default: 1209600
                },
                "scopes": {
                    name: 'scopes',
                    type: '["string"]'
                },
                "created": {
                    name: 'created',
                    type: 'Date'
                },
                "userId": {
                    name: 'userId',
                    type: 'string'
                },
                "access_token": {
                    name: 'access_token',
                    type: 'string'
                }
            },
            relations: {
                user: {
                    name: 'user',
                    type: 'User',
                    model: 'User'
                },
            }
        };
    };
    return AccessToken;
}());

var SDKToken = (function () {
    function SDKToken(data) {
        this.id = null;
        this.ttl = null;
        this.scopes = null;
        this.created = null;
        this.userId = null;
        this.user = null;
        this.access_token = null;
        this.rememberMe = null;
        Object.assign(this, data);
    }
    return SDKToken;
}());

//# sourceMappingURL=BaseModels.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/models/Container.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Container; });
/* tslint:disable */
var Container = (function () {
    function Container(data) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `Container`.
     */
    Container.getModelName = function () {
        return "Container";
    };
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of Container for dynamic purposes.
    **/
    Container.factory = function (data) {
        return new Container(data);
    };
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    Container.getModelDefinition = function () {
        return {
            name: 'Container',
            plural: 'Containers',
            path: 'Containers',
            idName: 'id',
            properties: {
                "id": {
                    name: 'id',
                    type: 'number'
                },
            },
            relations: {}
        };
    };
    return Container;
}());

//# sourceMappingURL=Container.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/models/Email.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Email; });
/* tslint:disable */
var Email = (function () {
    function Email(data) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `Email`.
     */
    Email.getModelName = function () {
        return "Email";
    };
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of Email for dynamic purposes.
    **/
    Email.factory = function (data) {
        return new Email(data);
    };
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    Email.getModelDefinition = function () {
        return {
            name: 'Email',
            plural: 'Emails',
            path: 'Emails',
            idName: 'id',
            properties: {
                "to": {
                    name: 'to',
                    type: 'string'
                },
                "from": {
                    name: 'from',
                    type: 'string'
                },
                "subject": {
                    name: 'subject',
                    type: 'string'
                },
                "text": {
                    name: 'text',
                    type: 'string'
                },
                "html": {
                    name: 'html',
                    type: 'string'
                },
                "id": {
                    name: 'id',
                    type: 'number'
                },
            },
            relations: {}
        };
    };
    return Email;
}());

//# sourceMappingURL=Email.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/models/FireLoop.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FireLoop; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__index__ = __webpack_require__("../../../../../src/app/sdk/models/index.ts");
/* tslint:disable */

var FireLoop = (function () {
    function FireLoop(socket, models) {
        this.socket = socket;
        this.models = models;
        this.references = {};
    }
    FireLoop.prototype.ref = function (model) {
        var name = model.getModelName();
        model.models = this.models;
        this.references[name] = new __WEBPACK_IMPORTED_MODULE_0__index__["a" /* FireLoopRef */](model, this.socket);
        return this.references[name];
    };
    return FireLoop;
}());

//# sourceMappingURL=FireLoop.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/models/FireLoopRef.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FireLoopRef; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_observable_merge__ = __webpack_require__("../../../../rxjs/add/observable/merge.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_observable_merge___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_rxjs_add_observable_merge__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__ = __webpack_require__("../../../../rxjs/Subject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("../../../../rxjs/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_merge__ = __webpack_require__("../../../../rxjs/add/operator/merge.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_merge___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_merge__);
/* tslint:disable */




/**
 * @class FireLoopRef<T>
 * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
 * @license MIT
 * @description
 * This class allows to create FireLoop References which will be in sync with
 * Server. It also allows to create FireLoop Reference Childs, that allows to
 * persist data according the generic model relationships.
 **/
var FireLoopRef = (function () {
    /**
    * @method constructor
    * @param {any} model The model we want to create a reference
    * @param {SocketConnection} socket Socket connection to handle events
    * @param {FireLoopRef<any>} parent Parent FireLoop model reference
    * @param {string} relationship The defined model relationship
    * @description
    * The constructor will receive the required parameters and then will register this reference
    * into the server, needed to allow multiple references for the same model.
    * This ids are referenced into this specific client connection and won't have issues
    * with other client ids.
    **/
    function FireLoopRef(model, socket, parent, relationship) {
        if (parent === void 0) { parent = null; }
        if (relationship === void 0) { relationship = null; }
        this.model = model;
        this.socket = socket;
        this.parent = parent;
        this.relationship = relationship;
        // Reference ID
        this.id = this.buildId();
        // Model Childs
        this.childs = {};
        // Disposable Events
        this.disposable = {};
        this.socket.emit("Subscribe." + (!parent ? model.getModelName() : parent.model.getModelName()), { id: this.id, scope: model.getModelName(), relationship: relationship });
        return this;
    }
    /**
    * @method dispose
    * @return {void}
    * @description
    * This method is super important to avoid memory leaks in the server.
    * This method requires to be called on components destroy
    *
    * ngOnDestroy() {
    *  this.someRef.dispose()
    * }
    **/
    FireLoopRef.prototype.dispose = function () {
        var _this = this;
        var subscription = this.operation('dispose', {}).subscribe(function () {
            Object.keys(_this.disposable).forEach(function (channel) {
                _this.socket.removeListener(channel, _this.disposable[channel]);
                _this.socket.removeAllListeners(channel);
            });
            subscription.unsubscribe();
        });
    };
    /**
    * @method upsert
    * @param {T} data Persisted model instance
    * @return {Observable<T>}
    * @description
    * Operation wrapper for upsert function.
    **/
    FireLoopRef.prototype.upsert = function (data) {
        return this.operation('upsert', data);
    };
    /**
    * @method create
    * @param {T} data Persisted model instance
    * @return {Observable<T>}
    * @description
    * Operation wrapper for create function.
    **/
    FireLoopRef.prototype.create = function (data) {
        return this.operation('create', data);
    };
    /**
    * @method remove
    * @param {T} data Persisted model instance
    * @return {Observable<T>}
    * @description
    * Operation wrapper for remove function.
    **/
    FireLoopRef.prototype.remove = function (data) {
        return this.operation('remove', data);
    };
    /**
    * @method remote
    * @param {string} method Remote method name
    * @param {any[]=} params Parameters to be applied into the remote method
    * @param {boolean} broadcast Flag to define if the method results should be broadcasted
    * @return {Observable<any>}
    * @description
    * This method calls for any remote method. It is flexible enough to
    * allow you call either built-in or custom remote methods.
    *
    * FireLoop provides this interface to enable calling remote methods
    * but also to optionally send any defined accept params that will be
    * applied within the server.
    **/
    FireLoopRef.prototype.remote = function (method, params, broadcast) {
        if (broadcast === void 0) { broadcast = false; }
        return this.operation('remote', { method: method, params: params, broadcast: broadcast });
    };
    /**
    * @method onRemote
    * @param {string} method Remote method name
    * @return {Observable<any>}
    * @description
    * This method listen for public broadcasted remote method results. If the remote method
    * execution is not public only the owner will receive the result data.
    **/
    FireLoopRef.prototype.onRemote = function (method) {
        var event = 'remote';
        if (!this.relationship) {
            event = this.model.getModelName() + "." + event;
        }
        else {
            event = this.parent.model.getModelName() + "." + this.relationship + "." + event;
        }
        return this.broadcasts(event, {});
    };
    /**
    * @method on
    * @param {string} event Event name
    * @param {LoopBackFilter} filter LoopBack query filter
    * @return {Observable<T>}
    * @description
    * Listener for different type of events. Valid events are:
    *   - change (Triggers on any model change -create, update, remove-)
    *   - value (Triggers on new entries)
    *   - child_added (Triggers when a child is added)
    *   - child_updated (Triggers when a child is updated)
    *   - child_removed (Triggers when a child is removed)
    **/
    FireLoopRef.prototype.on = function (event, filter) {
        if (filter === void 0) { filter = { limit: 100, order: 'id DESC' }; }
        if (event === 'remote') {
            throw new Error('The "remote" event is not allowed using "on()" method, use "onRemote()" instead');
        }
        var request;
        if (!this.relationship) {
            event = this.model.getModelName() + "." + event;
            request = { filter: filter };
        }
        else {
            event = this.parent.model.getModelName() + "." + this.relationship + "." + event;
            request = { filter: filter, parent: this.parent.instance };
        }
        if (event.match(/(value|change|stats)/)) {
            return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].merge(this.pull(event, request), this.broadcasts(event, request));
        }
        else {
            return this.broadcasts(event, request);
        }
    };
    /**
    * @method stats
    * @param {LoopBackFilter=} filter LoopBack query filter
    * @return {Observable<T>}
    * @description
    * Listener for real-time statistics, will trigger on every
    * statistic modification.
    * TIP: You can improve performance by adding memcached to LoopBack models.
    **/
    FireLoopRef.prototype.stats = function (filter) {
        return this.on('stats', filter);
    };
    /**
    * @method make
    * @param {any} instance Persisted model instance reference
    * @return {Observable<T>}
    * @description
    * This method will set a model instance into this a new FireLoop Reference.
    * This allows to persiste parentship when creating related instances.
    *
    * It also allows to have multiple different persisted instance references to same model.
    * otherwise if using singleton will replace a previous instance for a new instance, when
    * we actually want to have more than 1 instance of same model.
    **/
    FireLoopRef.prototype.make = function (instance) {
        var reference = new FireLoopRef(this.model, this.socket);
        reference.instance = instance;
        return reference;
    };
    /**
    * @method child
    * @param {string} relationship A defined model relationship
    * @return {FireLoopRef<T>}
    * @description
    * This method creates child references, which will persist related model
    * instances. e.g. Room.messages, where messages belongs to a specific Room.
    **/
    FireLoopRef.prototype.child = function (relationship) {
        // Return singleton instance
        if (this.childs[relationship]) {
            return this.childs[relationship];
        }
        // Try to get relation settings from current model
        var settings = this.model.getModelDefinition().relations[relationship];
        // Verify the relationship actually exists
        if (!settings) {
            throw new Error("Invalid model relationship " + this.model.getModelName() + " <-> " + relationship + ", verify your model settings.");
        }
        // Verify if the relationship model is public
        if (settings.model === '') {
            throw new Error("Relationship model is private, cam't use " + relationship + " unless you set your model as public.");
        }
        // Lets get a model reference and add a reference for all of the models
        var model = this.model.models.get(settings.model);
        model.models = this.model.models;
        // If everything goes well, we will store a child reference and return it.
        this.childs[relationship] = new FireLoopRef(model, this.socket, this, relationship);
        return this.childs[relationship];
    };
    /**
    * @method pull
    * @param {string} event Event name
    * @param {any} request Type of request, can be LB-only filter or FL+LB filter
    * @return {Observable<T>}
    * @description
    * This method will pull initial data from server
    **/
    FireLoopRef.prototype.pull = function (event, request) {
        var sbj = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["Subject"]();
        var that = this;
        var nowEvent = event + ".pull.requested." + this.id;
        this.socket.emit(event + ".pull.request." + this.id, request);
        function pullNow(data) {
            if (that.socket.removeListener) {
                that.socket.removeListener(nowEvent, pullNow);
            }
            sbj.next(data);
        }
        ;
        this.socket.on(nowEvent, pullNow);
        return sbj.asObservable();
    };
    /**
    * @method broadcasts
    * @param {string} event Event name
    * @param {any} request Type of request, can be LB-only filter or FL+LB filter
    * @return {Observable<T>}
    * @description
    * This will listen for public broadcasts announces and then request
    * for data according a specific client request, not shared with other clients.
    **/
    FireLoopRef.prototype.broadcasts = function (event, request) {
        var sbj = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["Subject"]();
        var channels = {
            announce: event + ".broadcast.announce." + this.id,
            broadcast: event + ".broadcast." + this.id
        };
        var that = this;
        // Announces Handler
        this.disposable[channels.announce] = function (res) {
            that.socket.emit(event + ".broadcast.request." + that.id, request);
        };
        // Broadcasts Handler
        this.disposable[channels.broadcast] = function (data) {
            sbj.next(data);
        };
        this.socket.on(channels.announce, this.disposable[channels.announce]);
        this.socket.on(channels.broadcast, this.disposable[channels.broadcast]);
        return sbj.asObservable();
    };
    /**
    * @method operation
    * @param {string} event Event name
    * @param {any} data Any type of data sent to the server
    * @return {Observable<T>}
    * @description
    * This internal method will run operations depending on current context
    **/
    FireLoopRef.prototype.operation = function (event, data) {
        if (!this.relationship) {
            event = this.model.getModelName() + "." + event + "." + this.id;
        }
        else {
            event = this.parent.model.getModelName() + "." + this.relationship + "." + event + "." + this.id;
        }
        var subject = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["Subject"]();
        var config = {
            data: data,
            parent: this.parent && this.parent.instance ? this.parent.instance : null
        };
        this.socket.emit(event, config);
        var resultEvent = '';
        if (!this.relationship) {
            resultEvent = this.model.getModelName() + ".value.result." + this.id;
        }
        else {
            resultEvent = this.parent.model.getModelName() + "." + this.relationship + ".value.result." + this.id;
        }
        this.socket.on(resultEvent, function (res) {
            if (res.error) {
                subject.error(res);
            }
            else {
                subject.next(res);
            }
        });
        if (event.match('dispose')) {
            setTimeout(function () { return subject.next(); });
        }
        // This event listener will be wiped within socket.connections
        this.socket.sharedObservables.sharedOnDisconnect.subscribe(function () { return subject.complete(); });
        return subject.asObservable().catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error); });
    };
    /**
    * @method buildId
    * @return {number}
    * @description
    * This internal method build an ID for this reference, this allows to have
    * multiple references for the same model or relationships.
    **/
    FireLoopRef.prototype.buildId = function () {
        return Date.now() + Math.floor(Math.random() * 100800) *
            Math.floor(Math.random() * 100700) *
            Math.floor(Math.random() * 198500);
    };
    return FireLoopRef;
}());

//# sourceMappingURL=FireLoopRef.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/models/Job.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Job; });
/* tslint:disable */
var Job = (function () {
    function Job(data) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `Job`.
     */
    Job.getModelName = function () {
        return "Job";
    };
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of Job for dynamic purposes.
    **/
    Job.factory = function (data) {
        return new Job(data);
    };
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    Job.getModelDefinition = function () {
        return {
            name: 'Job',
            plural: 'Jobs',
            path: 'Jobs',
            idName: 'id',
            properties: {
                "title": {
                    name: 'title',
                    type: 'string'
                },
                "weight": {
                    name: 'weight',
                    type: 'string'
                },
                "source": {
                    name: 'source',
                    type: 'any'
                },
                "destination": {
                    name: 'destination',
                    type: 'any'
                },
                "insurance": {
                    name: 'insurance',
                    type: 'boolean'
                },
                "valueOfLoad": {
                    name: 'valueOfLoad',
                    type: 'number'
                },
                "insPremium": {
                    name: 'insPremium',
                    type: 'number'
                },
                "budget": {
                    name: 'budget',
                    type: 'number'
                },
                "netPrice": {
                    name: 'netPrice',
                    type: 'number'
                },
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "peopleId": {
                    name: 'peopleId',
                    type: 'any'
                },
            },
            relations: {
                people: {
                    name: 'people',
                    type: 'any',
                    model: '',
                    relationType: 'belongsTo',
                    keyFrom: 'peopleId',
                    keyTo: 'id'
                },
                jobApplies: {
                    name: 'jobApplies',
                    type: 'any[]',
                    model: '',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'jobId'
                },
            }
        };
    };
    return Job;
}());

//# sourceMappingURL=Job.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/models/JobApply.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JobApply; });
/* tslint:disable */
var JobApply = (function () {
    function JobApply(data) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `JobApply`.
     */
    JobApply.getModelName = function () {
        return "JobApply";
    };
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of JobApply for dynamic purposes.
    **/
    JobApply.factory = function (data) {
        return new JobApply(data);
    };
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    JobApply.getModelDefinition = function () {
        return {
            name: 'JobApply',
            plural: 'JobApplies',
            path: 'JobApplies',
            idName: 'id',
            properties: {
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "jobId": {
                    name: 'jobId',
                    type: 'any'
                },
                "peopleId": {
                    name: 'peopleId',
                    type: 'any'
                },
            },
            relations: {
                job: {
                    name: 'job',
                    type: 'any',
                    model: '',
                    relationType: 'belongsTo',
                    keyFrom: 'jobId',
                    keyTo: 'id'
                },
                people: {
                    name: 'people',
                    type: 'any',
                    model: '',
                    relationType: 'belongsTo',
                    keyFrom: 'peopleId',
                    keyTo: 'id'
                },
            }
        };
    };
    return JobApply;
}());

//# sourceMappingURL=JobApply.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/models/Otp.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export Otp */
/* tslint:disable */
var Otp = (function () {
    function Otp(data) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `Otp`.
     */
    Otp.getModelName = function () {
        return "Otp";
    };
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of Otp for dynamic purposes.
    **/
    Otp.factory = function (data) {
        return new Otp(data);
    };
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    Otp.getModelDefinition = function () {
        return {
            name: 'Otp',
            plural: 'Otps',
            path: 'Otps',
            idName: 'id',
            properties: {
                "id": {
                    name: 'id',
                    type: 'any'
                },
            },
            relations: {}
        };
    };
    return Otp;
}());

//# sourceMappingURL=Otp.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/models/People.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return People; });
/* tslint:disable */
var People = (function () {
    function People(data) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `People`.
     */
    People.getModelName = function () {
        return "People";
    };
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of People for dynamic purposes.
    **/
    People.factory = function (data) {
        return new People(data);
    };
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    People.getModelDefinition = function () {
        return {
            name: 'People',
            plural: 'People',
            path: 'People',
            idName: 'id',
            properties: {
                "realm": {
                    name: 'realm',
                    type: 'string'
                },
                "username": {
                    name: 'username',
                    type: 'string'
                },
                "email": {
                    name: 'email',
                    type: 'string'
                },
                "companyName": {
                    name: 'companyName',
                    type: 'string',
                    default: ''
                },
                "emailVerified": {
                    name: 'emailVerified',
                    type: 'boolean'
                },
                "licenceIds": {
                    name: 'licenceIds',
                    type: 'Array&lt;any&gt;'
                },
                "governmentIds": {
                    name: 'governmentIds',
                    type: 'Array&lt;any&gt;'
                },
                "firebaseTokens": {
                    name: 'firebaseTokens',
                    type: 'Array&lt;any&gt;',
                    default: []
                },
                "createdAt": {
                    name: 'createdAt',
                    type: 'Date'
                },
                "updatedAt": {
                    name: 'updatedAt',
                    type: 'Date'
                },
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "password": {
                    name: 'password',
                    type: 'string'
                },
            },
            relations: {
                accessTokens: {
                    name: 'accessTokens',
                    type: 'any[]',
                    model: '',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'userId'
                },
                vehicles: {
                    name: 'vehicles',
                    type: 'any[]',
                    model: '',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'peopleId'
                },
                requestfordrivers: {
                    name: 'requestfordrivers',
                    type: 'any[]',
                    model: '',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'peopleId'
                },
                identities: {
                    name: 'identities',
                    type: 'any[]',
                    model: '',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'peopleId'
                },
                credentials: {
                    name: 'credentials',
                    type: 'any[]',
                    model: '',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'peopleId'
                },
            }
        };
    };
    return People;
}());

//# sourceMappingURL=People.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/models/Rating.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export Rating */
/* tslint:disable */
var Rating = (function () {
    function Rating(data) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `Rating`.
     */
    Rating.getModelName = function () {
        return "Rating";
    };
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of Rating for dynamic purposes.
    **/
    Rating.factory = function (data) {
        return new Rating(data);
    };
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    Rating.getModelDefinition = function () {
        return {
            name: 'Rating',
            plural: 'Ratings',
            path: 'Ratings',
            idName: 'id',
            properties: {
                "userRating": {
                    name: 'userRating',
                    type: 'number'
                },
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "jobId": {
                    name: 'jobId',
                    type: 'any'
                },
                "providerId": {
                    name: 'providerId',
                    type: 'any'
                },
                "peopleId": {
                    name: 'peopleId',
                    type: 'any'
                },
            },
            relations: {
                job: {
                    name: 'job',
                    type: 'any',
                    model: '',
                    relationType: 'belongsTo',
                    keyFrom: 'jobId',
                    keyTo: 'id'
                },
                provider: {
                    name: 'provider',
                    type: 'any',
                    model: '',
                    relationType: 'belongsTo',
                    keyFrom: 'providerId',
                    keyTo: 'id'
                },
                people: {
                    name: 'people',
                    type: 'any',
                    model: '',
                    relationType: 'belongsTo',
                    keyFrom: 'peopleId',
                    keyTo: 'id'
                },
            }
        };
    };
    return Rating;
}());

//# sourceMappingURL=Rating.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/models/Requestfordriver.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export Requestfordriver */
/* tslint:disable */
var Requestfordriver = (function () {
    function Requestfordriver(data) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `Requestfordriver`.
     */
    Requestfordriver.getModelName = function () {
        return "Requestfordriver";
    };
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of Requestfordriver for dynamic purposes.
    **/
    Requestfordriver.factory = function (data) {
        return new Requestfordriver(data);
    };
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    Requestfordriver.getModelDefinition = function () {
        return {
            name: 'Requestfordriver',
            plural: 'Requestfordrivers',
            path: 'Requestfordrivers',
            idName: 'id',
            properties: {
                "status": {
                    name: 'status',
                    type: 'boolean',
                    default: false
                },
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "ownerId": {
                    name: 'ownerId',
                    type: 'any'
                },
                "vehicleId": {
                    name: 'vehicleId',
                    type: 'any'
                },
                "peopleId": {
                    name: 'peopleId',
                    type: 'any'
                },
            },
            relations: {
                owner: {
                    name: 'owner',
                    type: 'any',
                    model: '',
                    relationType: 'belongsTo',
                    keyFrom: 'ownerId',
                    keyTo: 'id'
                },
                vehicle: {
                    name: 'vehicle',
                    type: 'any',
                    model: '',
                    relationType: 'belongsTo',
                    keyFrom: 'vehicleId',
                    keyTo: 'id'
                },
            }
        };
    };
    return Requestfordriver;
}());

//# sourceMappingURL=Requestfordriver.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/models/Transaction.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Transaction; });
/* tslint:disable */
var Transaction = (function () {
    function Transaction(data) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `Transaction`.
     */
    Transaction.getModelName = function () {
        return "Transaction";
    };
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of Transaction for dynamic purposes.
    **/
    Transaction.factory = function (data) {
        return new Transaction(data);
    };
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    Transaction.getModelDefinition = function () {
        return {
            name: 'Transaction',
            plural: 'Transactions',
            path: 'Transactions',
            idName: 'id',
            properties: {
                "id": {
                    name: 'id',
                    type: 'any'
                },
            },
            relations: {}
        };
    };
    return Transaction;
}());

//# sourceMappingURL=Transaction.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/models/Vehicle.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Vehicle; });
/* tslint:disable */
var Vehicle = (function () {
    function Vehicle(data) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `Vehicle`.
     */
    Vehicle.getModelName = function () {
        return "Vehicle";
    };
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of Vehicle for dynamic purposes.
    **/
    Vehicle.factory = function (data) {
        return new Vehicle(data);
    };
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    Vehicle.getModelDefinition = function () {
        return {
            name: 'Vehicle',
            plural: 'Vehicles',
            path: 'Vehicles',
            idName: 'id',
            properties: {
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "peopleId": {
                    name: 'peopleId',
                    type: 'any'
                },
            },
            relations: {
                people: {
                    name: 'people',
                    type: 'any',
                    model: '',
                    relationType: 'belongsTo',
                    keyFrom: 'peopleId',
                    keyTo: 'id'
                },
            }
        };
    };
    return Vehicle;
}());

//# sourceMappingURL=Vehicle.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/models/VehicleType.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VehicleType; });
/* tslint:disable */
var VehicleType = (function () {
    function VehicleType(data) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `VehicleType`.
     */
    VehicleType.getModelName = function () {
        return "VehicleType";
    };
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of VehicleType for dynamic purposes.
    **/
    VehicleType.factory = function (data) {
        return new VehicleType(data);
    };
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    VehicleType.getModelDefinition = function () {
        return {
            name: 'VehicleType',
            plural: 'VehicleTypes',
            path: 'VehicleTypes',
            idName: 'id',
            properties: {
                "name": {
                    name: 'name',
                    type: 'string'
                },
                "image": {
                    name: 'image',
                    type: 'string'
                },
                "id": {
                    name: 'id',
                    type: 'any'
                },
            },
            relations: {}
        };
    };
    return VehicleType;
}());

//# sourceMappingURL=VehicleType.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/models/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Email__ = __webpack_require__("../../../../../src/app/sdk/models/Email.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__People__ = __webpack_require__("../../../../../src/app/sdk/models/People.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Transaction__ = __webpack_require__("../../../../../src/app/sdk/models/Transaction.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Vehicle__ = __webpack_require__("../../../../../src/app/sdk/models/Vehicle.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Container__ = __webpack_require__("../../../../../src/app/sdk/models/Container.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__VehicleType__ = __webpack_require__("../../../../../src/app/sdk/models/VehicleType.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__Job__ = __webpack_require__("../../../../../src/app/sdk/models/Job.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__JobApply__ = __webpack_require__("../../../../../src/app/sdk/models/JobApply.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__BaseModels__ = __webpack_require__("../../../../../src/app/sdk/models/BaseModels.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__FireLoopRef__ = __webpack_require__("../../../../../src/app/sdk/models/FireLoopRef.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_9__FireLoopRef__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__Requestfordriver__ = __webpack_require__("../../../../../src/app/sdk/models/Requestfordriver.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__Otp__ = __webpack_require__("../../../../../src/app/sdk/models/Otp.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__Rating__ = __webpack_require__("../../../../../src/app/sdk/models/Rating.ts");
/* unused harmony namespace reexport */
/* tslint:disable */













//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/services/core/auth.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoopBackAuth; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__storage_storage_swaps__ = __webpack_require__("../../../../../src/app/sdk/storage/storage.swaps.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_BaseModels__ = __webpack_require__("../../../../../src/app/sdk/models/BaseModels.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



/**
* @author Jonathan Casarrubias <twitter:@johncasarrubias> <github:@mean-expert-official>
* @module SocketConnection
* @license MIT
* @description
* This module handle socket connections and return singleton instances for each
* connection, it will use the SDK Socket Driver Available currently supporting
* Angular 2 for web, NativeScript 2 and Angular Universal.
**/
var LoopBackAuth = (function () {
    /**
     * @method constructor
     * @param {InternalStorage} storage Internal Storage Driver
     * @description
     * The constructor will initialize the token loading data from storage
     **/
    function LoopBackAuth(storage) {
        this.storage = storage;
        /**
         * @type {SDKToken}
         **/
        this.token = new __WEBPACK_IMPORTED_MODULE_2__models_BaseModels__["a" /* SDKToken */]();
        /**
         * @type {string}
         **/
        this.prefix = '$LoopBackSDK$';
        this.token.id = this.load('id');
        this.token.user = this.load('user');
        this.token.userId = this.load('userId');
        this.token.created = this.load('created');
        this.token.ttl = this.load('ttl');
        this.token.access_token = this.load('access_token');
        this.token.rememberMe = this.load('rememberMe');
    }
    /**
     * @method setRememberMe
     * @param {boolean} value Flag to remember credentials
     * @return {void}
     * @description
     * This method will set a flag in order to remember the current credentials
     **/
    LoopBackAuth.prototype.setRememberMe = function (value) {
        this.token.rememberMe = value;
    };
    /**
     * @method setUser
     * @param {any} user Any type of user model
     * @return {void}
     * @description
     * This method will update the user information and persist it if the
     * rememberMe flag is set.
     **/
    LoopBackAuth.prototype.setUser = function (user) {
        this.token.user = user;
        this.save();
    };
    /**
     * @method setToken
     * @param {SDKToken} token SDKToken or casted AccessToken instance
     * @return {void}
     * @description
     * This method will set a flag in order to remember the current credentials
     **/
    LoopBackAuth.prototype.setToken = function (token) {
        this.token = Object.assign({}, this.token, token);
        this.save();
    };
    /**
     * @method getToken
     * @return {void}
     * @description
     * This method will set a flag in order to remember the current credentials.
     **/
    LoopBackAuth.prototype.getToken = function () {
        return this.token;
    };
    /**
     * @method getAccessTokenId
     * @return {string}
     * @description
     * This method will return the actual token string, not the object instance.
     **/
    LoopBackAuth.prototype.getAccessTokenId = function () {
        return this.token.id;
    };
    /**
     * @method getCurrentUserId
     * @return {any}
     * @description
     * This method will return the current user id, it can be number or string.
     **/
    LoopBackAuth.prototype.getCurrentUserId = function () {
        return this.token.userId;
    };
    /**
     * @method getCurrentUserData
     * @return {any}
     * @description
     * This method will return the current user instance.
     **/
    LoopBackAuth.prototype.getCurrentUserData = function () {
        return (typeof this.token.user === 'string') ? JSON.parse(this.token.user) : this.token.user;
    };
    /**
     * @method save
     * @return {boolean} Whether or not the information was saved
     * @description
     * This method will save in either local storage or cookies the current credentials.
     * But only if rememberMe is enabled.
     **/
    LoopBackAuth.prototype.save = function () {
        var today = new Date();
        var expires;
        if (this.token.rememberMe) {
            if (this.token.ttl == -1 || !this.token.ttl) {
                expires = new Date(today.getTime() + (365 * 24 * 60 * 60 * 1000));
            }
            else {
                expires = new Date(today.getTime() + (this.token.ttl * 1000));
            }
        }
        /* expires = new Date(today.getTime() + (this.token.ttl * 1000));
         expires = "Session";*/
        this.persist('id', this.token.id, expires);
        this.persist('user', this.token.user, expires);
        this.persist('userId', this.token.userId, expires);
        this.persist('created', this.token.created, expires);
        this.persist('ttl', this.token.ttl, expires);
        this.persist('rememberMe', this.token.rememberMe, expires);
        return true;
    };
    ;
    /**
     * @method load
     * @param {string} prop Property name
     * @return {any} Any information persisted in storage
     * @description
     * This method will load either from local storage or cookies the provided property.
     **/
    LoopBackAuth.prototype.load = function (prop) {
        return this.storage.get("" + this.prefix + prop);
    };
    /**
     * @method clear
     * @return {void}
     * @description
     * This method will clear cookies or the local storage.
     **/
    LoopBackAuth.prototype.clear = function () {
        var _this = this;
        Object.keys(this.token).forEach(function (prop) { return _this.storage.remove("" + _this.prefix + prop); });
        this.token = new __WEBPACK_IMPORTED_MODULE_2__models_BaseModels__["a" /* SDKToken */]();
    };
    /**
     * @method persist
     * @return {void}
     * @description
     * This method saves values to storage
     **/
    LoopBackAuth.prototype.persist = function (prop, value, expires) {
        try {
            this.storage.set("" + this.prefix + prop, (typeof value === 'object') ? JSON.stringify(value) : value, this.token.rememberMe ? expires : null);
        }
        catch (err) {
            console.error('Cannot access local/session storage:', err);
        }
    };
    return LoopBackAuth;
}());
LoopBackAuth = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __param(0, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_1__storage_storage_swaps__["a" /* InternalStorage */])),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__storage_storage_swaps__["a" /* InternalStorage */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__storage_storage_swaps__["a" /* InternalStorage */]) === "function" && _a || Object])
], LoopBackAuth);

var _a;
//# sourceMappingURL=auth.service.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/services/core/base.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaseLoopBackApi; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__search_params__ = __webpack_require__("../../../../../src/app/sdk/services/core/search.params.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__error_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__auth_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__lb_config__ = __webpack_require__("../../../../../src/app/sdk/lb.config.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__custom_SDKModels__ = __webpack_require__("../../../../../src/app/sdk/services/custom/SDKModels.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_Subject__ = __webpack_require__("../../../../rxjs/Subject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_Subject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_catch__ = __webpack_require__("../../../../rxjs/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__sockets_socket_connections__ = __webpack_require__("../../../../../src/app/sdk/sockets/socket.connections.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
/* tslint:disable */











/**
* @module BaseLoopBackApi
* @author Jonathan Casarrubias <@johncasarrubias> <github:jonathan-casarrubias>
* @author Nikolay Matiushenkov <https://github.com/mnvx>
* @license MIT
* @description
* Abstract class that will be implemented in every custom service automatically built
* by the sdk builder.
* It provides the core functionallity for every API call, either by HTTP Calls or by
* WebSockets.
**/
var BaseLoopBackApi = (function () {
    function BaseLoopBackApi(http, connection, models, auth, searchParams, errorHandler) {
        this.http = http;
        this.connection = connection;
        this.models = models;
        this.auth = auth;
        this.searchParams = searchParams;
        this.errorHandler = errorHandler;
        this.model = this.models.get(this.getModelName());
    }
    /**
     * @method request
     * @param {string}  method      Request method (GET, POST, PUT)
     * @param {string}  url         Request url (my-host/my-url/:id)
     * @param {any}     routeParams Values of url parameters
     * @param {any}     urlParams   Parameters for building url (filter and other)
     * @param {any}     postBody    Request postBody
     * @return {Observable<any>}
     * @description
     * This is a core method, every HTTP Call will be done from here, every API Service will
     * extend this class and use this method to get RESTful communication.
     **/
    BaseLoopBackApi.prototype.request = function (method, url, routeParams, urlParams, postBody, pubsub, customHeaders) {
        var _this = this;
        if (routeParams === void 0) { routeParams = {}; }
        if (urlParams === void 0) { urlParams = {}; }
        if (postBody === void 0) { postBody = {}; }
        if (pubsub === void 0) { pubsub = false; }
        // Transpile route variables to the actual request Values
        Object.keys(routeParams).forEach(function (key) {
            url = url.replace(new RegExp(":" + key + "(\/|$)", "g"), routeParams[key] + "$1");
        });
        if (pubsub) {
            if (url.match(/fk/)) {
                var arr = url.split('/');
                arr.pop();
                url = arr.join('/');
            }
            var event = ("[" + method + "]" + url).replace(/\?/, '');
            var subject_1 = new __WEBPACK_IMPORTED_MODULE_7_rxjs_Subject__["Subject"]();
            this.connection.on(event, function (res) { return subject_1.next(res); });
            return subject_1.asObservable();
        }
        else {
            // Headers to be sent
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append('Content-Type', 'application/json');
            // Authenticate request
            this.authenticate(url, headers);
            // Body fix for built in remote methods using "data", "options" or "credentials
            // that are the actual body, Custom remote method properties are different and need
            // to be wrapped into a body object
            var body = void 0;
            var postBodyKeys = typeof postBody === 'object' ? Object.keys(postBody) : [];
            if (postBodyKeys.length === 1) {
                body = postBody[postBodyKeys.shift()];
            }
            else {
                body = postBody;
            }
            var filter = '';
            // Separate filter object from url params and add to search query
            if (urlParams.filter) {
                if (__WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].isHeadersFilteringSet()) {
                    headers.append('filter', JSON.stringify(urlParams.filter));
                }
                else {
                    filter = "?filter=" + encodeURIComponent(JSON.stringify(urlParams.filter));
                }
                delete urlParams.filter;
            }
            // Separate where object from url params and add to search query
            /**
            CODE BELOW WILL GENERATE THE FOLLOWING ISSUES:
            - https://github.com/mean-expert-official/loopback-sdk-builder/issues/356
            - https://github.com/mean-expert-official/loopback-sdk-builder/issues/328
            if (urlParams.where) {
              headers.append('where', JSON.stringify(urlParams.where));
              delete urlParams.where;
            }
            **/
            if (typeof customHeaders === 'function') {
                headers = customHeaders(headers);
            }
            this.searchParams.setJSON(urlParams);
            var request = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Request */](new __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* RequestOptions */]({
                headers: headers,
                method: method,
                url: "" + url + filter,
                search: Object.keys(urlParams).length > 0 ? this.searchParams.getURLSearchParams() : null,
                body: body ? JSON.stringify(body) : undefined,
                withCredentials: __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getRequestOptionsCredentials()
            }));
            return this.http.request(request)
                .map(function (res) { return (res.text() != "" ? res.json() : {}); })
                .catch(function (e) { return _this.errorHandler.handleError(e); });
        }
    };
    /**
     * @method authenticate
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @param {string} url Server URL
     * @param {Headers} headers HTTP Headers
     * @return {void}
     * @description
     * This method will try to authenticate using either an access_token or basic http auth
     */
    BaseLoopBackApi.prototype.authenticate = function (url, headers) {
        if (this.auth.getAccessTokenId()) {
            headers.append('Authorization', __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getAuthPrefix() + this.auth.getAccessTokenId());
        }
    };
    /**
     * @method create
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @param {T} data Generic data type
     * @return {Observable<T>}
     * @description
     * Generic create method
     */
    BaseLoopBackApi.prototype.create = function (data, customHeaders) {
        var _this = this;
        return this.request('POST', [
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getPath(),
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getApiVersion(),
            this.model.getModelDefinition().path
        ].join('/'), undefined, undefined, { data: data }, null, customHeaders).map(function (data) { return _this.model.factory(data); });
    };
    /**
     * @method onCreate
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @param {T[]} data Generic data type array
     * @return {Observable<T[]>}
     * @description
     * Generic pubsub oncreate many method
     */
    BaseLoopBackApi.prototype.onCreate = function (data) {
        var _this = this;
        return this.request('POST', [
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getPath(),
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getApiVersion(),
            this.model.getModelDefinition().path
        ].join('/'), undefined, undefined, { data: data }, true)
            .map(function (datum) { return datum.map(function (data) { return _this.model.factory(data); }); });
    };
    /**
     * @method createMany
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @param {T[]} data Generic data type array
     * @return {Observable<T[]>}
     * @description
     * Generic create many method
     */
    BaseLoopBackApi.prototype.createMany = function (data, customHeaders) {
        var _this = this;
        return this.request('POST', [
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getPath(),
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getApiVersion(),
            this.model.getModelDefinition().path
        ].join('/'), undefined, undefined, { data: data }, null, customHeaders)
            .map(function (datum) { return datum.map(function (data) { return _this.model.factory(data); }); });
    };
    /**
     * @method onCreateMany
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @param {T[]} data Generic data type array
     * @return {Observable<T[]>}
     * @description
     * Generic create many method
     */
    BaseLoopBackApi.prototype.onCreateMany = function (data) {
        var _this = this;
        return this.request('POST', [
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getPath(),
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getApiVersion(),
            this.model.getModelDefinition().path
        ].join('/'), undefined, undefined, { data: data }, true)
            .map(function (datum) { return datum.map(function (data) { return _this.model.factory(data); }); });
    };
    /**
     * @method findById
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @param {any} data Generic data type
     * @return {Observable<T>}
     * @description
     * Generic findById method
     */
    BaseLoopBackApi.prototype.findById = function (id, filter, customHeaders) {
        var _this = this;
        if (filter === void 0) { filter = {}; }
        var _urlParams = {};
        if (filter)
            _urlParams.filter = filter;
        return this.request('GET', [
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getPath(),
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getApiVersion(),
            this.model.getModelDefinition().path,
            ':id'
        ].join('/'), { id: id }, _urlParams, undefined, null, customHeaders)
            .map(function (data) { return _this.model.factory(data); });
    };
    /**
     * @method find
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @return {Observable<T[+>}
     * @description
     * Generic find method
     */
    BaseLoopBackApi.prototype.find = function (filter, customHeaders) {
        var _this = this;
        if (filter === void 0) { filter = {}; }
        return this.request('GET', [
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getPath(),
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getApiVersion(),
            this.model.getModelDefinition().path
        ].join('/'), undefined, { filter: filter }, undefined, null, customHeaders)
            .map(function (datum) { return datum.map(function (data) { return _this.model.factory(data); }); });
    };
    /**
     * @method exists
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @return {Observable<T[]>}
     * @description
     * Generic exists method
     */
    BaseLoopBackApi.prototype.exists = function (id, customHeaders) {
        return this.request('GET', [
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getPath(),
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getApiVersion(),
            this.model.getModelDefinition().path,
            ':id/exists'
        ].join('/'), { id: id }, undefined, undefined, null, customHeaders);
    };
    /**
     * @method findOne
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @return {Observable<T>}
     * @description
     * Generic findOne method
     */
    BaseLoopBackApi.prototype.findOne = function (filter, customHeaders) {
        var _this = this;
        if (filter === void 0) { filter = {}; }
        return this.request('GET', [
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getPath(),
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getApiVersion(),
            this.model.getModelDefinition().path,
            'findOne'
        ].join('/'), undefined, { filter: filter }, undefined, null, customHeaders)
            .map(function (data) { return _this.model.factory(data); });
    };
    /**
     * @method updateAll
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @return {Observable<T[]>}
     * @description
     * Generic updateAll method
     */
    BaseLoopBackApi.prototype.updateAll = function (where, data, customHeaders) {
        if (where === void 0) { where = {}; }
        var _urlParams = {};
        if (where)
            _urlParams.where = where;
        return this.request('POST', [
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getPath(),
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getApiVersion(),
            this.model.getModelDefinition().path,
            'update'
        ].join('/'), undefined, _urlParams, { data: data }, null, customHeaders);
    };
    /**
     * @method onUpdateAll
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @return {Observable<T[]>}
     * @description
     * Generic pubsub onUpdateAll method
     */
    BaseLoopBackApi.prototype.onUpdateAll = function (where, data) {
        if (where === void 0) { where = {}; }
        var _urlParams = {};
        if (where)
            _urlParams.where = where;
        return this.request('POST', [
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getPath(),
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getApiVersion(),
            this.model.getModelDefinition().path,
            'update'
        ].join('/'), undefined, _urlParams, { data: data }, true);
    };
    /**
     * @method deleteById
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @return {Observable<T>}
     * @description
     * Generic deleteById method
     */
    BaseLoopBackApi.prototype.deleteById = function (id, customHeaders) {
        var _this = this;
        return this.request('DELETE', [
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getPath(),
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getApiVersion(),
            this.model.getModelDefinition().path,
            ':id'
        ].join('/'), { id: id }, undefined, undefined, null, customHeaders)
            .map(function (data) { return _this.model.factory(data); });
    };
    /**
     * @method onDeleteById
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @return {Observable<T>}
     * @description
     * Generic pubsub onDeleteById method
     */
    BaseLoopBackApi.prototype.onDeleteById = function (id) {
        var _this = this;
        return this.request('DELETE', [
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getPath(),
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getApiVersion(),
            this.model.getModelDefinition().path,
            ':id'
        ].join('/'), { id: id }, undefined, undefined, true).map(function (data) { return _this.model.factory(data); });
    };
    /**
     * @method count
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @return {Observable<{ count: number }>}
     * @description
     * Generic count method
     */
    BaseLoopBackApi.prototype.count = function (where, customHeaders) {
        if (where === void 0) { where = {}; }
        var _urlParams = {};
        if (where)
            _urlParams.where = where;
        return this.request('GET', [
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getPath(),
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getApiVersion(),
            this.model.getModelDefinition().path,
            'count'
        ].join('/'), undefined, _urlParams, undefined, null, customHeaders);
    };
    /**
     * @method updateAttributes
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @return {Observable<T>}
     * @description
     * Generic updateAttributes method
     */
    BaseLoopBackApi.prototype.updateAttributes = function (id, data, customHeaders) {
        var _this = this;
        return this.request('PUT', [
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getPath(),
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getApiVersion(),
            this.model.getModelDefinition().path,
            ':id'
        ].join('/'), { id: id }, undefined, { data: data }, null, customHeaders)
            .map(function (data) { return _this.model.factory(data); });
    };
    /**
     * @method onUpdateAttributes
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @return {Observable<T>}
     * @description
     * Generic onUpdateAttributes method
     */
    BaseLoopBackApi.prototype.onUpdateAttributes = function (id, data) {
        var _this = this;
        return this.request('PUT', [
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getPath(),
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getApiVersion(),
            this.model.getModelDefinition().path,
            ':id'
        ].join('/'), { id: id }, undefined, { data: data }, true).map(function (data) { return _this.model.factory(data); });
    };
    /**
     * @method upsert
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @return {Observable<T>}
     * @description
     * Generic upsert method
     */
    BaseLoopBackApi.prototype.upsert = function (data, customHeaders) {
        var _this = this;
        if (data === void 0) { data = {}; }
        return this.request('PUT', [
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getPath(),
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getApiVersion(),
            this.model.getModelDefinition().path,
        ].join('/'), undefined, undefined, { data: data }, null, customHeaders)
            .map(function (data) { return _this.model.factory(data); });
    };
    /**
     * @method onUpsert
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @return {Observable<T>}
     * @description
     * Generic pubsub onUpsert method
     */
    BaseLoopBackApi.prototype.onUpsert = function (data) {
        var _this = this;
        if (data === void 0) { data = {}; }
        return this.request('PUT', [
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getPath(),
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getApiVersion(),
            this.model.getModelDefinition().path,
        ].join('/'), undefined, undefined, { data: data }, true).map(function (data) { return _this.model.factory(data); });
    };
    /**
     * @method upsertPatch
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @return {Observable<T>}
     * @description
     * Generic upsert method using patch http method
     */
    BaseLoopBackApi.prototype.upsertPatch = function (data, customHeaders) {
        var _this = this;
        if (data === void 0) { data = {}; }
        return this.request('PATCH', [
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getPath(),
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getApiVersion(),
            this.model.getModelDefinition().path,
        ].join('/'), undefined, undefined, { data: data }, null, customHeaders)
            .map(function (data) { return _this.model.factory(data); });
    };
    /**
     * @method onUpsertPatch
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @return {Observable<T>}
     * @description
     * Generic pubsub onUpsertPatch method using patch http method
     */
    BaseLoopBackApi.prototype.onUpsertPatch = function (data) {
        var _this = this;
        if (data === void 0) { data = {}; }
        return this.request('PATCH', [
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getPath(),
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getApiVersion(),
            this.model.getModelDefinition().path,
        ].join('/'), undefined, undefined, { data: data }, true).map(function (data) { return _this.model.factory(data); });
    };
    /**
     * @method upsertWithWhere
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @return {Observable<T>}
     * @description
     * Generic upsertWithWhere method
     */
    BaseLoopBackApi.prototype.upsertWithWhere = function (where, data, customHeaders) {
        var _this = this;
        if (where === void 0) { where = {}; }
        if (data === void 0) { data = {}; }
        var _urlParams = {};
        if (where)
            _urlParams.where = where;
        return this.request('POST', [
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getPath(),
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getApiVersion(),
            this.model.getModelDefinition().path,
            'upsertWithWhere'
        ].join('/'), undefined, _urlParams, { data: data }, null, customHeaders)
            .map(function (data) { return _this.model.factory(data); });
    };
    /**
     * @method onUpsertWithWhere
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @return {Observable<T>}
     * @description
     * Generic pubsub onUpsertWithWhere method
     */
    BaseLoopBackApi.prototype.onUpsertWithWhere = function (where, data) {
        var _this = this;
        if (where === void 0) { where = {}; }
        if (data === void 0) { data = {}; }
        var _urlParams = {};
        if (where)
            _urlParams.where = where;
        return this.request('POST', [
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getPath(),
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getApiVersion(),
            this.model.getModelDefinition().path,
            'upsertWithWhere'
        ].join('/'), undefined, _urlParams, { data: data }, true).map(function (data) { return _this.model.factory(data); });
    };
    /**
     * @method replaceOrCreate
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @return {Observable<T>}
     * @description
     * Generic replaceOrCreate method
     */
    BaseLoopBackApi.prototype.replaceOrCreate = function (data, customHeaders) {
        var _this = this;
        if (data === void 0) { data = {}; }
        return this.request('POST', [
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getPath(),
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getApiVersion(),
            this.model.getModelDefinition().path,
            'replaceOrCreate'
        ].join('/'), undefined, undefined, { data: data }, null, customHeaders)
            .map(function (data) { return _this.model.factory(data); });
    };
    /**
     * @method onReplaceOrCreate
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @return {Observable<T>}
     * @description
     * Generic onReplaceOrCreate method
     */
    BaseLoopBackApi.prototype.onReplaceOrCreate = function (data) {
        var _this = this;
        if (data === void 0) { data = {}; }
        return this.request('POST', [
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getPath(),
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getApiVersion(),
            this.model.getModelDefinition().path,
            'replaceOrCreate'
        ].join('/'), undefined, undefined, { data: data }, true).map(function (data) { return _this.model.factory(data); });
    };
    /**
     * @method replaceById
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @return {Observable<T>}
     * @description
     * Generic replaceById method
     */
    BaseLoopBackApi.prototype.replaceById = function (id, data, customHeaders) {
        var _this = this;
        if (data === void 0) { data = {}; }
        return this.request('POST', [
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getPath(),
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getApiVersion(),
            this.model.getModelDefinition().path,
            ':id', 'replace'
        ].join('/'), { id: id }, undefined, { data: data }, null, customHeaders)
            .map(function (data) { return _this.model.factory(data); });
    };
    /**
     * @method onReplaceById
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @return {Observable<T>}
     * @description
     * Generic onReplaceById method
     */
    BaseLoopBackApi.prototype.onReplaceById = function (id, data) {
        var _this = this;
        if (data === void 0) { data = {}; }
        return this.request('POST', [
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getPath(),
            __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getApiVersion(),
            this.model.getModelDefinition().path,
            ':id', 'replace'
        ].join('/'), { id: id }, undefined, { data: data }, true).map(function (data) { return _this.model.factory(data); });
    };
    /**
     * @method createChangeStream
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @return {Observable<any>}
     * @description
     * Generic createChangeStream method
     */
    BaseLoopBackApi.prototype.createChangeStream = function () {
        var subject = new __WEBPACK_IMPORTED_MODULE_7_rxjs_Subject__["Subject"]();
        if (typeof EventSource !== 'undefined') {
            var emit = function (msg) { return subject.next(JSON.parse(msg.data)); };
            var source = new EventSource([
                __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getPath(),
                __WEBPACK_IMPORTED_MODULE_5__lb_config__["a" /* LoopBackConfig */].getApiVersion(),
                this.model.getModelDefinition().path,
                'change-stream'
            ].join('/'));
            source.addEventListener('data', emit);
            source.onerror = emit;
        }
        else {
            console.warn('SDK Builder: EventSource is not supported');
        }
        return subject.asObservable();
    };
    return BaseLoopBackApi;
}());
BaseLoopBackApi = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __param(0, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */])),
    __param(1, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_10__sockets_socket_connections__["a" /* SocketConnection */])),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_6__custom_SDKModels__["a" /* SDKModels */])),
    __param(3, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_4__auth_service__["a" /* LoopBackAuth */])),
    __param(4, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_2__search_params__["a" /* JSONSearchParams */])),
    __param(5, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Optional */])()), __param(5, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_3__error_service__["a" /* ErrorHandler */])),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_10__sockets_socket_connections__["a" /* SocketConnection */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_10__sockets_socket_connections__["a" /* SocketConnection */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_6__custom_SDKModels__["a" /* SDKModels */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__custom_SDKModels__["a" /* SDKModels */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__auth_service__["a" /* LoopBackAuth */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__auth_service__["a" /* LoopBackAuth */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2__search_params__["a" /* JSONSearchParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__search_params__["a" /* JSONSearchParams */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_3__error_service__["a" /* ErrorHandler */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__error_service__["a" /* ErrorHandler */]) === "function" && _f || Object])
], BaseLoopBackApi);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=base.service.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/services/core/error.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ErrorHandler; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__ = __webpack_require__("../../../../rxjs/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_observable_throw__ = __webpack_require__("../../../../rxjs/add/observable/throw.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_observable_throw___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_observable_throw__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
/* tslint:disable */


//import { ErrorObservable } from 'rxjs/observable/ErrorObservable';

/**
 * Default error handler
 */
var ErrorHandler = (function () {
    function ErrorHandler() {
    }
    // ErrorObservable when rxjs version < rc.5
    // ErrorObservable<string> when rxjs version = rc.5
    // I'm leaving any for now to avoid breaking apps using both versions
    ErrorHandler.prototype.handleError = function (error) {
        return __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error');
    };
    return ErrorHandler;
}());
ErrorHandler = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])()
], ErrorHandler);

//# sourceMappingURL=error.service.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/services/core/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__auth_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/auth.service.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__auth_service__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__error_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/error.service.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__search_params__ = __webpack_require__("../../../../../src/app/sdk/services/core/search.params.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__base_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/base.service.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__real_time__ = __webpack_require__("../../../../../src/app/sdk/services/core/real.time.ts");
/* unused harmony namespace reexport */
/* tslint:disable */





//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/services/core/io.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IO; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_Subject__ = __webpack_require__("../../../../rxjs/Subject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_rxjs_Subject__);
/* tslint:disable */

var IO = (function () {
    function IO(socket) {
        this.observables = {};
        this.socket = socket;
    }
    IO.prototype.emit = function (event, data) {
        this.socket.emit('ME:RT:1://event', {
            event: event,
            data: data
        });
    };
    IO.prototype.on = function (event) {
        if (this.observables[event]) {
            return this.observables[event];
        }
        var subject = new __WEBPACK_IMPORTED_MODULE_0_rxjs_Subject__["Subject"]();
        this.socket.on(event, function (res) { return subject.next(res); });
        this.observables[event] = subject.asObservable();
        return this.observables[event];
    };
    return IO;
}());

//# sourceMappingURL=io.service.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/services/core/real.time.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RealTime; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_share__ = __webpack_require__("../../../../rxjs/add/operator/share.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_share___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_share__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__io_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/io.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__models_FireLoop__ = __webpack_require__("../../../../../src/app/sdk/models/FireLoop.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__sockets_socket_connections__ = __webpack_require__("../../../../../src/app/sdk/sockets/socket.connections.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__custom_SDKModels__ = __webpack_require__("../../../../../src/app/sdk/services/custom/SDKModels.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_Subject__ = __webpack_require__("../../../../rxjs/Subject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_Subject__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
/* tslint:disable */









/**
* @author Jonathan Casarrubias <twitter:@johncasarrubias> <github:@johncasarrubias>
* @module RealTime
* @license MIT
* @description
* This module is a real-time interface for using socket connections, its main purpose
* is to make sure that when there is a valid connection, it will create instances
* of the different real-time functionalities like FireLoop, PubSub and IO.
**/
var RealTime = (function () {
    /**
    * @method constructor
    * @param {SocketConnection} connection WebSocket connection service
    * @param {SDKModels} models Model provider service
    * @param {LoopBackAuth} auth LoopBack authentication service
    * @description
    * It will intialize the shared on ready communication channel.
    **/
    function RealTime(connection, models, auth) {
        this.connection = connection;
        this.models = models;
        this.auth = auth;
        this.connecting = false;
        this.onReadySubject = new __WEBPACK_IMPORTED_MODULE_7_rxjs_Subject__["Subject"]();
        this.sharedOnReady = this.onReadySubject.asObservable().share();
        this.sharedOnReady.subscribe();
    }
    /**
    * @method onDisconnect
    * @return {Observable<any>}
    * @description
    * Will trigger when Real-Time Service is disconnected from server.
    **/
    RealTime.prototype.onDisconnect = function () {
        return this.connection.sharedObservables.sharedOnDisconnect;
    };
    /**
    * @method onAuthenticated
    * @return {Observable<any>}
    * @description
    * Will trigger when Real-Time Service is authenticated with the server.
    **/
    RealTime.prototype.onAuthenticated = function () {
        return this.connection.sharedObservables.sharedOnAuthenticated;
    };
    /**
    * @method onUnAuthorized
    * @return {Observable<any>}
    * @description
    * Will trigger when Real-Time Service is not authorized to connect with the server.
    **/
    RealTime.prototype.onUnAuthorized = function () {
        return this.connection.sharedObservables.sharedOnUnAuthorized;
    };
    /**
    * @method onReady
    * @return {Observable<any>}
    * @description
    * Will trigger when Real-Time Service is Ready for broadcasting.
    * and will register connection flow events to notify subscribers.
    **/
    RealTime.prototype.onReady = function () {
        var _this = this;
        // If there is a valid connection, then we just send back to the EventLoop
        // Or next will be executed before the actual subscription.
        if (this.connection.isConnected()) {
            var to_1 = setTimeout(function () {
                _this.onReadySubject.next('shared-connection');
                clearTimeout(to_1);
            });
            // Else if there is a current attempt of connection we wait for the prior
            // process that started the connection flow.
        }
        else if (this.connecting) {
            var ti_1 = setInterval(function () {
                if (_this.connection.isConnected()) {
                    _this.onReadySubject.next('shared-connection');
                    clearInterval(ti_1);
                }
            }, 500);
            // If there is not valid connection or attempt, then we start the connection flow
            // and make sure we notify all the onReady subscribers when done.
            // Also it will listen for desconnections so we unsubscribe and avoid both:
            // Memory leaks and duplicated triggered events.
        }
        else {
            this.connecting = true;
            this.connection.connect(this.auth.getToken());
            this.IO = new __WEBPACK_IMPORTED_MODULE_2__io_service__["a" /* IO */](this.connection);
            this.FireLoop = new __WEBPACK_IMPORTED_MODULE_4__models_FireLoop__["a" /* FireLoop */](this.connection, this.models);
            // Fire event for those subscribed 
            var s1_1 = this.connection.sharedObservables.sharedOnConnect.subscribe(function () {
                console.log('Real-Time connection has been established');
                _this.connecting = false;
                _this.onReadySubject.next('connected');
                var s2 = _this.connection.sharedObservables.sharedOnDisconnect.subscribe(function () {
                    s1_1.unsubscribe();
                    s2.unsubscribe();
                });
            });
        }
        return this.sharedOnReady;
    };
    return RealTime;
}());
RealTime = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["C" /* Injectable */])(),
    __param(0, Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_5__sockets_socket_connections__["a" /* SocketConnection */])),
    __param(1, Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_6__custom_SDKModels__["a" /* SDKModels */])),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_3__auth_service__["a" /* LoopBackAuth */])),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_5__sockets_socket_connections__["a" /* SocketConnection */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__sockets_socket_connections__["a" /* SocketConnection */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_6__custom_SDKModels__["a" /* SDKModels */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__custom_SDKModels__["a" /* SDKModels */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__auth_service__["a" /* LoopBackAuth */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__auth_service__["a" /* LoopBackAuth */]) === "function" && _c || Object])
], RealTime);

var _a, _b, _c;
//# sourceMappingURL=real.time.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/services/core/search.params.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JSONSearchParams; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
/* tslint:disable */


/**
* @author Jonathan Casarrubias <twitter:@johncasarrubias> <github:@mean-expert-official>
* @module JSONSearchParams
* @license MIT
* @description
* JSON Parser and Wrapper for the Angular2 URLSearchParams
* This module correctly encodes a json object into a query string and then creates
* an instance of the URLSearchParams component for later use in HTTP Calls
**/
var JSONSearchParams = (function () {
    function JSONSearchParams() {
    }
    JSONSearchParams.prototype.setJSON = function (obj) {
        this._usp = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["f" /* URLSearchParams */](this._JSON2URL(obj, false));
    };
    JSONSearchParams.prototype.getURLSearchParams = function () {
        return this._usp;
    };
    JSONSearchParams.prototype._JSON2URL = function (obj, parent) {
        var parts = [];
        for (var key in obj)
            parts.push(this._parseParam(key, obj[key], parent));
        return parts.join('&');
    };
    JSONSearchParams.prototype._parseParam = function (key, value, parent) {
        var processedKey = parent ? parent + '[' + key + ']' : key;
        if (value && ((typeof value) === 'object' || Array.isArray(value))) {
            return this._JSON2URL(value, processedKey);
        }
        return processedKey + '=' + value;
    };
    return JSONSearchParams;
}());
JSONSearchParams = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])()
], JSONSearchParams);

//# sourceMappingURL=search.params.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/services/custom/Container.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContainerApi; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__SDKModels__ = __webpack_require__("../../../../../src/app/sdk/services/custom/SDKModels.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_base_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/base.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__lb_config__ = __webpack_require__("../../../../../src/app/sdk/lb.config.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_auth_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_search_params__ = __webpack_require__("../../../../../src/app/sdk/services/core/search.params.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_error_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__ = __webpack_require__("../../../../../src/app/sdk/sockets/socket.connections.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
/* tslint:disable */









/**
 * Api services for the `Container` model.
 */
var ContainerApi = (function (_super) {
    __extends(ContainerApi, _super);
    function ContainerApi(http, connection, models, auth, searchParams, errorHandler) {
        var _this = _super.call(this, http, connection, models, auth, searchParams, errorHandler) || this;
        _this.http = http;
        _this.connection = connection;
        _this.models = models;
        _this.auth = auth;
        _this.searchParams = searchParams;
        _this.errorHandler = errorHandler;
        return _this;
    }
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @returns {object[]} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `Container` object.)
     * </em>
     */
    ContainerApi.prototype.getContainers = function (customHeaders) {
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Containers";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} data Request data.
     *
     * This method expects a subset of model properties as request parameters.
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `Container` object.)
     * </em>
     */
    ContainerApi.prototype.createContainer = function (options, customHeaders) {
        if (options === void 0) { options = {}; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Containers";
        var _routeParams = {};
        var _postBody = {
            options: options
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {string} container
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `` – `{}` -
     */
    ContainerApi.prototype.destroyContainer = function (container, customHeaders) {
        var _method = "DELETE";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Containers/:container";
        var _routeParams = {
            container: container
        };
        var _postBody = {};
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {string} container
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `Container` object.)
     * </em>
     */
    ContainerApi.prototype.getContainer = function (container, customHeaders) {
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Containers/:container";
        var _routeParams = {
            container: container
        };
        var _postBody = {};
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {string} container
     *
     * @returns {object[]} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `Container` object.)
     * </em>
     */
    ContainerApi.prototype.getFiles = function (container, customHeaders) {
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Containers/:container/files";
        var _routeParams = {
            container: container
        };
        var _postBody = {};
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {string} container
     *
     * @param {string} file
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `Container` object.)
     * </em>
     */
    ContainerApi.prototype.getFile = function (container, file, customHeaders) {
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Containers/:container/files/:file";
        var _routeParams = {
            container: container,
            file: file
        };
        var _postBody = {};
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {string} container
     *
     * @param {string} file
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `` – `{}` -
     */
    ContainerApi.prototype.removeFile = function (container, file, customHeaders) {
        var _method = "DELETE";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Containers/:container/files/:file";
        var _routeParams = {
            container: container,
            file: file
        };
        var _postBody = {};
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {string} container
     *
     * @param {object} data Request data.
     *
     *  - `req` – `{object}` -
     *
     *  - `res` – `{object}` -
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `result` – `{object}` -
     */
    ContainerApi.prototype.upload = function (container, req, res, customHeaders) {
        if (req === void 0) { req = {}; }
        if (res === void 0) { res = {}; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Containers/:container/upload";
        var _routeParams = {
            container: container
        };
        var _postBody = {};
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {string} container
     *
     * @param {string} file
     *
     * @param {object} req
     *
     * @param {object} res
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * This method returns no data.
     */
    ContainerApi.prototype.download = function (container, file, req, res, customHeaders) {
        if (req === void 0) { req = {}; }
        if (res === void 0) { res = {}; }
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Containers/:container/download/:file";
        var _routeParams = {
            container: container,
            file: file
        };
        var _postBody = {};
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} data Request data.
     *
     *  - `req` – `{object}` -
     *
     *  - `res` – `{object}` -
     *
     *  - `containerName` – `{string}` -
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    ContainerApi.prototype.imageUpload = function (req, res, containerName, customHeaders) {
        if (req === void 0) { req = {}; }
        if (res === void 0) { res = {}; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Containers/imageUpload";
        var _routeParams = {
            containerName: containerName
        };
        var _postBody = {};
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * The name of the model represented by this $resource,
     * i.e. `Container`.
     */
    ContainerApi.prototype.getModelName = function () {
        return "Container";
    };
    return ContainerApi;
}(__WEBPACK_IMPORTED_MODULE_3__core_base_service__["a" /* BaseLoopBackApi */]));
ContainerApi = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __param(0, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */])),
    __param(1, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */])),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */])),
    __param(3, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */])),
    __param(4, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */])),
    __param(5, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Optional */])()), __param(5, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */])),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */]) === "function" && _f || Object])
], ContainerApi);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=Container.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/services/custom/Email.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmailApi; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__SDKModels__ = __webpack_require__("../../../../../src/app/sdk/services/custom/SDKModels.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_base_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/base.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_auth_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_search_params__ = __webpack_require__("../../../../../src/app/sdk/services/core/search.params.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_error_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__sockets_socket_connections__ = __webpack_require__("../../../../../src/app/sdk/sockets/socket.connections.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
/* tslint:disable */








/**
 * Api services for the `Email` model.
 */
var EmailApi = (function (_super) {
    __extends(EmailApi, _super);
    function EmailApi(http, connection, models, auth, searchParams, errorHandler) {
        var _this = _super.call(this, http, connection, models, auth, searchParams, errorHandler) || this;
        _this.http = http;
        _this.connection = connection;
        _this.models = models;
        _this.auth = auth;
        _this.searchParams = searchParams;
        _this.errorHandler = errorHandler;
        return _this;
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `Email`.
     */
    EmailApi.prototype.getModelName = function () {
        return "Email";
    };
    return EmailApi;
}(__WEBPACK_IMPORTED_MODULE_3__core_base_service__["a" /* BaseLoopBackApi */]));
EmailApi = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __param(0, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */])),
    __param(1, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_7__sockets_socket_connections__["a" /* SocketConnection */])),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */])),
    __param(3, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_4__core_auth_service__["a" /* LoopBackAuth */])),
    __param(4, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_5__core_search_params__["a" /* JSONSearchParams */])),
    __param(5, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Optional */])()), __param(5, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_6__core_error_service__["a" /* ErrorHandler */])),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_7__sockets_socket_connections__["a" /* SocketConnection */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__sockets_socket_connections__["a" /* SocketConnection */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__core_auth_service__["a" /* LoopBackAuth */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__core_auth_service__["a" /* LoopBackAuth */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_5__core_search_params__["a" /* JSONSearchParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__core_search_params__["a" /* JSONSearchParams */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_6__core_error_service__["a" /* ErrorHandler */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__core_error_service__["a" /* ErrorHandler */]) === "function" && _f || Object])
], EmailApi);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=Email.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/services/custom/Job.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JobApi; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__SDKModels__ = __webpack_require__("../../../../../src/app/sdk/services/custom/SDKModels.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_base_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/base.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__lb_config__ = __webpack_require__("../../../../../src/app/sdk/lb.config.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_auth_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_search_params__ = __webpack_require__("../../../../../src/app/sdk/services/core/search.params.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_error_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__ = __webpack_require__("../../../../../src/app/sdk/sockets/socket.connections.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
/* tslint:disable */









/**
 * Api services for the `Job` model.
 */
var JobApi = (function (_super) {
    __extends(JobApi, _super);
    function JobApi(http, connection, models, auth, searchParams, errorHandler) {
        var _this = _super.call(this, http, connection, models, auth, searchParams, errorHandler) || this;
        _this.http = http;
        _this.connection = connection;
        _this.models = models;
        _this.auth = auth;
        _this.searchParams = searchParams;
        _this.errorHandler = errorHandler;
        return _this;
    }
    /**
     * Patch attributes for a model instance and persist it into the data source.
     *
     * @param {any} id Job id
     *
     * @param {object} data Request data.
     *
     *  - `data` – `{object}` - An object of model property name/value pairs
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `Job` object.)
     * </em>
     */
    JobApi.prototype.patchAttributes = function (id, data, customHeaders) {
        if (data === void 0) { data = {}; }
        var _method = "PATCH";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Jobs/:id";
        var _routeParams = {
            id: id
        };
        var _postBody = {
            data: data
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} data Request data.
     *
     *  - `success` – `{string}` -
     *
     *  - `weight` – `{number}` -
     *
     *  - `source` – `{object}` -
     *
     *  - `insurance` – `{boolean}` -
     *
     *  - `valueOfLoad` – `{number}` -
     *
     *  - `insPremium` – `{number}` -
     *
     *  - `budget` – `{number}` -
     *
     *  - `netPrice` – `{number}` -
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    JobApi.prototype.approveByAdmin = function (jobId, jobApproveStatus, customHeaders) {
        if (jobId === void 0) { jobId = {}; }
        if (jobApproveStatus === void 0) { jobApproveStatus = {}; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Jobs/approveByAdmin";
        var _routeParams = {};
        var _postBody = {
            data: {
                jobId: jobId,
                jobApproveStatus: jobApproveStatus
            }
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    JobApi.prototype.getJobsByAdmin = function (jobApproveStatus, customHeaders) {
        if (jobApproveStatus === void 0) { jobApproveStatus = {}; }
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Jobs/getJobsByAdmin";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        if (typeof jobApproveStatus !== 'undefined' && jobApproveStatus !== null)
            _urlParams.jobApproveStatus = jobApproveStatus;
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    JobApi.prototype.createJob = function (req, title, weight, source, destination, valueOfGoods, budget, netPrice, typeOfVehicle, date, endDate, description, natureOfGoods, typeOfJob, selectedRoute, distance, duration, vehicleTonnage, vehicleBodyType, requiredType, customHeaders) {
        if (req === void 0) { req = {}; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Jobs/createJob";
        var _routeParams = {};
        var _postBody = {
            data: {
                title: title,
                weight: weight,
                source: source,
                destination: destination,
                valueOfGoods: valueOfGoods,
                budget: budget,
                netPrice: netPrice,
                typeOfVehicle: typeOfVehicle,
                date: date,
                endDate: date,
                description: description,
                natureOfGoods: natureOfGoods,
                typeOfJob: typeOfJob,
                selectedRoute: selectedRoute,
                distance: distance,
                duration: duration,
                vehicleTonnage: vehicleTonnage,
                vehicleBodyType: vehicleBodyType,
                requiredType: requiredType
            }
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    JobApi.prototype.getJobPricing = function (vehicleName, tonnage, distanceInMeter, customHeaders) {
        if (tonnage === void 0) { tonnage = {}; }
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Jobs/getJobPricing";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        if (typeof vehicleName !== 'undefined' && vehicleName !== null)
            _urlParams.vehicleName = vehicleName;
        if (typeof tonnage !== 'undefined' && tonnage !== null)
            _urlParams.tonnage = tonnage;
        if (typeof distanceInMeter !== 'undefined' && distanceInMeter !== null)
            _urlParams.distanceInMeter = distanceInMeter;
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    JobApi.prototype.getJobList = function (realm, customHeaders) {
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Jobs/getJobs";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        if (typeof realm !== 'undefined' && realm !== null)
            _urlParams.realm = realm;
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    JobApi.prototype.applyForJob = function (req, jobId, customHeaders) {
        if (req === void 0) { req = {}; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/JobApplies/applyForJob";
        var _routeParams = {};
        var _postBody = {
            data: {
                jobId: jobId
            }
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    JobApi.prototype.LoginWithFB = function (access_token, realm, firebaseToken, customHeaders) {
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/auth/facebook/token";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        if (typeof access_token !== 'undefined' && access_token !== null)
            _urlParams.access_token = access_token;
        if (typeof realm !== 'undefined' && realm !== null)
            _urlParams.realm = realm;
        if (typeof firebaseToken !== 'undefined' && firebaseToken !== null)
            _urlParams.firebaseToken = firebaseToken;
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    JobApi.prototype.getNearJobs = function (location, customHeaders) {
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Jobs/getNearJobs";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        if (typeof location !== 'undefined' && location !== null)
            _urlParams.location = location;
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    JobApi.prototype.getJobById = function (jobId, customHeaders) {
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Jobs/getJobById";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        if (typeof jobId !== 'undefined' && jobId !== null)
            _urlParams.jobId = jobId;
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * The name of the model represented by this $resource,
     * i.e. `Job`.
     */
    JobApi.prototype.getModelName = function () {
        return "Job";
    };
    return JobApi;
}(__WEBPACK_IMPORTED_MODULE_3__core_base_service__["a" /* BaseLoopBackApi */]));
JobApi = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __param(0, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */])),
    __param(1, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */])),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */])),
    __param(3, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */])),
    __param(4, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */])),
    __param(5, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Optional */])()), __param(5, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */])),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */]) === "function" && _f || Object])
], JobApi);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=Job.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/services/custom/JobApply.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JobApplyApi; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__SDKModels__ = __webpack_require__("../../../../../src/app/sdk/services/custom/SDKModels.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_base_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/base.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__lb_config__ = __webpack_require__("../../../../../src/app/sdk/lb.config.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_auth_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_search_params__ = __webpack_require__("../../../../../src/app/sdk/services/core/search.params.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_error_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__ = __webpack_require__("../../../../../src/app/sdk/sockets/socket.connections.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
/* tslint:disable */









/**
 * Api services for the `JobApply` model.
 */
var JobApplyApi = (function (_super) {
    __extends(JobApplyApi, _super);
    function JobApplyApi(http, connection, models, auth, searchParams, errorHandler) {
        var _this = _super.call(this, http, connection, models, auth, searchParams, errorHandler) || this;
        _this.http = http;
        _this.connection = connection;
        _this.models = models;
        _this.auth = auth;
        _this.searchParams = searchParams;
        _this.errorHandler = errorHandler;
        return _this;
    }
    /**
     * Patch attributes for a model instance and persist it into the data source.
     *
     * @param {any} id JobApply id
     *
     * @param {object} data Request data.
     *
     *  - `data` – `{object}` - An object of model property name/value pairs
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `JobApply` object.)
     * </em>
     */
    JobApplyApi.prototype.patchAttributes = function (id, data, customHeaders) {
        if (data === void 0) { data = {}; }
        var _method = "PATCH";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/JobApplies/:id";
        var _routeParams = {
            id: id
        };
        var _postBody = {
            data: data
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} data Request data.
     *
     *  - `req` – `{object}` -
     *
     *  - `jobId` – `{string}` -
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    JobApplyApi.prototype.applyForJob = function (req, jobId, customHeaders) {
        if (req === void 0) { req = {}; }
        if (jobId === void 0) { jobId = {}; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/JobApplies/applyForJob";
        var _routeParams = {};
        var _postBody = {
            data: {
                jobId: jobId
            }
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * The name of the model represented by this $resource,
     * i.e. `JobApply`.
     */
    JobApplyApi.prototype.getModelName = function () {
        return "JobApply";
    };
    return JobApplyApi;
}(__WEBPACK_IMPORTED_MODULE_3__core_base_service__["a" /* BaseLoopBackApi */]));
JobApplyApi = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __param(0, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */])),
    __param(1, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */])),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */])),
    __param(3, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */])),
    __param(4, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */])),
    __param(5, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Optional */])()), __param(5, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */])),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */]) === "function" && _f || Object])
], JobApplyApi);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=JobApply.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/services/custom/Notification.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationApi; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__SDKModels__ = __webpack_require__("../../../../../src/app/sdk/services/custom/SDKModels.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_base_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/base.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__lb_config__ = __webpack_require__("../../../../../src/app/sdk/lb.config.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_auth_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_search_params__ = __webpack_require__("../../../../../src/app/sdk/services/core/search.params.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_error_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__ = __webpack_require__("../../../../../src/app/sdk/sockets/socket.connections.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
/* tslint:disable */









/**
 * Api services for the `Notification` model.
 */
var NotificationApi = (function (_super) {
    __extends(NotificationApi, _super);
    function NotificationApi(http, connection, models, auth, searchParams, errorHandler) {
        var _this = _super.call(this, http, connection, models, auth, searchParams, errorHandler) || this;
        _this.http = http;
        _this.connection = connection;
        _this.models = models;
        _this.auth = auth;
        _this.searchParams = searchParams;
        _this.errorHandler = errorHandler;
        return _this;
    }
    /**
     * Patch attributes for a model instance and persist it into the data source.
     *
     * @param {any} id Notification id
     *
     * @param {object} data Request data.
     *
     *  - `data` – `{object}` - An object of model property name/value pairs
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `Notification` object.)
     * </em>
     */
    NotificationApi.prototype.patchAttributes = function (id, data, customHeaders) {
        if (data === void 0) { data = {}; }
        var _method = "PATCH";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Notifications/:id";
        var _routeParams = {
            id: id
        };
        var _postBody = {
            data: data
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} filter
     *
     * @param {object} req
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    NotificationApi.prototype.getNotifications = function (filter, req, customHeaders) {
        if (filter === void 0) { filter = {}; }
        if (req === void 0) { req = {}; }
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Notifications/getNotifications";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} data Request data.
     *
     * This method expects a subset of model properties as request parameters.
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    NotificationApi.prototype.getAccessToken = function (userIds, customHeaders) {
        if (userIds === void 0) { userIds = {}; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Notifications/getAccessToken";
        var _routeParams = {};
        var _postBody = {
            userIds: userIds
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} data Request data.
     *
     *  - `jobInst` – `{object}` -
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    NotificationApi.prototype.createJobNoty = function (jobInst, customHeaders) {
        if (jobInst === void 0) { jobInst = {}; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Notifications/createJobNoty";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        if (typeof jobInst !== 'undefined' && jobInst !== null)
            _urlParams.jobInst = jobInst;
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * The name of the model represented by this $resource,
     * i.e. `Notification`.
     */
    NotificationApi.prototype.getModelName = function () {
        return "Notification";
    };
    return NotificationApi;
}(__WEBPACK_IMPORTED_MODULE_3__core_base_service__["a" /* BaseLoopBackApi */]));
NotificationApi = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __param(0, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */])),
    __param(1, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */])),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */])),
    __param(3, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */])),
    __param(4, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */])),
    __param(5, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Optional */])()), __param(5, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */])),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */]) === "function" && _f || Object])
], NotificationApi);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=Notification.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/services/custom/Otp.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OtpApi; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__SDKModels__ = __webpack_require__("../../../../../src/app/sdk/services/custom/SDKModels.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_base_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/base.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__lb_config__ = __webpack_require__("../../../../../src/app/sdk/lb.config.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_auth_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_search_params__ = __webpack_require__("../../../../../src/app/sdk/services/core/search.params.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_error_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__ = __webpack_require__("../../../../../src/app/sdk/sockets/socket.connections.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
/* tslint:disable */









/**
 * Api services for the `Otp` model.
 */
var OtpApi = (function (_super) {
    __extends(OtpApi, _super);
    function OtpApi(http, connection, models, auth, searchParams, errorHandler) {
        var _this = _super.call(this, http, connection, models, auth, searchParams, errorHandler) || this;
        _this.http = http;
        _this.connection = connection;
        _this.models = models;
        _this.auth = auth;
        _this.searchParams = searchParams;
        _this.errorHandler = errorHandler;
        return _this;
    }
    /**
     * Patch attributes for a model instance and persist it into the data source.
     *
     * @param {any} id Otp id
     *
     * @param {object} data Request data.
     *
     *  - `data` – `{object}` - An object of model property name/value pairs
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `Otp` object.)
     * </em>
     */
    OtpApi.prototype.patchAttributes = function (id, data, customHeaders) {
        if (data === void 0) { data = {}; }
        var _method = "PATCH";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Otps/:id";
        var _routeParams = {
            id: id
        };
        var _postBody = {
            data: data
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} data Request data.
     *
     * This method expects a subset of model properties as request parameters.
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    OtpApi.prototype.sendSMS = function (smsData, customHeaders) {
        if (smsData === void 0) { smsData = {}; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Otps/sendSMS";
        var _routeParams = {};
        var _postBody = {
            smsData: smsData
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} data Request data.
     *
     *  - `peopleId` – `{string}` -
     *
     *  - `type` – `{string}` -
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    OtpApi.prototype.resendOtp = function (peopleId, type, customHeaders) {
        if (type === void 0) { type = {}; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Otps/resendOtp";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        if (typeof peopleId !== 'undefined' && peopleId !== null)
            _urlParams.peopleId = peopleId;
        if (typeof type !== 'undefined' && type !== null)
            _urlParams.type = type;
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {string} peopleId
     *
     * @param {number} otp
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    OtpApi.prototype.checkResetOtp = function (peopleId, otp, customHeaders) {
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Otps/checkResetOtp";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        if (typeof peopleId !== 'undefined' && peopleId !== null)
            _urlParams.peopleId = peopleId;
        if (typeof otp !== 'undefined' && otp !== null)
            _urlParams.otp = otp;
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} data Request data.
     *
     *  - `peopleId` – `{string}` -
     *
     *  - `otp` – `{number}` -
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    OtpApi.prototype.verifyMobile = function (peopleId, otp, customHeaders) {
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Otps/verifyMobile";
        var _routeParams = {};
        var _postBody = {
            data: {
                peopleId: peopleId,
                otp: otp
            }
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} data Request data.
     *
     * This method expects a subset of model properties as request parameters.
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    OtpApi.prototype.checkOTP = function (obj, customHeaders) {
        if (obj === void 0) { obj = {}; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Otps/checkOTP";
        var _routeParams = {};
        var _postBody = {
            obj: obj
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * The name of the model represented by this $resource,
     * i.e. `Otp`.
     */
    OtpApi.prototype.getModelName = function () {
        return "Otp";
    };
    return OtpApi;
}(__WEBPACK_IMPORTED_MODULE_3__core_base_service__["a" /* BaseLoopBackApi */]));
OtpApi = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __param(0, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */])),
    __param(1, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */])),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */])),
    __param(3, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */])),
    __param(4, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */])),
    __param(5, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Optional */])()), __param(5, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */])),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */]) === "function" && _f || Object])
], OtpApi);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=Otp.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/services/custom/People.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PeopleApi; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__SDKModels__ = __webpack_require__("../../../../../src/app/sdk/services/custom/SDKModels.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_base_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/base.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__lb_config__ = __webpack_require__("../../../../../src/app/sdk/lb.config.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_auth_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_search_params__ = __webpack_require__("../../../../../src/app/sdk/services/core/search.params.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_error_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__ = __webpack_require__("../../../../../src/app/sdk/sockets/socket.connections.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
/* tslint:disable */









/**
 * Api services for the `People` model.
 */
var PeopleApi = (function (_super) {
    __extends(PeopleApi, _super);
    function PeopleApi(http, connection, models, auth, searchParams, errorHandler) {
        var _this = _super.call(this, http, connection, models, auth, searchParams, errorHandler) || this;
        _this.http = http;
        _this.connection = connection;
        _this.models = models;
        _this.auth = auth;
        _this.searchParams = searchParams;
        _this.errorHandler = errorHandler;
        return _this;
    }
    /**
     * Find a related item by id for identities.
     *
     * @param {any} id People id
     *
     * @param {any} fk Foreign key for identities
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `People` object.)
     * </em>
     */
    PeopleApi.prototype.getPeoplesByAdmin = function (realm, customHeaders) {
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/getPeoplesByAdmin";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        if (typeof realm !== 'undefined' && realm !== null)
            _urlParams.realm = realm;
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    PeopleApi.prototype.findByIdIdentities = function (id, fk, customHeaders) {
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/:id/identities/:fk";
        var _routeParams = {
            id: id,
            fk: fk
        };
        var _postBody = {};
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * Delete a related item by id for identities.
     *
     * @param {any} id People id
     *
     * @param {any} fk Foreign key for identities
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * This method returns no data.
     */
    PeopleApi.prototype.destroyByIdIdentities = function (id, fk, customHeaders) {
        var _method = "DELETE";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/:id/identities/:fk";
        var _routeParams = {
            id: id,
            fk: fk
        };
        var _postBody = {};
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * Update a related item by id for identities.
     *
     * @param {any} id People id
     *
     * @param {any} fk Foreign key for identities
     *
     * @param {object} data Request data.
     *
     * This method expects a subset of model properties as request parameters.
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `People` object.)
     * </em>
     */
    PeopleApi.prototype.updateByIdIdentities = function (id, fk, data, customHeaders) {
        if (data === void 0) { data = {}; }
        var _method = "PUT";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/:id/identities/:fk";
        var _routeParams = {
            id: id,
            fk: fk
        };
        var _postBody = {
            data: data
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * Find a related item by id for credentials.
     *
     * @param {any} id People id
     *
     * @param {any} fk Foreign key for credentials
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `People` object.)
     * </em>
     */
    PeopleApi.prototype.findByIdCredentials = function (id, fk, customHeaders) {
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/:id/credentials/:fk";
        var _routeParams = {
            id: id,
            fk: fk
        };
        var _postBody = {};
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * Delete a related item by id for credentials.
     *
     * @param {any} id People id
     *
     * @param {any} fk Foreign key for credentials
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * This method returns no data.
     */
    PeopleApi.prototype.destroyByIdCredentials = function (id, fk, customHeaders) {
        var _method = "DELETE";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/:id/credentials/:fk";
        var _routeParams = {
            id: id,
            fk: fk
        };
        var _postBody = {};
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * Update a related item by id for credentials.
     *
     * @param {any} id People id
     *
     * @param {any} fk Foreign key for credentials
     *
     * @param {object} data Request data.
     *
     * This method expects a subset of model properties as request parameters.
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `People` object.)
     * </em>
     */
    PeopleApi.prototype.updateByIdCredentials = function (id, fk, data, customHeaders) {
        if (data === void 0) { data = {}; }
        var _method = "PUT";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/:id/credentials/:fk";
        var _routeParams = {
            id: id,
            fk: fk
        };
        var _postBody = {
            data: data
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * Queries identities of People.
     *
     * @param {any} id People id
     *
     * @param {object} filter
     *
     * @returns {object[]} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `People` object.)
     * </em>
     */
    PeopleApi.prototype.getIdentities = function (id, filter, customHeaders) {
        if (filter === void 0) { filter = {}; }
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/:id/identities";
        var _routeParams = {
            id: id
        };
        var _postBody = {};
        var _urlParams = {};
        if (typeof filter !== 'undefined' && filter !== null)
            _urlParams.filter = filter;
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * Creates a new instance in identities of this model.
     *
     * @param {any} id People id
     *
     * @param {object} data Request data.
     *
     * This method expects a subset of model properties as request parameters.
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `People` object.)
     * </em>
     */
    PeopleApi.prototype.createIdentities = function (id, data, customHeaders) {
        if (data === void 0) { data = {}; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/:id/identities";
        var _routeParams = {
            id: id
        };
        var _postBody = {
            data: data
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * Deletes all identities of this model.
     *
     * @param {any} id People id
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * This method returns no data.
     */
    PeopleApi.prototype.deleteIdentities = function (id, customHeaders) {
        var _method = "DELETE";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/:id/identities";
        var _routeParams = {
            id: id
        };
        var _postBody = {};
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * Counts identities of People.
     *
     * @param {any} id People id
     *
     * @param {object} where Criteria to match model instances
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `count` – `{number}` -
     */
    PeopleApi.prototype.countIdentities = function (id, where, customHeaders) {
        if (where === void 0) { where = {}; }
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/:id/identities/count";
        var _routeParams = {
            id: id
        };
        var _postBody = {};
        var _urlParams = {};
        if (typeof where !== 'undefined' && where !== null)
            _urlParams.where = where;
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * Queries credentials of People.
     *
     * @param {any} id People id
     *
     * @param {object} filter
     *
     * @returns {object[]} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `People` object.)
     * </em>
     */
    PeopleApi.prototype.getCredentials = function (id, filter, customHeaders) {
        if (filter === void 0) { filter = {}; }
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/:id/credentials";
        var _routeParams = {
            id: id
        };
        var _postBody = {};
        var _urlParams = {};
        if (typeof filter !== 'undefined' && filter !== null)
            _urlParams.filter = filter;
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * Creates a new instance in credentials of this model.
     *
     * @param {any} id People id
     *
     * @param {object} data Request data.
     *
     * This method expects a subset of model properties as request parameters.
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `People` object.)
     * </em>
     */
    PeopleApi.prototype.createCredentials = function (id, data, customHeaders) {
        if (data === void 0) { data = {}; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/:id/credentials";
        var _routeParams = {
            id: id
        };
        var _postBody = {
            data: data
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * Deletes all credentials of this model.
     *
     * @param {any} id People id
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * This method returns no data.
     */
    PeopleApi.prototype.deleteCredentials = function (id, customHeaders) {
        var _method = "DELETE";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/:id/credentials";
        var _routeParams = {
            id: id
        };
        var _postBody = {};
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * Counts credentials of People.
     *
     * @param {any} id People id
     *
     * @param {object} where Criteria to match model instances
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `count` – `{number}` -
     */
    PeopleApi.prototype.countCredentials = function (id, where, customHeaders) {
        if (where === void 0) { where = {}; }
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/:id/credentials/count";
        var _routeParams = {
            id: id
        };
        var _postBody = {};
        var _urlParams = {};
        if (typeof where !== 'undefined' && where !== null)
            _urlParams.where = where;
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * Patch attributes for a model instance and persist it into the data source.
     *
     * @param {any} id People id
     *
     * @param {object} data Request data.
     *
     *  - `data` – `{object}` - An object of model property name/value pairs
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `People` object.)
     * </em>
     */
    PeopleApi.prototype.patchAttributes = function (id, data, customHeaders) {
        if (data === void 0) { data = {}; }
        var _method = "PATCH";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/:id";
        var _routeParams = {
            id: id
        };
        var _postBody = {
            data: data
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * Login a user with username/email and password.
     *
     * @param {string} include Related objects to include in the response. See the description of return value for more details.
     *   Default value: `user`.
     *
     *  - `rememberMe` - `boolean` - Whether the authentication credentials
     *     should be remembered in localStorage across app/browser restarts.
     *     Default: `true`.
     *
     * @param {object} data Request data.
     *
     * This method expects a subset of model properties as request parameters.
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` - The response body contains properties of the AccessToken created on login.
           *   Depending on the value of `include` parameter, the body may contain additional properties:
           *
           *     - `user` - `U+007BUserU+007D` - Data of the currently logged in user. (`include=user`)
           *
           *
     */
    PeopleApi.prototype.login = function (credentials, include, rememberMe, customHeaders) {
        var _this = this;
        if (include === void 0) { include = 'user'; }
        if (rememberMe === void 0) { rememberMe = true; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/login";
        var _routeParams = {};
        var _postBody = {
            credentials: credentials
        };
        var _urlParams = {};
        if (typeof include !== 'undefined' && include !== null)
            _urlParams.include = include;
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders)
            .map(function (response) {
            response = response.success.data;
            response.ttl = parseInt(response.ttl);
            response.rememberMe = rememberMe;
            _this.auth.setToken(response);
            return response;
        });
        return result;
    };
    /**
     * Logout a user with access token.
     *
     * @param {object} data Request data.
     *
     * This method does not accept any data. Supply an empty object.
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    PeopleApi.prototype.logout = function (customHeaders) {
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/logout";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        _urlParams.access_token = this.auth.getAccessTokenId();
        this.auth.clear();
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * Trigger user's identity verification with configured verifyOptions
     *
     * @param {any} id People id
     *
     * @param {object} data Request data.
     *
     * This method does not accept any data. Supply an empty object.
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * This method returns no data.
     */
    PeopleApi.prototype.verify = function (id, customHeaders) {
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/:id/verify";
        var _routeParams = {
            id: id
        };
        var _postBody = {};
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * Confirm a user registration with identity verification token.
     *
     * @param {string} uid
     *
     * @param {string} token
     *
     * @param {string} redirect
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * This method returns no data.
     */
    PeopleApi.prototype.confirm = function (uid, token, redirect, customHeaders) {
        if (redirect === void 0) { redirect = {}; }
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/confirm";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        if (typeof uid !== 'undefined' && uid !== null)
            _urlParams.uid = uid;
        if (typeof token !== 'undefined' && token !== null)
            _urlParams.token = token;
        if (typeof redirect !== 'undefined' && redirect !== null)
            _urlParams.redirect = redirect;
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * Change a user's password.
     *
     * @param {object} data Request data.
     *
     *  - `oldPassword` – `{string}` -
     *
     *  - `newPassword` – `{string}` -
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * This method returns no data.
     */
    PeopleApi.prototype.changePassword = function (oldPassword, newPassword, customHeaders) {
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/change-password";
        var _routeParams = {};
        var _postBody = {
            data: {
                oldPassword: oldPassword,
                newPassword: newPassword
            }
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} data Request data.
     *
     *  - `firebaseToken` – `{string}` -
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    PeopleApi.prototype.updateToken = function (firebaseToken, customHeaders) {
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/updateToken";
        var _routeParams = {};
        var _postBody = {
            data: {
                firebaseToken: firebaseToken
            }
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} data Request data.
     *
     *  - `driverId` – `{string}` -
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    PeopleApi.prototype.getDriverCurrentLoc = function (driverId, customHeaders) {
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/getDriverCurrentLoc";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        if (typeof driverId !== 'undefined' && driverId !== null)
            _urlParams.driverId = driverId;
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} data Request data.
     *
     *  - `name` – `{string}` -
     *
     *  - `mobile` – `{string}` -
     *
     *  - `email` – `{string}` -
     *
     *  - `req` – `{object}` -
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    PeopleApi.prototype.editProfile = function (name, mobile, email, req, customHeaders) {
        if (name === void 0) { name = {}; }
        if (mobile === void 0) { mobile = {}; }
        if (email === void 0) { email = {}; }
        if (req === void 0) { req = {}; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/editProfile";
        var _routeParams = {};
        var _postBody = {
            data: {
                name: name,
                mobile: mobile,
                email: email
            }
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} data Request data.
     *
     *  - `req` – `{object}` -
     *
     *  - `location` – `{GeoPoint}` -
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    PeopleApi.prototype.updateDriverLoc = function (req, location, customHeaders) {
        if (req === void 0) { req = {}; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/updateDriverLoc";
        var _routeParams = {};
        var _postBody = {
            data: {
                location: location
            }
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} data Request data.
     *
     *  - `req` – `{object}` -
     *
     *  - `res` – `{object}` -
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    PeopleApi.prototype.uploadDriverDoc = function (req, res, customHeaders) {
        if (req === void 0) { req = {}; }
        if (res === void 0) { res = {}; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/uploadDriverDoc";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    PeopleApi.prototype.uploadProfilePic = function (req, res, customHeaders) {
        if (req === void 0) { req = {}; }
        if (res === void 0) { res = {}; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/uploadProfilePic";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} req
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    PeopleApi.prototype.getMyInfo = function (req, customHeaders) {
        if (req === void 0) { req = {}; }
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/getMyInfo";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {string} peopleId
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    PeopleApi.prototype.getPeopleInfo = function (peopleId, customHeaders) {
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/getPeopleInfo";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        if (typeof peopleId !== 'undefined' && peopleId !== null)
            _urlParams.peopleId = peopleId;
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} data Request data.
     *
     *  - `realm` – `{string}` -
     *
     *  - `name` – `{string}` -
     *
     *  - `email` – `{string}` -
     *
     *  - `mobile` – `{string}` -
     *
     *  - `address` – `{string}` -
     *
     *  - `password` – `{string}` -
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `People` object.)
     * </em>
     */
    PeopleApi.prototype.createAdmin = function (realm, name, email, mobile, address, password, customHeaders) {
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/createAdmin";
        var _routeParams = {};
        var _postBody = {
            data: {
                realm: realm,
                name: name,
                email: email,
                mobile: mobile,
                address: address,
                password: password
            }
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * Signup users
     *
     * @param {object} data Request data.
     *
     *  - `req` – `{object}` -
     *
     *  - `res` – `{object}` -
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    PeopleApi.prototype.signup = function (req, res, customHeaders) {
        if (req === void 0) { req = {}; }
        if (res === void 0) { res = {}; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/signup";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * profile details
     *
     * @param {object} data Request data.
     *
     *  - `req` – `{object}` -
     *
     *  - `res` – `{object}` -
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `People` object.)
     * </em>
     */
    PeopleApi.prototype.viewProfile = function (req, res, customHeaders) {
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/viewProfile";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} data Request data.
     *
     *  - `driverId` – `{string}` -
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    PeopleApi.prototype.userApproval = function (peopleId, adminVerifiedStatus, customHeaders) {
        if (peopleId === void 0) { peopleId = {}; }
        if (adminVerifiedStatus === void 0) { adminVerifiedStatus = {}; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/userApproval";
        var _routeParams = {};
        var _postBody = {
            data: {
                peopleId: peopleId,
                adminVerifiedStatus: adminVerifiedStatus
            }
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * route details
     *
     * @param {GeoPoint} position
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    PeopleApi.prototype.findNearDriver = function (position, vehicleId, vehicleType, customHeaders) {
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/findNearDriver";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        if (typeof position !== 'undefined' && position !== null)
            _urlParams.position = position;
        if (typeof vehicleId !== 'undefined' && vehicleId !== null)
            _urlParams.vehicleId = vehicleId;
        if (typeof vehicleType !== 'undefined' && vehicleType !== null)
            _urlParams.vehicleType = vehicleType;
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**.map((instances: Array<People>) =>
          instances.map((instance: People) => new People(instance))
     * Find all instances of the model matched by filter from the data source.
     *
     * @param {object} filter Filter defining fields, where, aggregate, order, offset, and limit
     *
     * @param {object} options options
     *
     * @returns {object[]} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `People` object.)
     * </em>
     */
    PeopleApi.prototype.aggregate = function (filter, options, customHeaders) {
        if (filter === void 0) { filter = {}; }
        if (options === void 0) { options = {}; }
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/aggregate";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        if (typeof filter !== 'undefined' && filter !== null)
            _urlParams.filter = filter;
        if (typeof options !== 'undefined' && options !== null)
            _urlParams.options = options;
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * Creates a new instance in identities of this model.
     *
     * @param {any} id People id
     *
     * @param {object} data Request data.
     *
     * This method expects a subset of model properties as request parameters.
     *
     * @returns {object[]} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `People` object.)
     * </em>
     */
    PeopleApi.prototype.createManyIdentities = function (id, data, customHeaders) {
        if (data === void 0) { data = []; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/:id/identities";
        var _routeParams = {
            id: id
        };
        var _postBody = {
            data: data
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * Creates a new instance in credentials of this model.
     *
     * @param {any} id People id
     *
     * @param {object} data Request data.
     *
     * This method expects a subset of model properties as request parameters.
     *
     * @returns {object[]} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `People` object.)
     * </em>
     */
    PeopleApi.prototype.createManyCredentials = function (id, data, customHeaders) {
        if (data === void 0) { data = []; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/People/:id/credentials";
        var _routeParams = {
            id: id
        };
        var _postBody = {
            data: data
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * @ngdoc method
     * @name sdk.People#getCurrent
     * @methodOf sdk.People
     *
     * @description
     *
     * Get data of the currently logged user. Fail with HTTP result 401
     * when there is no user logged in.
     *
     * @returns object An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     */
    PeopleApi.prototype.getCurrent = function (filter) {
        if (filter === void 0) { filter = {}; }
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() + "/People" + "/:id";
        var id = this.auth.getCurrentUserId();
        if (id == null)
            id = '__anonymous__';
        var _routeParams = { id: id };
        var _urlParams = {};
        var _postBody = {};
        if (filter)
            _urlParams.filter = filter;
        return this.request(_method, _url, _routeParams, _urlParams, _postBody);
    };
    /**
     * Get data of the currently logged user that was returned by the last
     * call to {@link sdk.People#login} or
     * {@link sdk.People#getCurrent}. Return null when there
     * is no user logged in or the data of the current user were not fetched
     * yet.
     *
     * @returns object An Account instance.
     */
    PeopleApi.prototype.getCachedCurrent = function () {
        return this.auth.getCurrentUserData();
    };
    /**
     * Get data of the currently logged access tokern that was returned by the last
     * call to {@link sdk.People#login}
     *
     * @returns object An AccessToken instance.
     */
    PeopleApi.prototype.getCurrentToken = function () {
        return this.auth.getToken();
    };
    /**
     * @name sdk.People#isAuthenticated
     *
     * @returns {boolean} True if the current user is authenticated (logged in).
     */
    PeopleApi.prototype.isAuthenticated = function () {
        return !(this.getCurrentId() === '' || this.getCurrentId() == null || this.getCurrentId() == 'null');
    };
    /**
     * @name sdk.People#getCurrentId
     *
     * @returns object Id of the currently logged-in user or null.
     */
    PeopleApi.prototype.getCurrentId = function () {
        return this.auth.getCurrentUserId();
    };
    /**
     * The name of the model represented by this $resource,
     * i.e. `People`.
     */
    PeopleApi.prototype.getModelName = function () {
        return "People";
    };
    return PeopleApi;
}(__WEBPACK_IMPORTED_MODULE_3__core_base_service__["a" /* BaseLoopBackApi */]));
PeopleApi = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __param(0, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */])),
    __param(1, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */])),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */])),
    __param(3, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */])),
    __param(4, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */])),
    __param(5, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Optional */])()), __param(5, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */])),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */]) === "function" && _f || Object])
], PeopleApi);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=People.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/services/custom/Rating.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RatingApi; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__SDKModels__ = __webpack_require__("../../../../../src/app/sdk/services/custom/SDKModels.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_base_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/base.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__lb_config__ = __webpack_require__("../../../../../src/app/sdk/lb.config.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_auth_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_search_params__ = __webpack_require__("../../../../../src/app/sdk/services/core/search.params.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_error_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__ = __webpack_require__("../../../../../src/app/sdk/sockets/socket.connections.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
/* tslint:disable */









/**
 * Api services for the `Rating` model.
 */
var RatingApi = (function (_super) {
    __extends(RatingApi, _super);
    function RatingApi(http, connection, models, auth, searchParams, errorHandler) {
        var _this = _super.call(this, http, connection, models, auth, searchParams, errorHandler) || this;
        _this.http = http;
        _this.connection = connection;
        _this.models = models;
        _this.auth = auth;
        _this.searchParams = searchParams;
        _this.errorHandler = errorHandler;
        return _this;
    }
    /**
     * Patch attributes for a model instance and persist it into the data source.
     *
     * @param {any} id Rating id
     *
     * @param {object} data Request data.
     *
     *  - `data` – `{object}` - An object of model property name/value pairs
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `Rating` object.)
     * </em>
     */
    RatingApi.prototype.patchAttributes = function (id, data, customHeaders) {
        if (data === void 0) { data = {}; }
        var _method = "PATCH";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Ratings/:id";
        var _routeParams = {
            id: id
        };
        var _postBody = {
            data: data
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} data Request data.
     *
     *  - `req` – `{object}` -
     *
     *  - `jobId` – `{string}` -
     *
     *  - `userRating` – `{number}` -
     *
     *  - `content` – `{string}` -
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    RatingApi.prototype.giveRating = function (req, jobId, userRating, content, customHeaders) {
        if (req === void 0) { req = {}; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Ratings/giveRating";
        var _routeParams = {};
        var _postBody = {
            data: {
                jobId: jobId,
                userRating: userRating,
                content: content
            }
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {string} peopleId
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    RatingApi.prototype.getRatings = function (peopleId, customHeaders) {
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Ratings/getRatings";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        if (typeof peopleId !== 'undefined' && peopleId !== null)
            _urlParams.peopleId = peopleId;
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * The name of the model represented by this $resource,
     * i.e. `Rating`.
     */
    RatingApi.prototype.getModelName = function () {
        return "Rating";
    };
    return RatingApi;
}(__WEBPACK_IMPORTED_MODULE_3__core_base_service__["a" /* BaseLoopBackApi */]));
RatingApi = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __param(0, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */])),
    __param(1, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */])),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */])),
    __param(3, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */])),
    __param(4, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */])),
    __param(5, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Optional */])()), __param(5, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */])),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */]) === "function" && _f || Object])
], RatingApi);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=Rating.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/services/custom/Requestfordriver.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RequestfordriverApi; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__SDKModels__ = __webpack_require__("../../../../../src/app/sdk/services/custom/SDKModels.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_base_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/base.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__lb_config__ = __webpack_require__("../../../../../src/app/sdk/lb.config.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_auth_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_search_params__ = __webpack_require__("../../../../../src/app/sdk/services/core/search.params.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_error_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__ = __webpack_require__("../../../../../src/app/sdk/sockets/socket.connections.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
/* tslint:disable */









/**
 * Api services for the `Requestfordriver` model.
 */
var RequestfordriverApi = (function (_super) {
    __extends(RequestfordriverApi, _super);
    function RequestfordriverApi(http, connection, models, auth, searchParams, errorHandler) {
        var _this = _super.call(this, http, connection, models, auth, searchParams, errorHandler) || this;
        _this.http = http;
        _this.connection = connection;
        _this.models = models;
        _this.auth = auth;
        _this.searchParams = searchParams;
        _this.errorHandler = errorHandler;
        return _this;
    }
    /**
     * Patch attributes for a model instance and persist it into the data source.
     *
     * @param {any} id Requestfordriver id
     *
     * @param {object} data Request data.
     *
     *  - `data` – `{object}` - An object of model property name/value pairs
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `Requestfordriver` object.)
     * </em>
     */
    RequestfordriverApi.prototype.patchAttributes = function (id, data, customHeaders) {
        if (data === void 0) { data = {}; }
        var _method = "PATCH";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Requestfordrivers/:id";
        var _routeParams = {
            id: id
        };
        var _postBody = {
            data: data
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * requesting for driver
     *
     * @param {object} data Request data.
     *
     *  - `req` – `{object}` -
     *
     *  - `driverId` – `{string}` -
     *
     *  - `vehicleId` – `{string}` -
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    RequestfordriverApi.prototype.makeRequest = function (req, driverId, vehicleId, customHeaders) {
        if (req === void 0) { req = {}; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Requestfordrivers/makeRequest";
        var _routeParams = {};
        var _postBody = {
            data: {
                driverId: driverId,
                vehicleId: vehicleId
            }
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} data Request data.
     *
     *  - `req` – `{object}` -
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    RequestfordriverApi.prototype.getRequests = function (req, customHeaders) {
        if (req === void 0) { req = {}; }
        var _method = "Get";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Requestfordrivers/getRequests";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * confirming request
     *
     * @param {object} data Request data.
     *
     *  - `req` – `{object}` -
     *
     *  - `requestId` – `{string}` -
     *
     *  - `status` – `{string}` -
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    RequestfordriverApi.prototype.confirmByDriver = function (req, requestId, customHeaders) {
        if (req === void 0) { req = {}; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Requestfordrivers/confirmByDriver";
        var _routeParams = {};
        var _postBody = {
            data: {
                requestId: requestId
            }
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * The name of the model represented by this $resource,
     * i.e. `Requestfordriver`.
     */
    RequestfordriverApi.prototype.getModelName = function () {
        return "Requestfordriver";
    };
    return RequestfordriverApi;
}(__WEBPACK_IMPORTED_MODULE_3__core_base_service__["a" /* BaseLoopBackApi */]));
RequestfordriverApi = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __param(0, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */])),
    __param(1, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */])),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */])),
    __param(3, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */])),
    __param(4, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */])),
    __param(5, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Optional */])()), __param(5, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */])),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */]) === "function" && _f || Object])
], RequestfordriverApi);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=Requestfordriver.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/services/custom/SDKModels.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SDKModels; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_Email__ = __webpack_require__("../../../../../src/app/sdk/models/Email.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_People__ = __webpack_require__("../../../../../src/app/sdk/models/People.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_Transaction__ = __webpack_require__("../../../../../src/app/sdk/models/Transaction.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__models_Vehicle__ = __webpack_require__("../../../../../src/app/sdk/models/Vehicle.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__models_Container__ = __webpack_require__("../../../../../src/app/sdk/models/Container.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__models_VehicleType__ = __webpack_require__("../../../../../src/app/sdk/models/VehicleType.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__models_Job__ = __webpack_require__("../../../../../src/app/sdk/models/Job.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__models_JobApply__ = __webpack_require__("../../../../../src/app/sdk/models/JobApply.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
/* tslint:disable */









var SDKModels = (function () {
    function SDKModels() {
        this.models = {
            Email: __WEBPACK_IMPORTED_MODULE_1__models_Email__["a" /* Email */],
            People: __WEBPACK_IMPORTED_MODULE_2__models_People__["a" /* People */],
            Transaction: __WEBPACK_IMPORTED_MODULE_3__models_Transaction__["a" /* Transaction */],
            Vehicle: __WEBPACK_IMPORTED_MODULE_4__models_Vehicle__["a" /* Vehicle */],
            Container: __WEBPACK_IMPORTED_MODULE_5__models_Container__["a" /* Container */],
            VehicleType: __WEBPACK_IMPORTED_MODULE_6__models_VehicleType__["a" /* VehicleType */],
            Job: __WEBPACK_IMPORTED_MODULE_7__models_Job__["a" /* Job */],
            JobApply: __WEBPACK_IMPORTED_MODULE_8__models_JobApply__["a" /* JobApply */],
        };
    }
    SDKModels.prototype.get = function (modelName) {
        return this.models[modelName];
    };
    SDKModels.prototype.getAll = function () {
        return this.models;
    };
    SDKModels.prototype.getModelNames = function () {
        return Object.keys(this.models);
    };
    return SDKModels;
}());
SDKModels = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])()
], SDKModels);

//# sourceMappingURL=SDKModels.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/services/custom/Transaction.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TransactionApi; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__SDKModels__ = __webpack_require__("../../../../../src/app/sdk/services/custom/SDKModels.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_base_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/base.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__lb_config__ = __webpack_require__("../../../../../src/app/sdk/lb.config.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_auth_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_search_params__ = __webpack_require__("../../../../../src/app/sdk/services/core/search.params.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_error_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__ = __webpack_require__("../../../../../src/app/sdk/sockets/socket.connections.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
/* tslint:disable */









/**
 * Api services for the `Transaction` model.
 */
var TransactionApi = (function (_super) {
    __extends(TransactionApi, _super);
    function TransactionApi(http, connection, models, auth, searchParams, errorHandler) {
        var _this = _super.call(this, http, connection, models, auth, searchParams, errorHandler) || this;
        _this.http = http;
        _this.connection = connection;
        _this.models = models;
        _this.auth = auth;
        _this.searchParams = searchParams;
        _this.errorHandler = errorHandler;
        return _this;
    }
    /**
     * Patch attributes for a model instance and persist it into the data source.
     *
     * @param {any} id Transaction id
     *
     * @param {object} data Request data.
     *
     *  - `data` – `{object}` - An object of model property name/value pairs
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `Transaction` object.)
     * </em>
     */
    TransactionApi.prototype.patchAttributes = function (id, data, customHeaders) {
        if (data === void 0) { data = {}; }
        var _method = "PATCH";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Transactions/:id";
        var _routeParams = {
            id: id
        };
        var _postBody = {
            data: data
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} req
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    TransactionApi.prototype.getTransaction = function (req, customHeaders) {
        if (req === void 0) { req = {}; }
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Transactions/getTransaction";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} data Request data.
     *
     * This method does not accept any data. Supply an empty object.
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    TransactionApi.prototype.b2c = function (customHeaders) {
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Transactions/b2c";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} data Request data.
     *
     * This method does not accept any data. Supply an empty object.
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    TransactionApi.prototype.accountBalance = function (customHeaders) {
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Transactions/accountBalance";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} data Request data.
     *
     *  - `req` – `{object}` -
     *
     *  - `res` – `{object}` -
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    TransactionApi.prototype.queueTimeOutUrl = function (req, res, customHeaders) {
        if (req === void 0) { req = {}; }
        if (res === void 0) { res = {}; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Transactions/queueTimeOutUrl";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} data Request data.
     *
     *  - `req` – `{object}` -
     *
     *  - `req` – `{object}` -
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    // public balanceResultUrl(req: any = {}, req: any = {}, customHeaders?: Function): Observable<any> {
    // let _method: string = "POST";
    // let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    // "/Transactions/balanceResultUrl";
    // let _routeParams: any = {};
    // let _postBody: any = {};
    // let _urlParams: any = {};
    // let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    // return result;
    // }
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} data Request data.
     *
     * This method does not accept any data. Supply an empty object.
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    TransactionApi.prototype.transactionStatus = function (customHeaders) {
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Transactions/transactionStatus";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} data Request data.
     *
     *  - `mobile` – `{string}` -
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    TransactionApi.prototype.c2b = function (mobile, customHeaders) {
        if (mobile === void 0) { mobile = {}; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Transactions/c2b";
        var _routeParams = {};
        var _postBody = {
            data: {
                mobile: mobile
            }
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} data Request data.
     *
     *  - `amount` – `{number}` -
     *
     *  - `mobileNumber` – `{string}` -
     *
     *  - `accountRef` – `{string}` -
     *
     *  - `transDesc` – `{string}` -
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    TransactionApi.prototype.STKPushSimulation = function (amount, mobileNumber, accountRef, transDesc, customHeaders) {
        if (amount === void 0) { amount = {}; }
        if (mobileNumber === void 0) { mobileNumber = {}; }
        if (accountRef === void 0) { accountRef = {}; }
        if (transDesc === void 0) { transDesc = {}; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Transactions/STKPushSimulation";
        var _routeParams = {};
        var _postBody = {
            data: {
                amount: amount,
                mobileNumber: mobileNumber,
                accountRef: accountRef,
                transDesc: transDesc
            }
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} data Request data.
     *
     *  - `req` – `{object}` -
     *
     *  - `res` – `{object}` -
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    TransactionApi.prototype.STKPushCallback = function (req, res, customHeaders) {
        if (req === void 0) { req = {}; }
        if (res === void 0) { res = {}; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Transactions/STKPushCallback";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} data Request data.
     *
     *  - `checkoutRequestID` – `{string}` -
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    TransactionApi.prototype.STKPushQuery = function (checkoutRequestID, customHeaders) {
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Transactions/STKPushQuery";
        var _routeParams = {};
        var _postBody = {
            data: {
                checkoutRequestID: checkoutRequestID
            }
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * The name of the model represented by this $resource,
     * i.e. `Transaction`.
     */
    TransactionApi.prototype.getModelName = function () {
        return "Transaction";
    };
    return TransactionApi;
}(__WEBPACK_IMPORTED_MODULE_3__core_base_service__["a" /* BaseLoopBackApi */]));
TransactionApi = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __param(0, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */])),
    __param(1, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */])),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */])),
    __param(3, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */])),
    __param(4, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */])),
    __param(5, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Optional */])()), __param(5, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */])),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */]) === "function" && _f || Object])
], TransactionApi);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=Transaction.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/services/custom/Vehicle.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VehicleApi; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__SDKModels__ = __webpack_require__("../../../../../src/app/sdk/services/custom/SDKModels.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_base_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/base.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__lb_config__ = __webpack_require__("../../../../../src/app/sdk/lb.config.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_auth_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_search_params__ = __webpack_require__("../../../../../src/app/sdk/services/core/search.params.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_error_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__ = __webpack_require__("../../../../../src/app/sdk/sockets/socket.connections.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
/* tslint:disable */









/**
 * Api services for the `Vehicle` model.
 */
var VehicleApi = (function (_super) {
    __extends(VehicleApi, _super);
    function VehicleApi(http, connection, models, auth, searchParams, errorHandler) {
        var _this = _super.call(this, http, connection, models, auth, searchParams, errorHandler) || this;
        _this.http = http;
        _this.connection = connection;
        _this.models = models;
        _this.auth = auth;
        _this.searchParams = searchParams;
        _this.errorHandler = errorHandler;
        return _this;
    }
    /**
     * Patch attributes for a model instance and persist it into the data source.
     *
     * @param {any} id Vehicle id
     *
     * @param {object} data Request data.
     *
     *  - `data` – `{object}` - An object of model property name/value pairs
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `Vehicle` object.)
     * </em>
     */
    VehicleApi.prototype.approveAdmin = function (vehicleId, approveStatus, customHeaders) {
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Vehicles/approveAdmin";
        var _routeParams = {};
        var _postBody = {
            data: {
                vehicleId: vehicleId,
                approveStatus: approveStatus
            }
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    VehicleApi.prototype.getVehicleByAdmin = function (status, customHeaders) {
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Vehicles/getVehicleByAdmin";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        if (typeof status !== 'undefined' && status !== null)
            _urlParams.status = status;
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    VehicleApi.prototype.patchAttributes = function (id, data, customHeaders) {
        if (data === void 0) { data = {}; }
        var _method = "PATCH";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Vehicles/:id";
        var _routeParams = {
            id: id
        };
        var _postBody = {
            data: data
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * registering vehicles
     *
     * @param {object} data Request data.
     *
     *  - `req` – `{object}` -
     *
     *  - `res` – `{object}` -
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    VehicleApi.prototype.addVehicle = function (req, res, customHeaders) {
        if (req === void 0) { req = {}; }
        if (res === void 0) { res = {}; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Vehicles/addVehicle";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * uploading documents
     *
     * @param {object} data Request data.
     *
     *  - `req` – `{object}` -
     *
     *  - `res` – `{object}` -
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    VehicleApi.prototype.uploadDocs = function (req, res, customHeaders) {
        if (req === void 0) { req = {}; }
        if (res === void 0) { res = {}; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Vehicles/uploadDocs";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * details of all added vehicles
     *
     * @param {object} data Request data.
     *
     *  - `filter` – `{object}` -
     *
     *  - `req` – `{object}` -
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `Vehicle` object.)
     * </em>
     */
    VehicleApi.prototype.getMyVehicles = function (filter, req, customHeaders) {
        if (filter === void 0) { filter = {}; }
        if (req === void 0) { req = {}; }
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Vehicles/getMyVehicles";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        if (typeof filter !== 'undefined' && filter !== null)
            _urlParams.filter = filter;
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    VehicleApi.prototype.getVehicle = function (filter, req, customHeaders) {
        if (filter === void 0) { filter = {}; }
        if (req === void 0) { req = {}; }
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Vehicles";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        if (typeof filter !== 'undefined' && filter !== null)
            _urlParams.filter = filter;
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    VehicleApi.prototype.getVehicleById = function (vehicleId, customHeaders) {
        if (vehicleId === void 0) { vehicleId = {}; }
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Vehicles/getVehicleById";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        if (typeof vehicleId !== 'undefined' && vehicleId !== null)
            _urlParams.vehicleId = vehicleId;
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    VehicleApi.prototype.getOwnerVehicles = function (filter, ownerId, customHeaders) {
        if (filter === void 0) { filter = {}; }
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Vehicles/getOwnerVehicles";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        if (typeof filter !== 'undefined' && filter !== null)
            _urlParams.filter = filter;
        if (typeof ownerId !== 'undefined' && ownerId !== null)
            _urlParams.ownerId = ownerId;
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    VehicleApi.prototype.getAssessPrice = function (customHeaders) {
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Vehicles/getAssessPrice";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    VehicleApi.prototype.vehicleAssessment = function (req, vehicleId, amount, paymentType, numberOfYear, paymentOptions, customHeaders) {
        if (req === void 0) { req = {}; }
        if (paymentOptions === void 0) { paymentOptions = {}; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Vehicles/assessment";
        var _routeParams = {};
        var _postBody = {
            data: {
                vehicleId: vehicleId,
                amount: amount,
                paymentType: paymentType,
                numberOfYear: numberOfYear,
                paymentOptions: paymentOptions
            }
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * The name of the model represented by this $resource,
     * i.e. `Vehicle`.
     */
    VehicleApi.prototype.getModelName = function () {
        return "Vehicle";
    };
    return VehicleApi;
}(__WEBPACK_IMPORTED_MODULE_3__core_base_service__["a" /* BaseLoopBackApi */]));
VehicleApi = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __param(0, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */])),
    __param(1, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */])),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */])),
    __param(3, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */])),
    __param(4, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */])),
    __param(5, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Optional */])()), __param(5, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */])),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */]) === "function" && _f || Object])
], VehicleApi);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=Vehicle.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/services/custom/VehicleType.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VehicleTypeApi; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__SDKModels__ = __webpack_require__("../../../../../src/app/sdk/services/custom/SDKModels.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_base_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/base.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__lb_config__ = __webpack_require__("../../../../../src/app/sdk/lb.config.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_auth_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_search_params__ = __webpack_require__("../../../../../src/app/sdk/services/core/search.params.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_error_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__ = __webpack_require__("../../../../../src/app/sdk/sockets/socket.connections.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
/* tslint:disable */









/**
 * Api services for the `VehicleType` model.
 */
var VehicleTypeApi = (function (_super) {
    __extends(VehicleTypeApi, _super);
    function VehicleTypeApi(http, connection, models, auth, searchParams, errorHandler) {
        var _this = _super.call(this, http, connection, models, auth, searchParams, errorHandler) || this;
        _this.http = http;
        _this.connection = connection;
        _this.models = models;
        _this.auth = auth;
        _this.searchParams = searchParams;
        _this.errorHandler = errorHandler;
        return _this;
    }
    /**
     * Patch attributes for a model instance and persist it into the data source.
     *
     * @param {any} id VehicleType id
     *
     * @param {object} data Request data.
     *
     *  - `data` – `{object}` - An object of model property name/value pairs
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `VehicleType` object.)
     * </em>
     */
    VehicleTypeApi.prototype.patchAttributes = function (id, data, customHeaders) {
        if (data === void 0) { data = {}; }
        var _method = "PATCH";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/VehicleTypes/:id";
        var _routeParams = {
            id: id
        };
        var _postBody = {
            data: data
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} data Request data.
     *
     *  - `req` – `{object}` -
     *
     *  - `res` – `{object}` -
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    VehicleTypeApi.prototype.addOrUpdate = function (req, res, customHeaders) {
        if (req === void 0) { req = {}; }
        if (res === void 0) { res = {}; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/VehicleTypes/addOrUpdate";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    VehicleTypeApi.prototype.getVehicleType = function (req, typeOfJob, customHeaders) {
        if (req === void 0) { req = {}; }
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/VehicleTypes/getVehicleType";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        if (typeof typeOfJob !== 'undefined' && typeOfJob !== null)
            _urlParams.typeOfJob = typeOfJob;
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * The name of the model represented by this $resource,
     * i.e. `VehicleType`.
     */
    VehicleTypeApi.prototype.getModelName = function () {
        return "VehicleType";
    };
    return VehicleTypeApi;
}(__WEBPACK_IMPORTED_MODULE_3__core_base_service__["a" /* BaseLoopBackApi */]));
VehicleTypeApi = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __param(0, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */])),
    __param(1, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */])),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */])),
    __param(3, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */])),
    __param(4, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */])),
    __param(5, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Optional */])()), __param(5, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */])),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */]) === "function" && _f || Object])
], VehicleTypeApi);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=VehicleType.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/services/custom/Wallet.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WalletApi; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__SDKModels__ = __webpack_require__("../../../../../src/app/sdk/services/custom/SDKModels.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_base_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/base.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__lb_config__ = __webpack_require__("../../../../../src/app/sdk/lb.config.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_auth_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_search_params__ = __webpack_require__("../../../../../src/app/sdk/services/core/search.params.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_error_service__ = __webpack_require__("../../../../../src/app/sdk/services/core/error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__ = __webpack_require__("../../../../../src/app/sdk/sockets/socket.connections.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
/* tslint:disable */









/**
 * Api services for the `Wallet` model.
 */
var WalletApi = (function (_super) {
    __extends(WalletApi, _super);
    function WalletApi(http, connection, models, auth, searchParams, errorHandler) {
        var _this = _super.call(this, http, connection, models, auth, searchParams, errorHandler) || this;
        _this.http = http;
        _this.connection = connection;
        _this.models = models;
        _this.auth = auth;
        _this.searchParams = searchParams;
        _this.errorHandler = errorHandler;
        return _this;
    }
    /**
     * Patch attributes for a model instance and persist it into the data source.
     *
     * @param {any} id Wallet id
     *
     * @param {object} data Request data.
     *
     *  - `data` – `{object}` - An object of model property name/value pairs
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * <em>
     * (The remote method definition does not provide any description.
     * This usually means the response is a `Wallet` object.)
     * </em>
     */
    WalletApi.prototype.patchAttributes = function (id, data, customHeaders) {
        if (data === void 0) { data = {}; }
        var _method = "PATCH";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Wallets/:id";
        var _routeParams = {
            id: id
        };
        var _postBody = {
            data: data
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} req
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    WalletApi.prototype.getWallet = function (req, customHeaders) {
        if (req === void 0) { req = {}; }
        var _method = "GET";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Wallets/getWallet";
        var _routeParams = {};
        var _postBody = {};
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} data Request data.
     *
     *  - `req` – `{object}` -
     *
     *  - `mobileNumber` – `{string}` -
     *
     *  - `amount` – `{number}` -
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    WalletApi.prototype.addMPesaMoney = function (req, mobileNumber, amount, customHeaders) {
        if (req === void 0) { req = {}; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Wallets/addMPesaMoney";
        var _routeParams = {};
        var _postBody = {
            data: {
                mobileNumber: mobileNumber,
                amount: amount
            }
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * <em>
           * (The remote method definition does not provide any description.)
           * </em>
     *
     * @param {object} data Request data.
     *
     *  - `req` – `{object}` -
     *
     *  - `fName` – `{string}` -
     *
     *  - `lName` – `{string}` -
     *
     *  - `amount` – `{number}` -
     *
     *  - `cardNumber` – `{number}` -
     *
     *  - `month` – `{number}` -
     *
     *  - `year` – `{number}` -
     *
     *  - `cvv` – `{number}` -
     *
     * @returns {object} An empty reference that will be
     *   populated with the actual data once the response is returned
     *   from the server.
     *
     * Data properties:
     *
     *  - `success` – `{object}` -
     */
    WalletApi.prototype.addCardMoney = function (req, fName, lName, amount, cardNumber, month, year, cvv, customHeaders) {
        if (req === void 0) { req = {}; }
        var _method = "POST";
        var _url = __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath() + "/" + __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getApiVersion() +
            "/Wallets/addCardMoney";
        var _routeParams = {};
        var _postBody = {
            data: {
                fName: fName,
                lName: lName,
                amount: amount,
                cardNumber: cardNumber,
                month: month,
                year: year,
                cvv: cvv
            }
        };
        var _urlParams = {};
        var result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
        return result;
    };
    /**
     * The name of the model represented by this $resource,
     * i.e. `Wallet`.
     */
    WalletApi.prototype.getModelName = function () {
        return "Wallet";
    };
    return WalletApi;
}(__WEBPACK_IMPORTED_MODULE_3__core_base_service__["a" /* BaseLoopBackApi */]));
WalletApi = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __param(0, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */])),
    __param(1, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */])),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */])),
    __param(3, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */])),
    __param(4, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */])),
    __param(5, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Optional */])()), __param(5, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */])),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8__sockets_socket_connections__["a" /* SocketConnection */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__SDKModels__["a" /* SDKModels */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* LoopBackAuth */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__core_search_params__["a" /* JSONSearchParams */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__core_error_service__["a" /* ErrorHandler */]) === "function" && _f || Object])
], WalletApi);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=Wallet.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/services/custom/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Email__ = __webpack_require__("../../../../../src/app/sdk/services/custom/Email.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__People__ = __webpack_require__("../../../../../src/app/sdk/services/custom/People.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_1__People__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Transaction__ = __webpack_require__("../../../../../src/app/sdk/services/custom/Transaction.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Vehicle__ = __webpack_require__("../../../../../src/app/sdk/services/custom/Vehicle.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_3__Vehicle__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Container__ = __webpack_require__("../../../../../src/app/sdk/services/custom/Container.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__VehicleType__ = __webpack_require__("../../../../../src/app/sdk/services/custom/VehicleType.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_5__VehicleType__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__Job__ = __webpack_require__("../../../../../src/app/sdk/services/custom/Job.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_6__Job__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__Notification__ = __webpack_require__("../../../../../src/app/sdk/services/custom/Notification.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__JobApply__ = __webpack_require__("../../../../../src/app/sdk/services/custom/JobApply.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__SDKModels__ = __webpack_require__("../../../../../src/app/sdk/services/custom/SDKModels.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__logger_service__ = __webpack_require__("../../../../../src/app/sdk/services/custom/logger.service.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__Requestfordriver__ = __webpack_require__("../../../../../src/app/sdk/services/custom/Requestfordriver.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__Wallet__ = __webpack_require__("../../../../../src/app/sdk/services/custom/Wallet.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__Otp__ = __webpack_require__("../../../../../src/app/sdk/services/custom/Otp.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__Rating__ = __webpack_require__("../../../../../src/app/sdk/services/custom/Rating.ts");
/* unused harmony namespace reexport */
/* tslint:disable */















//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/services/custom/logger.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoggerService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__lb_config__ = __webpack_require__("../../../../../src/app/sdk/lb.config.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
/* tslint:disable */


/**
* @author Jonathan Casarrubias <twitter:@johncasarrubias> <github:@johncasarrubias>
* @module LoggerService
* @license MIT
* @description
* Console Log wrapper that can be disabled in production mode
**/
var LoggerService = (function () {
    function LoggerService() {
    }
    LoggerService.prototype.log = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        if (__WEBPACK_IMPORTED_MODULE_1__lb_config__["a" /* LoopBackConfig */].debuggable())
            console.log.apply(console, args);
    };
    LoggerService.prototype.info = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        if (__WEBPACK_IMPORTED_MODULE_1__lb_config__["a" /* LoopBackConfig */].debuggable())
            console.info.apply(console, args);
    };
    LoggerService.prototype.error = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        if (__WEBPACK_IMPORTED_MODULE_1__lb_config__["a" /* LoopBackConfig */].debuggable())
            console.error.apply(console, args);
    };
    LoggerService.prototype.count = function (arg) {
        if (__WEBPACK_IMPORTED_MODULE_1__lb_config__["a" /* LoopBackConfig */].debuggable())
            console.count(arg);
    };
    LoggerService.prototype.group = function (arg) {
        if (__WEBPACK_IMPORTED_MODULE_1__lb_config__["a" /* LoopBackConfig */].debuggable())
            console.count(arg);
    };
    LoggerService.prototype.groupEnd = function () {
        if (__WEBPACK_IMPORTED_MODULE_1__lb_config__["a" /* LoopBackConfig */].debuggable())
            console.groupEnd();
    };
    LoggerService.prototype.profile = function (arg) {
        if (__WEBPACK_IMPORTED_MODULE_1__lb_config__["a" /* LoopBackConfig */].debuggable())
            console.count(arg);
    };
    LoggerService.prototype.profileEnd = function () {
        if (__WEBPACK_IMPORTED_MODULE_1__lb_config__["a" /* LoopBackConfig */].debuggable())
            console.profileEnd();
    };
    LoggerService.prototype.time = function (arg) {
        if (__WEBPACK_IMPORTED_MODULE_1__lb_config__["a" /* LoopBackConfig */].debuggable())
            console.time(arg);
    };
    LoggerService.prototype.timeEnd = function (arg) {
        if (__WEBPACK_IMPORTED_MODULE_1__lb_config__["a" /* LoopBackConfig */].debuggable())
            console.timeEnd(arg);
    };
    return LoggerService;
}());
LoggerService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])()
], LoggerService);

//# sourceMappingURL=logger.service.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/services/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__core_index__ = __webpack_require__("../../../../../src/app/sdk/services/core/index.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_0__core_index__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__custom_index__ = __webpack_require__("../../../../../src/app/sdk/services/custom/index.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_1__custom_index__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_1__custom_index__["b"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_1__custom_index__["c"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_1__custom_index__["d"]; });
/* tslint:disable */


//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/sockets/socket.browser.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SocketBrowser; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_socket_io_client__ = __webpack_require__("../../../../socket.io-client/lib/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_socket_io_client___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_socket_io_client__);
/* tslint:disable */

/**
* @author Jonathan Casarrubias <twitter:@johncasarrubias> <github:@mean-expert-official>
* @module SocketBrowser
* @license MIT
* @description
* This module handle socket connections for web browsers, it will be DI Swapped
* depending on the platform environment.
* This module will be generated when the -d ng2web flag is set
**/
var SocketBrowser = (function () {
    function SocketBrowser() {
    }
    /**
     * @method connect
     * @param {string} url URL path to connect with the server.
     * @param {any} options Any socket.io v1 =< valid options
     * @return {any} Not currently a socket.io-client for web Typings implemented.
     * @description
     * This method will return a valid socket connection.
     **/
    SocketBrowser.prototype.connect = function (url, options) {
        return __WEBPACK_IMPORTED_MODULE_0_socket_io_client__(url, options);
    };
    return SocketBrowser;
}());

//# sourceMappingURL=socket.browser.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/sockets/socket.connections.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SocketConnection; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__socket_driver__ = __webpack_require__("../../../../../src/app/sdk/sockets/socket.driver.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Subject__ = __webpack_require__("../../../../rxjs/Subject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Subject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_share__ = __webpack_require__("../../../../rxjs/add/operator/share.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_share___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_share__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__lb_config__ = __webpack_require__("../../../../../src/app/sdk/lb.config.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
/* tslint:disable */





/**
* @author Jonathan Casarrubias <twitter:@johncasarrubias> <github:@mean-expert-official>
* @module SocketConnection
* @license MIT
* @description
* This module handle socket connections and return singleton instances for each
* connection, it will use the SDK Socket Driver Available currently supporting
* Angular 2 for web, NativeScript 2 and Angular Universal.
**/
var SocketConnection = (function () {
    /**
     * @method constructor
     * @param {SocketDriver} driver Socket IO Driver
     * @param {NgZone} zone Angular 2 Zone
     * @description
     * The constructor will set references for the shared hot observables from
     * the class subjects. Then it will subscribe each of these observables
     * that will create a channel that later will be shared between subscribers.
     **/
    function SocketConnection(driver, zone) {
        this.driver = driver;
        this.zone = zone;
        this.subjects = {
            onConnect: new __WEBPACK_IMPORTED_MODULE_2_rxjs_Subject__["Subject"](),
            onDisconnect: new __WEBPACK_IMPORTED_MODULE_2_rxjs_Subject__["Subject"](),
            onAuthenticated: new __WEBPACK_IMPORTED_MODULE_2_rxjs_Subject__["Subject"](),
            onUnAuthorized: new __WEBPACK_IMPORTED_MODULE_2_rxjs_Subject__["Subject"]()
        };
        this.sharedObservables = {};
        this.authenticated = false;
        this.sharedObservables = {
            sharedOnConnect: this.subjects.onConnect.asObservable().share(),
            sharedOnDisconnect: this.subjects.onDisconnect.asObservable().share(),
            sharedOnAuthenticated: this.subjects.onAuthenticated.asObservable().share(),
            sharedOnUnAuthorized: this.subjects.onUnAuthorized.asObservable().share()
        };
        // This is needed to create the first channel, subsequents will share the connection
        // We are using Hot Observables to avoid duplicating connection status events.
        this.sharedObservables.sharedOnConnect.subscribe();
        this.sharedObservables.sharedOnDisconnect.subscribe();
        this.sharedObservables.sharedOnAuthenticated.subscribe();
        this.sharedObservables.sharedOnUnAuthorized.subscribe();
    }
    /**
     * @method connect
     * @param {AccessToken} token AccesToken instance
     * @return {void}
     * @description
     * This method will create a new socket connection when not previously established.
     * If there is a broken connection it will re-connect.
     **/
    SocketConnection.prototype.connect = function (token) {
        var _this = this;
        if (token === void 0) { token = null; }
        if (!this.socket) {
            console.info('Creating a new connection with: ', __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath());
            // Create new socket connection
            this.socket = this.driver.connect(__WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].getPath(), {
                log: false,
                secure: __WEBPACK_IMPORTED_MODULE_4__lb_config__["a" /* LoopBackConfig */].isSecureWebSocketsSet(),
                forceNew: true,
                forceWebsockets: true,
                transports: ['websocket']
            });
            // Listen for connection
            this.on('connect', function () {
                _this.subjects.onConnect.next('connected');
                // Authenticate or start heartbeat now    
                _this.emit('authentication', token);
            });
            // Listen for authentication
            this.on('authenticated', function () {
                _this.authenticated = true;
                _this.subjects.onAuthenticated.next();
                _this.heartbeater();
            });
            // Listen for authentication
            this.on('unauthorized', function (err) {
                _this.authenticated = false;
                _this.subjects.onUnAuthorized.next(err);
            });
            // Listen for disconnections
            this.on('disconnect', function (status) { return _this.subjects.onDisconnect.next(status); });
        }
        else if (this.socket && !this.socket.connected) {
            if (typeof this.socket.off === 'function') {
                this.socket.off();
            }
            if (typeof this.socket.destroy === 'function') {
                this.socket.destroy();
            }
            delete this.socket;
            this.connect(token);
        }
    };
    /**
     * @method isConnected
     * @return {boolean}
     * @description
     * This method will return true or false depending on established connections
     **/
    SocketConnection.prototype.isConnected = function () {
        return (this.socket && this.socket.connected);
    };
    /**
     * @method on
     * @param {string} event Event name
     * @param {Function} handler Event listener handler
     * @return {void}
     * @description
     * This method listen for server events from the current WebSocket connection.
     * This method is a facade that will wrap the original "on" method and run it
     * within the Angular Zone to avoid update issues.
     **/
    SocketConnection.prototype.on = function (event, handler) {
        var _this = this;
        this.socket.on(event, function (data) { return _this.zone.run(function () { return handler(data); }); });
    };
    /**
     * @method emit
     * @param {string} event Event name
     * @param {any=} data Any type of data
     * @return {void}
     * @description
     * This method will send any type of data to the server according the event set.
     **/
    SocketConnection.prototype.emit = function (event, data) {
        if (data) {
            this.socket.emit(event, data);
        }
        else {
            this.socket.emit(event);
        }
    };
    /**
     * @method removeListener
     * @param {string} event Event name
     * @param {Function} handler Event listener handler
     * @return {void}
     * @description
     * This method will wrap the original "on" method and run it within the Angular Zone
     * Note: off is being used since the nativescript socket io client does not provide
     * removeListener method, but only provides with off which is provided in any platform.
     **/
    SocketConnection.prototype.removeListener = function (event, handler) {
        if (typeof this.socket.off === 'function') {
            this.socket.off(event, handler);
        }
    };
    /**
     * @method removeAllListeners
     * @param {string} event Event name
     * @param {Function} handler Event listener handler
     * @return {void}
     * @description
     * This method will wrap the original "on" method and run it within the Angular Zone
     * Note: off is being used since the nativescript socket io client does not provide
     * removeListener method, but only provides with off which is provided in any platform.
     **/
    SocketConnection.prototype.removeAllListeners = function (event) {
        if (typeof this.socket.removeAllListeners === 'function') {
            this.socket.removeAllListeners(event);
        }
    };
    /**
     * @method disconnect
     * @return {void}
     * @description
     * This will disconnect the client from the server
     **/
    SocketConnection.prototype.disconnect = function () {
        this.socket.disconnect();
    };
    /**
     * @method heartbeater
     * @return {void}
     * @description
     * This will keep the connection as active, even when users are not sending
     * data, this avoids disconnection because of a connection not being used.
     **/
    SocketConnection.prototype.heartbeater = function () {
        var _this = this;
        var heartbeater = setInterval(function () {
            if (_this.isConnected()) {
                _this.socket.emit('lb-ping');
            }
            else {
                _this.socket.removeAllListeners('lb-pong');
                clearInterval(heartbeater);
            }
        }, 15000);
        this.socket.on('lb-pong', function (data) { return console.info('Heartbeat: ', data); });
    };
    return SocketConnection;
}());
SocketConnection = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __param(0, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_1__socket_driver__["a" /* SocketDriver */])),
    __param(1, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_0__angular_core__["R" /* NgZone */])),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__socket_driver__["a" /* SocketDriver */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__socket_driver__["a" /* SocketDriver */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["R" /* NgZone */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["R" /* NgZone */]) === "function" && _b || Object])
], SocketConnection);

var _a, _b;
//# sourceMappingURL=socket.connections.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/sockets/socket.driver.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SocketDriver; });
/* tslint:disable */
/**
 * @module SocketDriver
 * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
 * @license MIT
 * @description
 * The SocketDriver class is used for dependency injection swapping.
 * It will be provided using factory method from different sources.
 **/
var SocketDriver = (function () {
    function SocketDriver() {
    }
    SocketDriver.prototype.connect = function (url, options) { };
    return SocketDriver;
}());

//# sourceMappingURL=socket.driver.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/storage/cookie.browser.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CookieBrowser; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
/* tslint:disable */

/**
* @author Jonathan Casarrubias <twitter:@johncasarrubias> <github:@mean-expert-official>
* @module CookieBrowser
* @license MIT
* @description
* This module handle cookies, it will be provided using DI Swapping according the
* SDK Socket Driver Available currently supporting Angular 2 for web and NativeScript 2.
**/
var CookieBrowser = (function () {
    function CookieBrowser() {
        /**
         * @type {CookieInterface}
         **/
        this.cookies = {};
    }
    /**
     * @method get
     * @param {string} key Cookie key name
     * @return {any}
     * @description
     * The getter will return any type of data persisted in cookies.
     **/
    CookieBrowser.prototype.get = function (key) {
        if (!this.cookies[key]) {
            var cookie = window.document
                .cookie.split('; ')
                .filter(function (item) { return item.split('=')[0] === key; }).pop();
            if (!cookie) {
                return null;
            }
            this.cookies[key] = this.parse(cookie.split('=').slice(1).join('='));
        }
        return this.cookies[key];
    };
    /**
     * @method set
     * @param {string} key Cookie key name
     * @param {any} value Any value
     * @param {Date=} expires The date of expiration (Optional)
     * @return {void}
     * @description
     * The setter will return any type of data persisted in cookies.
     **/
    CookieBrowser.prototype.set = function (key, value, expires) {
        this.cookies[key] = value;
        var cookie = key + "=" + value + "; path=/" + (expires ? "; expires=" + expires.toUTCString() : '');
        window.document.cookie = cookie;
    };
    /**
     * @method remove
     * @param {string} key Cookie key name
     * @return {void}
     * @description
     * This method will remove a cookie from the client.
     **/
    CookieBrowser.prototype.remove = function (key) {
        document.cookie = key + '=; path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        delete this.cookies[key];
    };
    /**
     * @method parse
     * @param {any} value Input data expected to be JSON
     * @return {void}
     * @description
     * This method will parse the string as JSON if possible, otherwise will
     * return the value itself.
     **/
    CookieBrowser.prototype.parse = function (value) {
        try {
            return JSON.parse(value);
        }
        catch (e) {
            return value;
        }
    };
    return CookieBrowser;
}());
CookieBrowser = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])()
], CookieBrowser);

//# sourceMappingURL=cookie.browser.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/storage/storage.browser.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StorageBrowser; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
/* tslint:disable */

/**
* @author Jonathan Casarrubias <twitter:@johncasarrubias> <github:@mean-expert-official>
* @module StorageBrowser
* @license MIT
* @description
* This module handle localStorage, it will be provided using DI Swapping according the
* SDK Socket Driver Available currently supporting Angular 2 for web and NativeScript 2.
**/
var StorageBrowser = (function () {
    function StorageBrowser() {
    }
    /**
     * @method get
     * @param {string} key Storage key name
     * @return {any}
     * @description
     * The getter will return any type of data persisted in localStorage.
     **/
    StorageBrowser.prototype.get = function (key) {
        var data = localStorage.getItem(key);
        return this.parse(data);
    };
    /**
     * @method set
     * @param {string} key Storage key name
     * @param {any} value Any value
     * @return {void}
     * @description
     * The setter will return any type of data persisted in localStorage.
     **/
    StorageBrowser.prototype.set = function (key, value, expires) {
        localStorage.setItem(key, typeof value === 'object' ? JSON.stringify(value) : value);
    };
    /**
     * @method remove
     * @param {string} key Storage key name
     * @return {void}
     * @description
     * This method will remove a localStorage item from the client.
     **/
    StorageBrowser.prototype.remove = function (key) {
        if (localStorage[key]) {
            localStorage.removeItem(key);
        }
        else {
            console.log('Trying to remove unexisting key: ', key);
        }
    };
    /**
     * @method parse
     * @param {any} value Input data expected to be JSON
     * @return {void}
     * @description
     * This method will parse the string as JSON if possible, otherwise will
     * return the value itself.
     **/
    StorageBrowser.prototype.parse = function (value) {
        try {
            return JSON.parse(value);
        }
        catch (e) {
            return value;
        }
    };
    return StorageBrowser;
}());
StorageBrowser = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])()
], StorageBrowser);

//# sourceMappingURL=storage.browser.js.map

/***/ }),

/***/ "../../../../../src/app/sdk/storage/storage.swaps.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export BaseStorage */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InternalStorage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return SDKStorage; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/* tslint:disable */
/**
 * @module Storage
 * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
 * @license MIT
 * @description
 * The InternalStorage class is used for dependency injection swapping.
 * It will be provided using factory method from different sources.
 **/
var BaseStorage = (function () {
    function BaseStorage() {
    }
    /**
     * @method get
     * @param {string} key Storage key name
     * @return {any}
     * @description
     * The getter will return any type of data persisted in storage.
     **/
    BaseStorage.prototype.get = function (key) { };
    /**
     * @method set
     * @param {string} key Storage key name
     * @param {any} value Any value
     * @return {void}
     * @description
     * The setter will return any type of data persisted in localStorage.
     **/
    BaseStorage.prototype.set = function (key, value, expires) { };
    /**
     * @method remove
     * @param {string} key Storage key name
     * @return {void}
     * @description
     * This method will remove a localStorage item from the client.
     **/
    BaseStorage.prototype.remove = function (key) { };
    return BaseStorage;
}());

/**
 * @module InternalStorage
 * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
 * @license MIT
 * @description
 * The InternalStorage class is used for dependency injection swapping.
 * It will be provided using factory method from different sources.
 * This is mainly required because Angular Universal integration.
 * It does inject a CookieStorage instead of LocalStorage.
 **/
var InternalStorage = (function (_super) {
    __extends(InternalStorage, _super);
    function InternalStorage() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return InternalStorage;
}(BaseStorage));

/**
 * @module SDKStorage
 * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
 * @license MIT
 * @description
 * The SDKStorage class is used for dependency injection swapping.
 * It will be provided using factory method according the right environment.
 * This is created for public usage, to allow persisting custom data
 * Into the local storage API.
 **/
var SDKStorage = (function (_super) {
    __extends(SDKStorage, _super);
    function SDKStorage() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return SDKStorage;
}(BaseStorage));

//# sourceMappingURL=storage.swaps.js.map

/***/ }),

/***/ "../../../../../src/app/services/default-params/default-params.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DefaultParamsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DefaultParamsService = (function () {
    function DefaultParamsService() {
    }
    DefaultParamsService.prototype.defaultUserImg = function () {
        return "assets/images/admin.jpg";
    };
    return DefaultParamsService;
}());
DefaultParamsService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __metadata("design:paramtypes", [])
], DefaultParamsService);

//# sourceMappingURL=default-params.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/direction/direction.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DirectionService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__agm_core__ = __webpack_require__("../../../../@agm/core/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DirectionService = (function () {
    function DirectionService(gmapsApi, mapsAPILoader) {
        var _this = this;
        this.gmapsApi = gmapsApi;
        this.mapsAPILoader = mapsAPILoader;
        this.mapsAPILoader.load().then(function () {
            _this.directionsService = new google.maps.DirectionsService;
        });
    }
    DirectionService.prototype.getDirection = function (source, destination, cb) {
        // this.gmapsApi.getNativeMap().then(map => {
        this.directionsService.route({
            origin: source,
            destination: destination,
            waypoints: [],
            optimizeWaypoints: true,
            travelMode: 'DRIVING',
            provideRouteAlternatives: true
        }, cb);
    };
    return DirectionService;
}());
DirectionService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__agm_core__["b" /* GoogleMapsAPIWrapper */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__agm_core__["b" /* GoogleMapsAPIWrapper */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__agm_core__["c" /* MapsAPILoader */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__agm_core__["c" /* MapsAPILoader */]) === "function" && _b || Object])
], DirectionService);

var _a, _b;
//# sourceMappingURL=direction.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/multipart/multipart.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MultipartService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sdk_index__ = __webpack_require__("../../../../../src/app/sdk/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__ = __webpack_require__("../../../../rxjs/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MultipartService = (function () {
    function MultipartService(http, auth) {
        this.http = http;
        this.auth = auth;
        this.base = __WEBPACK_IMPORTED_MODULE_2__sdk_index__["c" /* LoopBackConfig */].getPath();
        this.urls = {
            signup: this.base + "/api/People/adminCreateUser"
        };
    }
    MultipartService.prototype.request = function (url, data) {
        return this.http.post(url, this.getFormObj(data), this.getOption()).map(function (res) { return (res.text() != "" ? res.json() : {}); }).catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    MultipartService.prototype.getFormObj = function (obj) {
        var fd = new FormData();
        if (obj.data && typeof obj.data == 'object') {
            if (obj.data.extra) {
                delete obj.data.extra;
            }
            if (obj.data.address) {
                delete obj.data.address;
            }
            fd.append('data', JSON.stringify(obj.data));
        }
        for (var x in obj.files) {
            for (var y in obj.files[x]) {
                fd.append(x, obj.files[x][y]);
            }
        }
        return fd;
    };
    MultipartService.prototype.getOption = function () {
        var header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        header.append('Authorization', this.auth.getAccessTokenId() || "");
        var opts = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* RequestOptions */]();
        opts.headers = header;
        return opts;
    };
    MultipartService.prototype.signupApi = function (data) {
        return this.request(this.urls.signup, data);
    };
    return MultipartService;
}());
MultipartService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__sdk_index__["b" /* LoopBackAuth */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__sdk_index__["b" /* LoopBackAuth */]) === "function" && _b || Object])
], MultipartService);

var _a, _b;
//# sourceMappingURL=multipart.service.js.map

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_23" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ }),

/***/ 1:
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map