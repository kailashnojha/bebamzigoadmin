import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './../component/dashboard/dashboard.component';
import { LoginComponent } from './../component/login/login.component';
import { VehicleListComponent } from './../component/vehicle-list/vehicle-list.component';
import { VehicleDetailsComponent } from './../component/vehicle-details/vehicle-details.component';
import { UserListComponent } from './../component/user-list/user-list.component';
import { UserDetailsComponent } from './../component/user-details/user-details.component';
import { JobListComponent } from './../component/job-list/job-list.component';
import { AddJobComponent } from './../component/add-job/add-job.component';
import { AddUserComponent } from './../component/add-user/add-user.component';


const routes: Routes = [
	{ path: '', redirectTo: '/', pathMatch: 'full' },
	{ path: '', component: DashboardComponent },
	{ path: 'login', component: LoginComponent },
	{ path: 'vehicle-list/:status', component: VehicleListComponent },
	{ path: 'vehicle-details/:vehicleId', component: VehicleDetailsComponent },	
  { path: 'user-list/:type', component: UserListComponent },
  { path: 'user-details/:peopleId', component: UserDetailsComponent },    
  { path: 'job-list/:status', component: JobListComponent },    
  { path: 'add-job/:type', component: AddJobComponent },    
  { path: 'add-user/:type', component: AddUserComponent },      
]; 

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [ RouterModule ],
  declarations: []
})
export class RoutingModule { }
