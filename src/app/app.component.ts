import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router'
import { LoopBackConfig, LoopBackAuth, PeopleApi,JobApi,NotificationApi } from './sdk/index';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  	isLoginPage:boolean = true;
	userAcessToken:string;
	constructor(private router: Router, public auth: LoopBackAuth, private peopleApi:PeopleApi){


		
		LoopBackConfig.setBaseURL('https://api.mybebamzigo.com');
		// LoopBackConfig.setBaseURL('http://localhost:3000');
		LoopBackConfig.setApiVersion('api'); 
		
    
	}	
	
	ngOnInit() {
		
		this.router.events.subscribe((evt) => {
			if (!(evt instanceof NavigationEnd)) {
				return;
			}
			this.userAcessToken = this.auth.getAccessTokenId();
			if(evt.url == "/login"){
				this.isLoginPage = true;
				if(!!this.userAcessToken){
					// console.log("come in this part")
					this.isLoginPage = false;
					this.router.navigate(['/']);
				}
			}	
			else{
				// console.log(evt.url)
				this.isLoginPage = false
				if(!this.userAcessToken)
					this.router.navigate(['/login']);
			}
			// console.log(this.isLoginPage);
			
				
			
		})	
		
	}	


	logout(){
		this.peopleApi.logout().subscribe((success)=>{
			this.router.navigate(['/login']);
		},(error)=>{
			if(error.statusCode == 401){
				this.router.navigate(['/login']);
			}
		})
	}

}
