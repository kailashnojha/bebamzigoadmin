// --------------- Modules ------------------------------
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RoutingModule } from './routing/routing.module';
import { SDKBrowserModule } from './sdk/index';
import { FormsModule } from '@angular/forms';
import { AgmCoreModule, GoogleMapsAPIWrapper } from '@agm/core';

// --------------- Components ------------------------------
import { AppComponent } from './app.component';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { LoginComponent } from './component/login/login.component';
import { VehicleListComponent } from './component/vehicle-list/vehicle-list.component';
import { VehicleDetailsComponent } from './component/vehicle-details/vehicle-details.component';
import { UserListComponent } from './component/user-list/user-list.component';
import { UserDetailsComponent } from './component/user-details/user-details.component';
import { JobListComponent } from './component/job-list/job-list.component';
import { AddJobComponent } from './component/add-job/add-job.component';
import { AddUserComponent } from './component/add-user/add-user.component';

// --------------- Provider ------------------------------
import { DefaultParamsService } from './services/default-params/default-params.service';
import { MultipartService } from './services/multipart/multipart.service';
import { DirectionService } from './services/direction/direction.service';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    VehicleListComponent,
    VehicleDetailsComponent,
    UserListComponent,
    UserDetailsComponent,
    JobListComponent,
    AddJobComponent,
    AddUserComponent
  ],
  imports: [
    BrowserModule,
    RoutingModule,
    SDKBrowserModule.forRoot(),
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDll9PQUPlUyzG5oknwZ1YnZv83sGY51FQ',
      libraries:["places"]
    })
  ],
  providers: [
    DefaultParamsService,
    MultipartService,
    GoogleMapsAPIWrapper,
    DirectionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
