/* tslint:disable */

declare var Object: any;
export interface PeopleInterface {
  "realm"?: string;
  "username"?: string;
  "email": string;
  "companyName"?: string;
  "emailVerified"?: boolean;
  "licenceIds"?: Array<any>;
  "governmentIds"?: Array<any>;
  "firebaseTokens"?: Array<any>;
  "createdAt"?: Date;
  "updatedAt"?: Date;
  "id"?: any;
  "password"?: string;
  accessTokens?: any[];
  vehicles?: any[];
  requestfordrivers?: any[];
  identities?: any[];
  credentials?: any[];
}

export class People implements PeopleInterface {
  "realm": string;
  "username": string;
  "email": string;
  "companyName": string;
  "emailVerified": boolean;
  "licenceIds": Array<any>;
  "governmentIds": Array<any>;
  "firebaseTokens": Array<any>;
  "createdAt": Date;
  "updatedAt": Date;
  "id": any;
  "password": string;
  accessTokens: any[];
  vehicles: any[];
  requestfordrivers: any[];
  identities: any[];
  credentials: any[];
  constructor(data?: PeopleInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `People`.
   */
  public static getModelName() {
    return "People";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of People for dynamic purposes.
  **/
  public static factory(data: PeopleInterface): People{
    return new People(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'People',
      plural: 'People',
      path: 'People',
      idName: 'id',
      properties: {
        "realm": {
          name: 'realm',
          type: 'string'
        },
        "username": {
          name: 'username',
          type: 'string'
        },
        "email": {
          name: 'email',
          type: 'string'
        },
        "companyName": {
          name: 'companyName',
          type: 'string',
          default: ''
        },
        "emailVerified": {
          name: 'emailVerified',
          type: 'boolean'
        },
        "licenceIds": {
          name: 'licenceIds',
          type: 'Array&lt;any&gt;'
        },
        "governmentIds": {
          name: 'governmentIds',
          type: 'Array&lt;any&gt;'
        },
        "firebaseTokens": {
          name: 'firebaseTokens',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
        "createdAt": {
          name: 'createdAt',
          type: 'Date'
        },
        "updatedAt": {
          name: 'updatedAt',
          type: 'Date'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
        "password": {
          name: 'password',
          type: 'string'
        },
      },
      relations: {
        accessTokens: {
          name: 'accessTokens',
          type: 'any[]',
          model: '',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'userId'
        },
        vehicles: {
          name: 'vehicles',
          type: 'any[]',
          model: '',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'peopleId'
        },
        requestfordrivers: {
          name: 'requestfordrivers',
          type: 'any[]',
          model: '',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'peopleId'
        },
        identities: {
          name: 'identities',
          type: 'any[]',
          model: '',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'peopleId'
        },
        credentials: {
          name: 'credentials',
          type: 'any[]',
          model: '',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'peopleId'
        },
      }
    }
  }
}
