/* tslint:disable */

declare var Object: any;
export interface JobInterface {
  "title"?: string;
  "weight"?: string;
  "source"?: any;
  "destination"?: any;
  "insurance"?: boolean;
  "valueOfLoad"?: number;
  "insPremium"?: number;
  "budget"?: number;
  "netPrice"?: number;
  "id"?: any;
  "peopleId"?: any;
  people?: any;
  jobApplies?: any[];
}

export class Job implements JobInterface {
  "title": string;
  "weight": string;
  "source": any;
  "destination": any;
  "insurance": boolean;
  "valueOfLoad": number;
  "insPremium": number;
  "budget": number;
  "netPrice": number;
  "id": any;
  "peopleId": any;
  people: any;
  jobApplies: any[];
  constructor(data?: JobInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Job`.
   */
  public static getModelName() {
    return "Job";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Job for dynamic purposes.
  **/
  public static factory(data: JobInterface): Job{
    return new Job(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Job',
      plural: 'Jobs',
      path: 'Jobs',
      idName: 'id',
      properties: {
        "title": {
          name: 'title',
          type: 'string'
        },
        "weight": {
          name: 'weight',
          type: 'string'
        },
        "source": {
          name: 'source',
          type: 'any'
        },
        "destination": {
          name: 'destination',
          type: 'any'
        },
        "insurance": {
          name: 'insurance',
          type: 'boolean'
        },
        "valueOfLoad": {
          name: 'valueOfLoad',
          type: 'number'
        },
        "insPremium": {
          name: 'insPremium',
          type: 'number'
        },
        "budget": {
          name: 'budget',
          type: 'number'
        },
        "netPrice": {
          name: 'netPrice',
          type: 'number'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
        "peopleId": {
          name: 'peopleId',
          type: 'any'
        },
      },
      relations: {
        people: {
          name: 'people',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'peopleId',
          keyTo: 'id'
        },
        jobApplies: {
          name: 'jobApplies',
          type: 'any[]',
          model: '',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'jobId'
        },
      }
    }
  }
}
