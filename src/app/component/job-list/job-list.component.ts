import { Component, OnInit } from '@angular/core';
import { PeopleApi, JobApi } from './../../sdk/index';
import { ActivatedRoute, Router } from "@angular/router";
import { DefaultParamsService } from "./../../services/default-params/default-params.service";

@Component({
  selector: 'app-job-list',
  templateUrl: './job-list.component.html',
  styleUrls: ['./job-list.component.css']
})
export class JobListComponent implements OnInit {
	
  params:any = {};	
  jobs:Array<any>= [];	
  approveOps:any={};

  constructor(private dPService:DefaultParamsService,private jobApi:JobApi, private route : ActivatedRoute, private router:Router) { }

  ngOnInit() {
  	this.route.params.subscribe( params => {
		this.params = params;
		console.log(this.params);
		this.getJobs();  	
	})
  }

  getType(type){
  	
  	if(type == "rejected")
  	   return "reject"
  	else 
  		return type;
  	
  }

  getJobs(){
  	this.jobApi.getJobsByAdmin(this.getType(this.params.status)).subscribe((success)=>{
  		this.jobs = success.success.data;
      console.log(this.jobs)
  	},(error)=>{

  	})
  }

  openModel(status,jobId){
    this.approveOps.status = status;
    this.approveOps.jobId = jobId;
    console.log(this.approveOps)  
    $('#jobApproval').modal('show');
  } 


  verify(){
    this.jobApi.approveByAdmin(this.approveOps.jobId,this.approveOps.status).subscribe((success)=>{
      console.log(success);
      this.getJobs();
      $('#jobApproval').modal('hide');
    },(error)=>{

    })
  }

}
