import { Component, OnInit } from '@angular/core';
import { DefaultParamsService } from "./../../services/default-params/default-params.service";
import {ActivatedRoute, Router} from "@angular/router";
import { PeopleApi, LoopBackConfig } from './../../sdk/index';
import { MultipartService } from '../../services/multipart/multipart.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
    defaultUserImg:string;
    files:any = {};
    user:any = {};  
    params : any = {};
    baseURL:string;	
    errSuccObj :any = {};
    userErr : any = {};
    isLogin : boolean = false;
	re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    constructor(private multipartApi:MultipartService, private dPService:DefaultParamsService,private peopleApi:PeopleApi, private route : ActivatedRoute, private router:Router) { 
    	this.defaultUserImg = this.dPService.defaultUserImg();
    }

	ngOnInit() {
		this.isLogin = true;
    	this.route.params.subscribe( params => {
  			this.params = params;
  			this.user.realm = this.params.type;
    	});
	}

	signup(signupForm){
		let _self = this;
		if( signupForm.valid && this.user.confirmPassword == this.user.password ){
			this.multipartApi.signupApi({data:this.user,files:this.files}).subscribe((success:any)=>{
				this.errSuccObj.error = success.success.msg;
				this.errSuccObj.error = undefined;
			},(error:any)=>{
				this.errSuccObj.error = error.message;
				this.errSuccObj.success = undefined;
			});
		}   
	}

	checkPassword(){
		if( this.user.confirmPassword != undefined && this.user.confirmPassword.length > 0){
			if(this.user.password !== this.user.confirmPassword){
				this.userErr.passMatch = true;
			}
			if(this.user.password === this.user.confirmPassword){
				this.userErr.passMatch = false;
				this.userErr.password = false;
				this.userErr.confirmPassword = false;
			}
		}
		if((this.user.confirmPassword != undefined && this.user.password != undefined) && (this.user.confirmPassword.length == 0 || this.user.password.length == 0)){
			this.userErr.passMatch = false;
		}
	}
	
	validateEmail(){
		if(this.user.email.length > 0){
			var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			
			if(re.test(this.user.email)){
				this.userErr.emailValid = false;
			}
			else{
				this.userErr.emailValid = true;
			}
		}
		else{
			this.userErr.emailValid = false;
		}
	}
}
