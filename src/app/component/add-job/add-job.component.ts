import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import {} from '@types/googlemaps';
import { MapsAPILoader, GoogleMapsAPIWrapper } from '@agm/core';
import {VehicleTypeApi,JobApi,LoopBackConfig} from "./../../sdk";
import {ActivatedRoute, Router} from "@angular/router";
import { DirectionService } from './../../services/direction/direction.service';
@Component({
  selector: 'app-add-job',
  templateUrl: './add-job.component.html',
  styleUrls: ['./add-job.component.css']
})
export class AddJobComponent implements OnInit {

    job:any = {};
    vehicleTypes:any = [];
    selectedVehicleTypes:any = [];
    baseURL:string;
    params:any = {};
    showFormValue:string = "firstForm";
    routes:any = [];
    isFormValidate :any = {
    	firstForm:true,
    	firstFormSubmit:false,
    	secondForm:true,
    	secondFormSubmit:false,
    	thirdForm:true,
    	thirdFormSubmit:false
    }

    firstFormError: any = {
    	pickLocation:false,
    	dropLocation:false,
    	startDate:false,
    	startTime:false,
    	endDate:false,
    	endTime:false,
    	typeOfVehicle:false
    }

    secondFormError:any = {
    	title:false,
    	description:false,
    	selectedRoute:false,
    }

    thirdformError:any = {

    }

    selectedRoute:any={};


    constructor(private directionService:DirectionService, private ref:ChangeDetectorRef,private router : Router, private route: ActivatedRoute, private jobApi:JobApi, private vehicleTypeApi:VehicleTypeApi, public gMaps: GoogleMapsAPIWrapper, private mapsAPILoader:MapsAPILoader) { 

    }

    ngOnInit() {

    	this.route.params.subscribe( params => {
	  		this.params = params;
	  		this.job.typeOfJob = this.params.type =="on-demand" ? "on demand" : "standard rate";	
	 		this.getVehicleType(this.job.typeOfJob);
	 		this.job.requiredType = "Vehicle";
	  	});

    	this.baseURL = LoopBackConfig.getPath();

    	this.mapsAPILoader.load().then(()=>{
			this.initializePickupMap();
			this.initializeDropMap();
		});

    	this.initializeDatepicker();
    	this.initializeClockpicker();
    	this.initializeDatepicker1();
    	this.initializeClockpicker1();
    }

    back(){

    	if(this.showFormValue === "secondForm"){
    		this.showFormValue = "firstForm";
    	}else{
    		this.showFormValue = "secondForm";
    	}
    	this.ref.detectChanges();
    }

    selectVehicle(typeOfVehicle){
    	this.job.typeOfVehicle = typeOfVehicle;
    	this.firstFormValidator();
    }

    getVehicleType(typeOfJob){
    	this.vehicleTypeApi.getVehicleType(undefined,typeOfJob).subscribe((vehicleTypes)=>{
    		
    		this.vehicleTypes = vehicleTypes.success.data;
    		if(this.job.requiredType == "Vehicle")
    			this.selectedVehicleTypes = this.vehicleTypes.vehicle;
    		else
    			this.selectedVehicleTypes = this.vehicleTypes.machinery;	
    		this.selectVehicle(undefined);
    	},(error)=>{

    	})
    }

    changeRequireType(){
    	if(this.job.requiredType == "Vehicle")
			this.selectedVehicleTypes = this.vehicleTypes.vehicle;
		else
			this.selectedVehicleTypes = this.vehicleTypes.machinery;	

		this.selectVehicle(undefined);    		
    }

    firstForm(){
		this.showFormValue = "firstForm";
    }
    
	firstFormValidator(){

		if(!this.isFormValidate.firstFormSubmit)
			return;

		this.isFormValidate.firstForm = true;

		if(typeof this.job.source == 'object' && this.job.source.address != "" && this.job.source.location.lat!= undefined && this.job.source.location.lng!= undefined ){
			this.firstFormError.pickLocation = false;
		}else{
			console.log("come in second part");
			this.firstFormError.pickLocation = true;
			this.isFormValidate.firstForm = false;
		}

		if(this.job.requiredType == "Machinery" || (typeof this.job.destination == 'object' && this.job.destination.address != "" && this.job.destination.location.lat!= undefined && this.job.destination.location.lng!= undefined )){
			this.firstFormError.dropLocation = false;
		}else{
			this.firstFormError.dropLocation = true;
			this.isFormValidate.firstForm = false;
		}	

		if(this.job.time != undefined && this.job.time != "" && this.job.time != null){
			this.firstFormError.startTime = false;
		}else{
			this.firstFormError.startTime = true;
			this.isFormValidate.firstForm = false;
		}	
		
		if(this.job.date != undefined && this.job.date != "" && this.job.date != null){
			this.firstFormError.date = false;
		}else{
			this.firstFormError.date = true;
			this.isFormValidate.firstForm = false;
		}	

		if(this.job.requiredType == "Vehicle" || (this.job.endDate != undefined && this.job.endDate != "" && this.job.endDate != null)){
			this.firstFormError.endDate = false;
		}else{
			this.firstFormError.endDate = true;
			this.isFormValidate.firstForm = false;
		}	
		
		if(this.job.requiredType == "Vehicle" || (this.job.endTime != undefined && this.job.endTime != "" && this.job.endTime != null)){
			this.firstFormError.endTime = false;
		}else{
			this.firstFormError.endTime = true;
			this.isFormValidate.firstForm = false;
		}	

		if(this.job.typeOfVehicle != undefined && this.job.typeOfVehicle != "" && this.job.typeOfVehicle != null){
			this.firstFormError.typeOfVehicle = false;
		}else{
			this.firstFormError.typeOfVehicle = true;
			this.isFormValidate.firstForm = false;
		}
		this.ref.detectChanges();	
	}

	selectRoute(route){
		this.selectedRoute = route;
		console.log(this.selectedRoute);
	}

    secondForm(){
    	let _self = this;
    	this.isFormValidate.firstFormSubmit = true;
    	this.firstFormValidator()
    	
    	if(this.isFormValidate.firstForm){
	    	if(this.job.requiredType == "Vehicle"){
	    		this.getDirection(this.job.source.location,this.job.destination.location,function(error,routes){
	    			if(error) {

	    			}else{
	    				_self.routes = routes;
	    				_self.showFormValue = "secondForm";
	    				console.log(_self.routes);	
	    			}
	    			_self.ref.detectChanges();	
	    		});	
	    	}else{
	    		this.routes = [];
	    		this.showFormValue = "secondForm";
	    	}
    	}
    }

	secondFormValidator(){
		if(!this.isFormValidate.secondFormSubmit)
			return;

		this.isFormValidate.secondForm = true;

		if(this.job.source.title != "" && this.job.title!= undefined ){
			this.firstFormError.title = false;
		}else{
			this.firstFormError.title = true;
			this.isFormValidate.secondForm = false;
		}
	}

    thirdForm(){
    	this.showFormValue = "thirdForm";
    }

    initializePickupMap(){
		let options = {
			componentRestrictions: {country: 'ke'}
		};
		let pickupAutocomplete = new google.maps.places.Autocomplete(<HTMLInputElement>document.getElementById("pickupAutocomplete"),options);
		pickupAutocomplete.addListener('place_changed', () => {
			this.job.source = {};
			let place: google.maps.places.PlaceResult = pickupAutocomplete.getPlace();
			this.job.source = {
				address:place.formatted_address,
				location:{
					lat:place.geometry.location.lat(),
					lng:place.geometry.location.lng()
				}
			}
			this.firstFormValidator();
		});
    }

    initializeDropMap(){
    	let _self = this;
    	let options = {
			componentRestrictions: {country: 'ke'}
		};
		let pickupAutocomplete = new google.maps.places.Autocomplete(<HTMLInputElement>document.getElementById("dropAutocomplete"),options);
		pickupAutocomplete.addListener('place_changed', () => {
			this.job.destination = {};
			let place: google.maps.places.PlaceResult = pickupAutocomplete.getPlace();
			this.job.destination = {
				address:place.formatted_address,
				location:{
					lat:place.geometry.location.lat(),
					lng:place.geometry.location.lng()
				}
			}
			_self.firstFormValidator();
		});	
    }

	initializeDatepicker(){
		setTimeout(()=>{
			var date_input = $('#date'); //our date input has the name "date"
			let _self = this;
			
			date_input.datepicker({
				format: 'D d. M yyyy',
				todayHighlight: true,
				autoclose: true,
				numberOfMonths: 1,
				defaultDate: "+1d",
				startDate: '+0d',
				endDate: '+30d'
			}).on('changeDate',function(e){
				_self.job.date = e.date;		
        		_self.firstFormValidator();
    		});
		},0)
	}

	initializeClockpicker(){
		let _self = this;
		setTimeout(()=>{
			$('#time').clockpicker({
				'default': 'now',       // default time, 'now' or '13:14' e.g.
				fromnow: 0,          // set default time to * milliseconds from now (using with default = 'now')
				placement: 'bottom', // clock popover placement
				align: 'left',       // popover arrow align
				donetext: 'Done',    // done button text
				autoclose: true,    // auto close when minute is selected
				twelvehour: false, // change to 12 hour AM/PM clock from 24 hour
				vibrate: false,
				afterDone: function() {
                   _self.job.time = $("#time")[0].value;
                   _self.firstFormValidator();
                }
			});
		},0)	
	}	

	initializeDatepicker1(){
		setTimeout(()=>{
			var date_input = $('#endDate'); //our date input has the name "date"
			let _self = this;
		
			date_input.datepicker({
				format: 'D d. M yyyy',
				todayHighlight: true,
				autoclose: true,
				numberOfMonths: 1,
				defaultDate: "+1d",
				startDate: '+0d',
				endDate: '+30d'
			}).on('changeDate',function(e){
				_self.job.date = e.date;		
        		_self.firstFormValidator();
    		});
		},0)	
	}
	
	initializeClockpicker1(){
		let _self = this;
		setTimeout(()=>{
			$('#endTime').clockpicker({
				'default': 'now',       // default time, 'now' or '13:14' e.g.
				fromnow: 0,          // set default time to * milliseconds from now (using with default = 'now')
				placement: 'bottom', // clock popover placement
				align: 'left',       // popover arrow align
				donetext: 'Done',    // done button text
				autoclose: true,    // auto close when minute is selected
				twelvehour: false, // change to 12 hour AM/PM clock from 24 hour
				vibrate: false,
				afterDone: function() {
                   _self.job.endTime = $("#endTime")[0].value;
                   _self.firstFormValidator();
                }
			});
		},0)	
	}

	getDirection(source:any,destination:any,cb){
		let _self = this;
		this.directionService.getDirection(source,destination,(response: any, status: any) => {
			if (status === 'OK') {
				_self.routes = response.routes;
				console.log(_self.routes)
				console.log("routes : ",_self.routes);
				cb(null,response.routes)
				// _self.setPosition();
			}else{
				cb("error",null);
			}
		})
	}		

    /*firstFormError: any= {
    	pickLocation:false,
    	dropLocation:false,
    	startDate:false,
    	startTime:false,
    	endDate:false,
    	endTime:false,
    	typeOfVehicle:false
    }*/

	thirdFormValidator(){
	}


}
