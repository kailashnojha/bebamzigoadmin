import { Component, OnInit } from '@angular/core';
import { PeopleApi } from './../../sdk/index';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    userErr : any = {};	
    user:any={};	
    errSuccObj :any = {};

    constructor(private peopleApi:PeopleApi,private router:Router) { }

    ngOnInit() {
    }

	validate(){
		let error = false;
		if(!this.user.email){
			this.userErr.email = true;
			error = true;
		}
		else{
			this.userErr.email = false;
		}
		if(!this.user.password){
			this.userErr.password = true;
			error = true;
		}
		else{
			this.userErr.password = false;
		}
		return error;
	}
	
	validateEmail(){
		if(this.user.email.length > 0){
			var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			
			if(re.test(this.user.email)){
				this.userErr.emailValid = false;
				if(this.userErr.email){
					this.userErr.email = false;
				}
			}
			else{
				this.userErr.emailValid = true;
			}
		}
		else{
			this.userErr.emailValid = false;
		}
	}

	login(loginForm:any){
		// let firebaseToken = this.formatingFunctionsService.getToken();
		// this.user.firebaseToken = firebaseToken;
		let _self = this;
		if(!this.validate() && !this.userErr.emailValid){
			console.log(this.user);
			let rememberMe = false;
			this.peopleApi.login(this.user,'user',rememberMe).subscribe((success)=>{
				this.router.navigate(['/']);
			},(error)=>{
				this.errSuccObj.error = error.message;
			})    
		}
	}

}
