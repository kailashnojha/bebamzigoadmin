import { Component, OnInit } from '@angular/core';
import { VehicleApi, LoopBackConfig } from './../../sdk/index';
import { ActivatedRoute, Router } from "@angular/router";
import { DefaultParamsService } from "./../../services/default-params/default-params.service";


@Component({
  selector: 'app-vehicle-list',
  templateUrl: './vehicle-list.component.html',
  styleUrls: ['./vehicle-list.component.css']
})
export class VehicleListComponent implements OnInit {
  
  params:any={};
  vehicles:Array<any>=[];
  approveOps:any={};
  constructor(private dPService:DefaultParamsService,private vehicleApi:VehicleApi, private route : ActivatedRoute, private router:Router) { 

  }

  ngOnInit() {
  	this.route.params.subscribe( params => {
  		this.params = params;
      console.log(this.params)
  		this.getVehicles();
  	});
  }

  getVehicles(){
  	this.vehicleApi.getVehicleByAdmin(this.params.status).subscribe((success)=>{
  		this.vehicles = success.success.data;
  	},(error)=>{

  	})
  }

  openModel(status,vehicleId){
    this.approveOps.status = status;
    this.approveOps.vehicleId = vehicleId;
    console.log(this.approveOps)  
    $('#vehicleApproval').modal('show');
  } 


  verify(){
    this.vehicleApi.approveAdmin(this.approveOps.vehicleId,this.approveOps.status).subscribe((success)=>{
      console.log(success);
      this.getVehicles();
      $('#vehicleApproval').modal('hide');
    },(error)=>{

    })
  }



}
