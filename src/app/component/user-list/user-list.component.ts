import { Component, OnInit } from '@angular/core';
import { PeopleApi, LoopBackConfig } from './../../sdk/index';
import { ActivatedRoute, Router } from "@angular/router";
import { DefaultParamsService } from "./../../services/default-params/default-params.service";
declare var $ :any;

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  defaultUserImg:string;
  peopleList:Array<any> = [];	
  params : any = {};
  baseURL:string;	
  approveOps:any={};
  constructor(private dPService:DefaultParamsService,private peopleApi:PeopleApi, private route : ActivatedRoute, private router:Router) { 
    this.defaultUserImg = this.dPService.defaultUserImg();
  }

  ngOnInit() {
    this.baseURL = LoopBackConfig.getPath();
  	this.route.params.subscribe( params => {
  		this.params = params;
      console.log(this.params)
      this.getUserList();
  	});
  }

  getType(type){
  	let length = type.length;
  	return type.substring(0,length-1);
  }

  getUserList(){
		this.peopleApi.getPeoplesByAdmin(this.getType(this.params.type)).subscribe((success)=>{
			this.peopleList = success.success.data;
		},(error)=>{
		})
	}

  openModel(status,peopleId){
    this.approveOps.status = status;
    this.approveOps.peopleId = peopleId;
    console.log(this.approveOps)  
    $('#userApproval').modal('show');
  } 


  verify(){
    this.peopleApi.userApproval(this.approveOps.peopleId,this.approveOps.status).subscribe((success)=>{
      console.log(success);
      this.getUserList();
      $('#userApproval').modal('hide');
    },(error)=>{

    })
  }
  
}
