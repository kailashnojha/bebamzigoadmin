import { Component, OnInit } from '@angular/core';
import { PeopleApi, LoopBackConfig } from './../../sdk/index';
import {ActivatedRoute, Router} from "@angular/router";
import { DefaultParamsService } from "./../../services/default-params/default-params.service";

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {
  defaultUserImg:string;
  peopleInfo:any={};	
  params : any = {};
  baseURL:string;	
  constructor(private dPService:DefaultParamsService,private peopleApi:PeopleApi, private route : ActivatedRoute, private router:Router) { 
    this.defaultUserImg =this.dPService.defaultUserImg();
  }

  ngOnInit() {

  	this.route.params.subscribe( params => {
  		this.params = params;
  		console.log(this.params);
  	});

  	this.baseURL = LoopBackConfig.getPath();
  	this.getUserInfo();	

  }

  getUserInfo(){
  	this.peopleApi.getPeopleInfo(this.params.peopleId).subscribe((success)=>{
  		this.peopleInfo = success.success.data;
  	},(error)=>{

  	})
  }

}
