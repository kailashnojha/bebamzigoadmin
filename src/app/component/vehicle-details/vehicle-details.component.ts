import { Component, OnInit } from '@angular/core';
import { VehicleApi, LoopBackConfig } from './../../sdk/index';
import { ActivatedRoute, Router } from "@angular/router";
import { DefaultParamsService } from "./../../services/default-params/default-params.service";

@Component({
  selector: 'app-vehicle-details',
  templateUrl: './vehicle-details.component.html',
  styleUrls: ['./vehicle-details.component.css']
})
export class VehicleDetailsComponent implements OnInit {
  params:any;
  vehicleInfo : any= {};
  constructor(private dPService:DefaultParamsService,private vehicleApi:VehicleApi, private route : ActivatedRoute, private router:Router) { 

  }

  ngOnInit() {
  	this.route.params.subscribe( params => {
		this.params = params;
		console.log(this.params);
		this.getVehicleInfo();
	});
  }

  getVehicleInfo(){
  	this.vehicleApi.getVehicleById(this.params.vehicleId).subscribe((success)=>{
  		this.vehicleInfo = success.success.data;
  		console.log(this.vehicleInfo.owner);
  	},(error)=>{

  	})
  }

}
