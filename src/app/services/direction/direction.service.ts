import { Injectable } from '@angular/core';
import { GoogleMapsAPIWrapper, MapsAPILoader } from '@agm/core';
import {} from '@types/googlemaps';
@Injectable()
export class DirectionService {
	public directionsService:any;
	constructor(private gmapsApi: GoogleMapsAPIWrapper, private mapsAPILoader:MapsAPILoader) { 
		this.mapsAPILoader.load().then(()=>{
			this.directionsService = new google.maps.DirectionsService;
		})
	}

	getDirection(source,destination,cb) {
		// this.gmapsApi.getNativeMap().then(map => {
		this.directionsService.route({
			origin: source,
			destination: destination,
			waypoints: [],
			optimizeWaypoints: true,
			travelMode: 'DRIVING',
			provideRouteAlternatives:true
		}, cb);
	}
}
