import { TestBed, inject } from '@angular/core/testing';

import { DefaultParamsService } from './default-params.service';

describe('DefaultParamsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DefaultParamsService]
    });
  });

  it('should be created', inject([DefaultParamsService], (service: DefaultParamsService) => {
    expect(service).toBeTruthy();
  }));
});
