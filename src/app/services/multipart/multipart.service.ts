import { Injectable } from '@angular/core';
import {Http, Headers,RequestOptions} from '@angular/http';
import { LoopBackAuth, LoopBackConfig }  from './../../sdk/index';
import { Observable } from 'rxjs/Observable';
@Injectable()
export class MultipartService {

  base:string;
  urls: any;

  constructor(private http:Http,private auth:LoopBackAuth) { 
  	this.base = LoopBackConfig.getPath();
  	this.urls = {
  		signup : this.base+"/api/People/adminCreateUser"
  	}
  }


  request(url,data){
    return this.http.post(url,this.getFormObj(data),this.getOption()).map((res: any) => (res.text() != "" ? res.json() : {})).catch((error) => {return Observable.throw(error.json().error || 'Server error');});
  }

  getFormObj(obj){
  	let fd = new FormData();
    if(obj.data && typeof obj.data == 'object'){
		if(obj.data.extra){
			delete obj.data.extra;
		}
		if(obj.data.address){
			delete obj.data.address;
		}
		fd.append('data',JSON.stringify(obj.data));
    }
  	for(let x in obj.files){
		for(let y in obj.files[x]){
			fd.append(x,obj.files[x][y]);   
		}
    }
  	return fd;
  }

  getOption(){
    let header : Headers = new Headers();
    header.append('Authorization',this.auth.getAccessTokenId() || "");
    let opts: RequestOptions = new RequestOptions();
    opts.headers = header;
    return opts;
  }


  signupApi(data){
    return this.request(this.urls.signup,data);
  }


}
